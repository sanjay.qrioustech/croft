/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
@import Firebase;
#import "AppDelegate.h"
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <RNCPushNotificationIOS.h>
#import <UserNotifications/UserNotifications.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
  
  if ([FIRApp defaultApp] == nil) {
    [FIRApp configure];
  }
  UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
  center.delegate = self;
  
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"Croft"
                                            initialProperties:nil];
  
  // Set up a loading view. Here are three ways to do it
  // 1. Set up a plain background color
//  UIView* loadingView = [UIView new];
//  loadingView.backgroundColor = [UIColor colorWithRed:0x66 green:0x33 blue:0x99 alpha:1];
  // 2. Use a full screen image
  UIImageView* loadingView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"colourLogo"]];
  // 3. Use a nib file. For that create a View element in a xib.
  //    The name of the nib is the name of the file without the extension.
//  UIView* loadingView = [[[NSBundle mainBundle] loadNibNamed:@"Croft" owner:self options:nil] firstObject];
  // Final step! Assign the view on the RCTRootView object.
  rootView.loadingView = loadingView;
  
//  UIView *picker = [[UIView alloc] initWithFrame: rootView.bounds];
//  picker.backgroundColor=[UIColor blueColor];
//   [rootView addSubview: picker];
//  //rootView.loadingView = picker;
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}
//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
  completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}
// Required to register for notifications
- (void)application:(UIApplication* )application didRegisterUserNotificationSettings:(UIUserNotificationSettings* )notificationSettings
{
[RNCPushNotificationIOS didRegisterUserNotificationSettings:notificationSettings];
}
// Required for the register event.
- (void)application:(UIApplication* )application didRegisterForRemoteNotificationsWithDeviceToken:(NSData* )deviceToken
{
[RNCPushNotificationIOS didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication* )application didReceiveRemoteNotification:(NSDictionary* )userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
[RNCPushNotificationIOS didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
// Required for the registrationError event.
- (void)application:(UIApplication* )application didFailToRegisterForRemoteNotificationsWithError:(NSError* )error
{
[RNCPushNotificationIOS didFailToRegisterForRemoteNotificationsWithError:error];
}
// Required for the localNotification event.
- (void)application:(UIApplication* )application didReceiveLocalNotification:(UILocalNotification* )notification
{
[RNCPushNotificationIOS didReceiveLocalNotification:notification];
}


@end
