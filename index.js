/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Provider } from 'react-redux';
import {getStore, getPersistor}   from './src/store/configureStore';
import { PersistGate } from 'redux-persist/integration/react'

const AppRedux = () => {
    const myStore = getStore();  
    const myPersistor = getPersistor();
    return (
        <Provider store={myStore}>
            <PersistGate 
                    persistor={myPersistor}>
                   <App/>     
            </PersistGate>   
        </Provider>
    )
}


AppRegistry.registerComponent(appName, () => AppRedux);
