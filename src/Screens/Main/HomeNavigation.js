import React from 'react';
import { Text, View, Image, TouchableOpacity, Platform } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './HomeScreen/HomeScreen'
import FarmDetailsScreen from './FarmerDetailsScreen/FarmerDetailsHeader'
import ProductDetailsScreen from './ProductDetailsScreen/ProductDetailsScreen'

import MyOredrsHeaderScreen from './MyOrdersScreen/MyOredrsHeaderScreen'
import MyOrderDetail from './MyOrdersScreen/MyOrderDetailScreen'
import MyUpcomingOrdersScreen from './MyOrdersScreen/MyUpcomingOrdersScreen'
import MyPastOrdersScreen from './MyOrdersScreen/MyPastOrdersScreen'

import MyProfileHeaderScreen from './MyProfileScreen/MyProfileHeaderScreen'
import MyProfileDetailScreen from './MyProfileScreen/MyProfileDetailScreen'
import MyProfileHeaderScreen1 from './MyProfileScreen/MyProfileHeaderScreen1'

import AboutMeScreen from './MyProfileScreen/AboutMeScreen'
import ChangePassword from './MyProfileScreen/ChangePasswordScreen'
import FeedbackScreen from './MyProfileScreen/FeedbackScreen'
import GardeInfoScreeen from './MyProfileScreen/GarderInfoScreen'
import PickupdeliveryScreen from './MyProfileScreen/Pickup&DeliveryScreen'
import PaymentInfoScreen from './MyProfileScreen/PaymentInfoScreen'
import BankDetailsScreen from './MyProfileScreen/BankDetailsScreen'
import ReviewOrderScreen from './ReviewOrderScreen/ReviewOrderScreen'
import AddProductScreen from './AddProductScreen/AddProductScreen';
import EditCartScreen from './EditCartScreen/EditCartScreen';
import EditProductScreen from './AddProductScreen/EditProductScreen'

import SearchScreen from './SearchScreen/SearchScreen'

import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import DeliveryAddressScreen from './ReviewOrderScreen/DeliveryAddress'

const HomeNavigator = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen
  },
  FarmDetails: {
    screen: FarmDetailsScreen
  },
  ProductDetails: {
    screen: ProductDetailsScreen
  },
  ReviewOrder: {
    screen: ReviewOrderScreen
  },
  DeliveryAddress: {
    screen: DeliveryAddressScreen
  },
  EditCart: {
    screen: EditCartScreen
  }
},
  {
    headerLayoutPreset: 'center',
    initialRouteName: 'HomeScreen',
  });

const SearchNavigator = createStackNavigator({
  Search: {
    screen: SearchScreen
  },
  FarmDetails: {
    screen: FarmDetailsScreen
  },
  ProductDetails: {
    screen: ProductDetailsScreen
  }
},
  {
    headerLayoutPreset: 'center',
    initialRouteName: 'Search',
  });

const MyPostNavigator = createStackNavigator({
  AddProduct: {
    screen: AddProductScreen
  },
},
  {
    headerLayoutPreset: 'center',
    initialRouteName: 'AddProduct',
  });

const MyOredrNavigator = createStackNavigator({
  MyOrder: {
    screen: MyOredrsHeaderScreen
  },
  OrderDetail: {
    screen: MyOrderDetail
  }
},
  {
    headerLayoutPreset: 'center',
    initialRouteName: 'MyOrder',
  });

const MyProfileNavigator = createStackNavigator({
  MyProfile: {
    // screen: MyProfileHeaderScreen,
    screen: MyProfileHeaderScreen1,

    navigationOptions: {
      header : null,
    }
  },
  FarmDetails: {
    screen: FarmDetailsScreen
  },
  EditProduct: {
    screen: EditProductScreen
  },
  ProductDetails: {
    screen: ProductDetailsScreen
  },
  AboutMe: {
    screen: AboutMeScreen
  },
  ChangePassword: {
    screen: ChangePassword
  },
  FeedbackScreen: {
    screen: FeedbackScreen
  },
  GardeInfoScreeen: {
    screen: GardeInfoScreeen
  },
  PickupdeliveryScreen: {
    screen: PickupdeliveryScreen
  },
  PaymentInfoScreen: {
    screen: PaymentInfoScreen
  },
  BankDetailsScreen:{
    screen : BankDetailsScreen
  }
},
  {
    headerLayoutPreset: 'center',
    initialRouteName: 'MyProfile',
  });

const HomeTabNavigator = createBottomTabNavigator({
  HomeNav: HomeNavigator,
  MyOrders: MyOredrNavigator,
  Post: MyPostNavigator,
  Search: SearchNavigator,
  MyAccount: MyProfileNavigator,
},
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarLabel: () => {
        if (navigation.state.routeName === "Post") {
          return (<SimpleText style={{ color: Colors.theme, textAlign: 'center', fontSize: 12, bottom: 2 }} text='Post' />)
        } else {
          return null
        }
      },
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        //let IconComponent = Ionicons;
        let iconName;
        let iconColor;
        let iconWidth;
        let imageTop = 0
        if (routeName === 'HomeNav') {
          iconName = Assets.home;
          iconColor = focused ? Colors.theme : Colors.inactiveTab;
          iconWidth = 23
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.

        } else if (routeName === 'MyOrders') {
          iconName = Assets.myOrdersTabIcon
          iconColor = focused ? Colors.theme : Colors.inactiveTab;
          iconWidth = 23
        } else if (routeName === 'Post') {
          iconName = Assets.postTabIcon
          iconColor = Colors.theme;
          iconWidth = 25
          imageTop = 3
        } else if (routeName === 'Search') {
          iconName = Assets.searchTabIcon
          iconColor = focused ? Colors.theme : Colors.inactiveTab;
          iconWidth = 23
        } else if (routeName === 'MyAccount') {
          iconName = Assets.profileTabIcon
          iconColor = focused ? Colors.theme : Colors.inactiveTab;
          iconWidth = 23
        }

        // You can return any component that you like here!
        return <Image source={iconName} style={{ top: imageTop, width: iconWidth, height: iconWidth, tintColor: iconColor }} />
        //return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      //showLabel: false,
      activeTintColor: Colors.theme,
      inactiveTintColor: Colors.inactiveTab,
    },
  }
);

 export default createAppContainer(HomeTabNavigator);

// const AppContainer = createAppContainer(HomeTabNavigator);

// export default class HomeNavigation extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1 }}>
//         <AppContainer />
//         <View style={{
//           width: '100%', height: 50,
//           justifyContent: 'center',
//           alignItems: 'center',
//           position: 'absolute', //Here is the trick
//           bottom: Platform.OS === 'ios' ? 90 : 50 ,
//         }}>
//           <TouchableOpacity onPress={() => this.props.navigation.navigate('ReviewOrder')} style={[{ width: '50%', alignItems: 'center', justifyContent: "center", backgroundColor: '#de2618', borderRadius: 20, height: 40 }]}>
//             <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
//               <Ionicons name={'md-cart'} size={22} color='white' style={{ marginLeft: 15 }} />
//               <View style={{ flex: 3, flexDirection: 'column', paddingLeft: 10, justifyContent: 'space-between', alignItems: 'center', height: 30 }}>
//                 <SimpleText style={{ fontWeight: 'bold', color: 'white', fontSize: 8 }} text='VIEW CART'></SimpleText>
//                 <SimpleText style={{ fontWeight: 'bold', fontSize: 12, color: 'white', paddingBottom: 1 }} text="John's Farm"></SimpleText>
//               </View>
//               <SimpleText style={{ paddingRight: 10, color: 'white', fontSize: 10 }} text='2 items'></SimpleText>
//             </View>
//           </TouchableOpacity>
//         </View>
//       </View>
//     );
//   }
// }