import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import ModalDropdown from 'react-native-modal-dropdown';
import Ionicon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';

export default class ReviewOrderPickupScreen extends React.Component {

    state={
        timeSlots : this.props.timeSlots.map(slot => {
            let str = slot.from + " to " + slot.to
            return str
        })
    }

   
    
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 10 }}>
                    <SimpleText style={{ marginTop: 10, paddingLeft: 15, color: Colors.text, fontSize: 13, width:'50%' }} text='Note:'></SimpleText>
                    <SimpleText style={{ marginTop: 10, marginLeft: -20, color: Colors.subtextGray, fontSize: 13, width:'50%' }} text={this.props.note}></SimpleText>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 10 }}>
                    <SimpleText style={{ marginTop: 10, paddingLeft: 15, color: Colors.text, fontSize: 13 ,width:'40%'}} text='Select Pickup Time:'></SimpleText>

                    <TouchableOpacity 
                    onPress={()=>{
                        this.refs.time_dropdown.show()
                    }}
                    style={{ marginTop: 3, marginLeft: 18, borderWidth: 1, height: 30, alignItems: 'center', flexDirection: 'row', borderRadius: 5 }}>
                        <ModalDropdown
                            ref = "time_dropdown"
                            defaultValue={this.state.timeSlots[0]}
                            defaultIndex={0}
                            style={{ paddingHorizontal: 7, paddingVertical: 1 }}
                            textStyle={{ color: Colors.text, fontSize: 12 }}
                            dropdownTextStyle={{ fontSize: 13 }}
                            options={this.state.timeSlots}
                            onSelect={(index,value) => this.props.handlePickupTimeSelectHandler(value)}
                        />
                        {/* <SimpleText style={{ marginHorizontal: 10, fontSize: 15, fontWeight: '400' }} text="Pounds" /> */}
                        <View style={{ marginHorizontal: 10, height: 30, width: 1, backgroundColor: 'black' }}></View>
                        <Ionicon name='md-arrow-dropdown' size={15} style={{ marginRight: 10 }} color='black' />
                    </TouchableOpacity>
                    {/* <SimpleText style={{ marginTop: 10, paddingLeft: 15, color: Colors.text, fontSize: 13 }} text='10:00 AM'></SimpleText> */}
                </View>
            </View>
        );
    }
}