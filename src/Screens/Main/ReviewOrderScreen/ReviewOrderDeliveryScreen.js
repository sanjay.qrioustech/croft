import React from 'react';
import { Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import { CheckBox, colors } from 'react-native-elements'
import AddNewButton from 'library/components/addNewButton'
import * as selector from './reviewSelector'
import * as action from '../../../store/actions';
import { connect } from 'react-redux';
import {NavigationEvents} from 'react-navigation'
class ReviewOrderPickupScreen extends React.Component {
    state = {
        selectedAddressIndex: 0,
        selectedTimeIndex: 0,
        address_list : this.props.address.map((item,index) => { return { ...item, isSelected :( index === 0 ? true : false )} }),
    }
    handleDeliveryAddressSelectHandler1 = (index) => {
        // let slot  = value.split("to")
        // console.log("VALUE +> ",value)

        let list = this.state.address_list
        list = list.map((item,i) => {
            return{
                ...item,
                isSelected : index === i ? true : false
            }
        })

        this.setState({
            ...this.state,
            address_list : list
        })
    }
    render() {
        return (
            
            <View style={{ flex: 1 }}>
                <NavigationEvents onDidFocus={ () => {
                    this.setState({
                        address_list: this.props.address.map((item,index) => { return { ...item, isSelected :( index === 0 ? true : false )} })
                    })
                }}></NavigationEvents>
                <View style={{ paddingLeft: 15, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start', marginTop: 20 }}>
                    <SimpleText style={{ color: Colors.text, fontSize: 13, width: '37%' }} text='Select Delivery Address:'></SimpleText>
                    <View style={{ width: '63%', backgroundColor: Colors.white, justifyContent: 'flex-start' }}>
                        <FlatList
                        data={this.state.address_list}
                        renderItem={({item, index})=>
                        <CheckBox
                                    title={item.address}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={item.isSelected}
                                    onPress={() => {
                                        this.handleDeliveryAddressSelectHandler1(index)
                                        this.props.handleDeliveryAddressSelectHandler(index, this.state.address_list)}}
                                    containerStyle={{ marginTop: -13, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                    }
                        />
                        {/* {this.state.address_list.map((address, index) => {
                            return (
                                <CheckBox
                                    title={address.address}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={address.isSelected}
                                    onPress={() => this.props.handleDeliveryAddressSelectHandler(index)}
                                    containerStyle={{ marginTop: -13, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                            )
                        })} */}
                        {/* <CheckBox
                            title='21 Street, NY, USA'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checkedColor={Colors.theme}
                            uncheckedColor={Colors.primary}
                            checked={this.state.selectedAddressIndex === 0}
                            onPress={() => this.setState({ selectedAddressIndex: 0 })}
                            containerStyle={{ marginTop: -13, backgroundColor: 'transparent', borderColor: 'transparent' }}
                        ></CheckBox>
                        <CheckBox
                            title='223 Street, Dellas, USA'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checkedColor={Colors.theme}
                            uncheckedColor={Colors.primary}
                            checked={this.state.selectedAddressIndex === 1}
                            onPress={() => this.setState({ selectedAddressIndex: 1 })}
                            containerStyle={{ marginTop: -20, backgroundColor: 'transparent', borderColor: 'transparent' }}
                        ></CheckBox> */}
                        <AddNewButton title=' Add ' style={{ marginLeft: 25, borderColor: 'black', borderWidth: 0, marginRight: 120, }} action={() =>
                            this.props.navigation.navigate('DeliveryAddress')
                        } />
                    </View>
                </View>
                <View style={{ marginTop: 20, paddingLeft: 15, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }}>
                    <SimpleText style={{ color: Colors.text, fontSize: 13, width: '37%' }} text='Select Delivery Time:'></SimpleText>
                    <View style={{ width: '63%', backgroundColor: Colors.white, justifyContent: 'flex-start' }}>
                        {this.props.delivery_time.map((item,index) => {
                            return (
                                <CheckBox
                                    title={item.text}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={item.isSelected}
                                    onPress={() => this.props.handleDeliveryTimeSlectHandler(index)}
                                    containerStyle={{ marginTop: -13, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                            )
                        })}
                        {/* <CheckBox
                            title='Office (10AM to 5PM)'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checkedColor={Colors.theme}
                            uncheckedColor={Colors.primary}
                            checked={this.state.selectedTimeIndex === 0}
                            onPress={() => this.setState({ selectedTimeIndex: 0 })}
                            containerStyle={{ marginTop: -13, backgroundColor: 'transparent', borderColor: 'transparent' }}
                        ></CheckBox>
                        <CheckBox
                            title='Home (All Day)'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checkedColor={Colors.theme}
                            uncheckedColor={Colors.primary}
                            checked={this.state.selectedTimeIndex === 1}
                            onPress={() => this.setState({ selectedTimeIndex: 1 })}
                            containerStyle={{ marginTop: -20, backgroundColor: 'transparent', borderColor: 'transparent' }}
                        ></CheckBox>
                        <CheckBox
                            title='ASAP'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checkedColor={Colors.theme}
                            uncheckedColor={Colors.primary}
                            checked={this.state.selectedTimeIndex === 2}
                            onPress={() => this.setState({ selectedTimeIndex: 2 })}
                            containerStyle={{ marginTop: -20, marginBottom: -15, backgroundColor: 'transparent', borderColor: 'transparent' }}
                        ></CheckBox> */}
                    </View>
                </View>
            </View>
           
        );
    }
}

const mapStateToProps = state => {
    return {
        state1: state,
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData,
        deliveryType : state.farmReducers.cartData !== null ? state.farmReducers.cartData.farm_details.deliveryType : [],
        timeSlots : selector.getFarms(state) ,
        address : selector.getAddress(state)
    };
};

export default connect(mapStateToProps, null)(ReviewOrderPickupScreen)