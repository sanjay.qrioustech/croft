import { createSelector } from 'reselect'
import moment from 'moment';

const getFarmTimeSlot = (state) => state.farmReducers.cartData !== null ?state.farmReducers.cartData.farm_details.time : null

const getFarmAddress = (state) => state.authreducers.auth.address_list 


export const getFarms = createSelector(
    [ getFarmTimeSlot ],
    ( timeslot ) => {

        if(timeslot === null) return []
        let startTime = timeslot.from;
        let toTime = timeslot.to;

        let slotarr = getTimeStops(startTime, toTime)
        let timeSlotsArr
        timeSlotsArr = slotarr.map((time,index) => {
            if(index === slotarr.length-1) {
                return undefined
            }
            return {
                from : time,
                to : slotarr[index+1]
            }
        })

        return timeSlotsArr.filter(f => f!==undefined)
    }
)


const getTimeStops = (start, end) => {
    var startTime = moment(start, 'hh A');
    var endTime = moment(end, 'hh A');
    
    if( endTime.isBefore(startTime) ){
      endTime.add(1, 'day');
    }
  
    var timeStops = [];
  
    while(startTime <= endTime){
      timeStops.push(new moment(startTime).format('hh A'));
      startTime.add(60, 'minutes');
    }
    return timeStops;
  }
  

export const getAddress = createSelector(
    [ getFarmAddress ],
    ( addresses ) => {

        // let arr = []
        // arr.push({
        //     ...addresses,
        // })

        return addresses
    }
)