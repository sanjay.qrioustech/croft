import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, Alert, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'

import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import SimpleText from 'library/components/SimpleText'
import Autocomplete from 'react-native-autocomplete-input'
import axios from 'axios'
import { connect } from "react-redux";
import * as action from '../../../store/actions';

class DeliveryAddress extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Review',
            headerTintColor: colors.theme,
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    };

    state = {
        address: '',
        city: '',
        state: '',
        zipcode: ''
    }

    onTextChangeHandler = (type,value) => {
        this.setState({
            ...this.state,
            [type] : value
        })
    }

    componentDidMount() { }

    handleSubmit = () => {
        console.log(this.props.auth)
        let data = {
            user_id : this.props.auth._id,
            ...this.state
        }
        this.props.dispatch(action.addaddress(data)).then(() => {
            this.props.navigation.navigate('ReviewOrder')
        }).catch((e) => console.log(e))
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <Image source={assets.transperentlogo} style={{ height: 120, width: 120, alignSelf: 'center', marginBottom: 10 }}></Image>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 15, backgroundColor: colors.backgroundGray }}>

                            <CommonTextInput 
                                defaultValue={this.state.address} 
                                style={{ width: Dimensions.get('screen').width - 20 }} name='Address'
                                placeholder='Required'
                                onTextChange={(address) => this.onTextChangeHandler('address',address)}
                            />
                            <CommonTextInput 
                                defaultValue={this.state.city} 
                                style={{ width: Dimensions.get('screen').width - 20 }} name='City'  placeholder='Required'
                                onTextChange={(address) => this.onTextChangeHandler('city',address)}
                            />
                            <CommonTextInput 
                                defaultValue={this.state.state} 
                                style={{ width: Dimensions.get('screen').width - 20 }} 
                                name='State'
                                placeholder='Required'
                                onTextChange={(address) => this.onTextChangeHandler('state',address)}
                            />
                            <CommonTextInput 
                                defaultValue={this.state.zipcode} 
                                style={{ width: Dimensions.get('screen').width - 20 }} name='Zipcode' 
                                placeholder='Required'
                                keyboardType='number-pad'
                                onTextChange={(address) => this.onTextChangeHandler('zipcode',address)}
                            />
                        </View>
                        <View 
                            style={{ 
                                flex: 1, 
                                justifyContent: 'center', 
                                alignItems: 'center', 
                                paddingTop: 30, 
                                backgroundColor: colors.backgroundGray 
                            }}>
                            <CircularCornerButton
                                disabled={this.state.address === '' ||
                                    this.state.city === '' ||
                                    this.state.state === '' ||
                                    this.state.zipcode === ''} 
                                action={() => this.handleSubmit()}
                                title='Add Address'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                            />
                        </View>
                    </View>
                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}


const mapStateToProps = state => {
    return {
        auth: state.authreducers.auth,
    };
};

export default connect(mapStateToProps)(DeliveryAddress)


