import React from 'react';
import { Text, View, Image, TouchableOpacity, SafeAreaView, FlatList, ScrollView } from 'react-native';
import { CheckBox, colors } from 'react-native-elements'
import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import RadioGroup, { Radio } from 'react-native-radio-input';
import ReviewOrderPickupScreen from '../ReviewOrderScreen/ReviewOrderPickupScreen'
import ReviewOrderDeliveryScreen from '../ReviewOrderScreen/ReviewOrderDeliveryScreen'
import CircularCornerButton from 'library/components/circularCornerButton'
import { connect } from 'react-redux';
import ReviewCartItem from 'library/components/ReviewCartItem'
import { IMAGE_BASE_URL } from '../../../appConstant'
import * as selector from './reviewSelector'
import * as action from '../../../store/actions';

//const review = ['Delivery', 'Pickup']

class ReviewOrderScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Review',
            headerTintColor: Colors.theme,
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    };

    state = {
        selectedIndex: this.props.deliveryType.includes('pickup') ? 0 : 1,
        address_list : this.props.address.map((item,index) => { return { ...item, isSelected :( index === 0 ? true : false )} }),
        timeslot : this.props.timeSlots.length > 0 ? this.props.timeSlots[0].from + " to " + this.props.timeSlots[0].to : "",
        delivery_time : [{
            text : 'office (10AM to 5PM)',
            isSelected : true
        },{
            text : 'Home (All Day)',
            isSelected : false
        },{
            text : 'ASAP',
            isSelected : false
        }]
    }

    handleOrderType = (index) => {
        this.setState({ ...this.state , selectedIndex : index },() => {
            console.log(this.state)
        })
    }

    handlePickupTimeSelectHandler = (value) => {
        //let slot  = value.split("to")
        console.log(value)
        this.setState({ ...this.state, timeslot : value })
    }

    handleDeliveryAddressSelectHandler = (index, list) => {
        // let slot  = value.split("to")
        // console.log("VALUE +> ",value)

        // let list = this.state.address_list
        list = list.map((item,i) => {
            return{
                ...item,
                isSelected : index === i ? true : false
            }
        })

        this.setState({
            ...this.state,
            address_list : list
        })
    }

    handleDeliveryTimeSlectHandler = (index) => {
        let list = this.state.delivery_time
        list = list.map((item,i) => {
            return{
                ...item,
                isSelected : index === i ? true : false
            }
        })

        this.setState({
            ...this.state,
            delivery_time : list
        })
    }

    handleSubmit = () => {
        
        // let data = {
        //     orderType : this.state.selectedIndex === 0 ? "pickup" : "delivery",
        //     pickuptime : this.state.timeslot,
        //     delivery_address : this.state.address_list.filter(f => f.isSelected)[0],
        //     delivery_time : this.state.delivery_time.filter(f => f.isSelected)[0]
        // }
        

        let data = {
            user_id : this.props.auth._id,
            cart_id : this.props.cartList._id,
            order_type : this.state.selectedIndex === 0 ?  'pickup' : 'delivery' ,
            time : this.state.selectedIndex === 0 ? this.state.timeslot : this.state.delivery_time.filter(f => f.isSelected)[0].text,
        }
        
        if(this.state.selectedIndex === 1){
            data.address = this.state.address_list.filter(f => f.isSelected)[0].address;
            data.delivery_charge = this.props.cartList.user_id.farm.delivery_charge;
        }

        // if(this.props.auth.isCardUpdated){
        //     console.log("Request Data => ",data)
        //     this.props.dispatch(action.placeorderAction(data)).then(() => {
        //         this.props.dispatch(action.getCartListApiAction(this.props.auth._id))
        //         this.props.navigation.goBack();
        //     }).catch ((e) => console.log(e))
        // }else{
        //     this.props.navigation.navigate('PaymentInfoScreen')
        // }

        if(!this.props.auth.isCardUpdated || this.props.auth.isCardUpdated===undefined){
            this.props.navigation.navigate('PaymentInfoScreen')
        }else{
            console.log("Request Data => ",data)
            this.props.dispatch(action.placeorderAction(data)).then(() => {
                this.props.dispatch(action.getCartListApiAction(this.props.auth._id))
                this.props.navigation.goBack();
            }).catch ((e) => console.log(e))
        }
        
    }

    componentDidMount() {
        if(this.props.cartList === null){
            this.props.navigation.goBack();
        }
        console.log("deliveryType => ", this.props.deliveryType)
        console.log('AUTH==>',this.props.auth)
    }

    render() {
        let currentView = this.state.selectedIndex == 0 ?
            <ReviewOrderPickupScreen
                note = { this.props.cartList !== null ? "Farm is available from " + this.props.cartList.farm_details.time.from + " to " + this.props.cartList.farm_details.time.to : ""}
                timeSlots = {this.props.timeSlots}
                handlePickupTimeSelectHandler = {this.handlePickupTimeSelectHandler}
            /> : <ReviewOrderDeliveryScreen {...this.props} 
                address_list={this.state.address_list}
                delivery_time = {this.state.delivery_time}
                handleDeliveryAddressSelectHandler={this.handleDeliveryAddressSelectHandler}
                handleDeliveryTimeSlectHandler = {this.handleDeliveryTimeSlectHandler} />

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.backgroundGray }}>
                <ScrollView style={{ backgroundColor: 'white' }}>
                    <View style={{ flex: 1, paddingTop: 15 }}>

                        <FlatList
                            data={this.props.cartList !== null ? this.props.cartList.products : []}
                            keyExtractor={item => item._id}
                            style={{ marginBottom: 20 }}
                            renderItem={({ item }) =>
                                <ReviewCartItem
                                    farmName={this.props.cartList !== null ? this.props.cartList.farm_name : ""}
                                    productName={item.product_id.product_name}
                                    productImage={item.product_id.image.length > 0 ? IMAGE_BASE_URL + item.product_id.image[0] : ''}
                                    productPrice={item.product_id.price + "/" + item.product_id.unit}
                                    EditAction={()=>{
                                        this.props.navigation.navigate('EditCart',{
                                            data: item.product_id,
                                            farm_name: this.props.cartList.farm_name,
                                            cart_quantity: item.quantity
                                        })
                                    }}
                                    productQuantity={item.quantity + " " + item.product_id.unit}
                                />}
                        />
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }}>
                            <SimpleText style={{ paddingLeft: 15, color: Colors.text, fontSize: 13, width: '40%' }} text='Order Type:'></SimpleText>
                            <View style={{ width: '60%', backgroundColor: Colors.white, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <CheckBox
                                    title='Pick Up'
                                    textStyle = { !this.props.deliveryType.includes('pickup') ? { color: 'lightgray' } : null }
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    disabled={
                                        !this.props.deliveryType.includes('pickup')
                                    }
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={this.state.selectedIndex === 0}
                                    onPress={() => this.handleOrderType(0)}
                                    containerStyle={{ marginTop: -13, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                                <CheckBox
                                    title='Delivery'
                                    textStyle = { !this.props.deliveryType.includes('delivery') ? { color: 'lightgray' } : null }
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checkedColor={Colors.theme}
                                    disabled={!this.props.deliveryType.includes("delivery")}
                                    uncheckedColor={Colors.primary}
                                    checked={this.state.selectedIndex === 1}
                                    onPress={() => this.handleOrderType(1)}
                                    containerStyle={{ marginTop: -20, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                            </View>
                            
                        </View>
                        {this.state.selectedIndex === 1 ? <SimpleText style={{ paddingLeft: '45%', color: Colors.red, fontSize: 13, width: '100%' }} text='Delivery Charges Applied'></SimpleText> : null }

                        {currentView}

                        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', paddingTop: 50, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton title='Continue' style={{ width: '60%' }} action={() => this.handleSubmit()}/>
                        </View> 
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}


const mapStateToProps = state => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData,
        deliveryType : state.farmReducers.cartData !== null ? state.farmReducers.cartData.farm_details.deliveryType : [],
        timeSlots : selector.getFarms(state) ,
        address : selector.getAddress(state)
    };
};

export default connect(mapStateToProps, null)(ReviewOrderScreen)