import React, { Component } from 'react'
import { SafeAreaView, View, FlatList, Text, ScrollView, TouchableOpacity } from 'react-native'
import ProductListItem from 'library/components/productListItem'
import Assets from 'res/Assets'
import SimpleText from 'library/components/SimpleText'
import colors from 'res/Colors'


class FarmStoreScreen extends Component {
    state = {
        showAddReview: false,
        starCount: 0,
        productArray: [
            {
                id: 1,
                image: Assets.testImageMango,
                name: "Mango",
                price: "$5/Unit",
                date: '01/01/2020'
            },
            {
                id: 2,
                image: Assets.testImageMango,
                name: "Apple",
                price: "$10/Unit",
                date: '01/01/2020'
            },

        ]
    };
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ marginTop: 10 }}>
                        {/* <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17 }} text="Current Products" /> */}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17 }} text="Current Products" />
                            {/* <SimpleText style={{ margin: 10, fontSize: 12, color: colors.theme }} text="View All" /> */}

                        </View>
                        <FlatList
                            data={this.props.productData.currentList}
                            keyExtractor={item => item.id}
                            ListEmptyComponent={
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontSize: 15, textAlign: 'center', padding: 20 }}>No Data Found</Text>
                                </View>
                            }
                            renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('EditProduct', {
                                    data: item
                                }) }}>
                                    <ProductListItem style={{ width: '100%' }}
                                        image={item.image}
                                        name={item.product_name}
                                        price={'$' + item.price + '/' + item.unit}
                                        date={item.harvesting_date}
                                    />
                                </TouchableOpacity>
                            }


                        />
                        <View style={{ backgroundColor: colors.backgroundGray }}>

                            {/* <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17 }} text="Upcoming Harvests" /> */}
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17 }} text="Upcoming Harvests" />
                                {/* <SimpleText style={{ margin: 10, fontSize: 12, color: colors.theme }} text="View All" /> */}

                            </View>
                            <FlatList
                                data={this.props.productData.upcomingList}
                                keyExtractor={item => item.id}
                                ListEmptyComponent={
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: 15, textAlign: 'center', padding: 20 }}>No Data Found</Text>
                                    </View>
                                }
                                renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('EditProduct', {
                                    data: item
                                }) }}>
                                    <ProductListItem style={{ width: '100%' }}
                                        image={item.image}
                                        name={item.product_name}
                                        price={'$' + item.price + '/' + item.unit}
                                        date={item.harvesting_date}
                                    />
                                </TouchableOpacity>
                                }

                            />
                        </View>

                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}
export default FarmStoreScreen