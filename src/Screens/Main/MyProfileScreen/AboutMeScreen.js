import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, Alert, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CommonTextInputMultiline from 'library/components/commonInputTextMultiline'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import { connect } from "react-redux";
import * as action from '../../../store/actions';
import Geocoder from 'react-native-geocoding';
import { Colors } from 'react-native/Libraries/NewAppScreen'

class AboutMe extends Component {
    static navigationOptions = {
        title: 'About Me',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    state = {
        slectedtab: 0,
        auth: {
            user_id: this.props.auth._id,
            fname: '',
            lname: '',
            address: this.props.auth.address_list[0].address,
            city: this.props.auth.address_list[0].city,
            state: this.props.auth.address_list[0].state,
            zipcode: this.props.auth.address_list[0].zipcode,
            aboutFarm: '',
            farm: {
                name: '',
                about_farm: '',
            },
            ...this.props.auth
        }
    }

    componentDidMount() {
        //Geocoder.init("AIzaSyBsFHabdOkzS7m_Eqc5bO8maqbJrhCpocc");
    }

    handleFarmName(farmName) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    name: farmName
                }
            }


        })
        console.log(this.state.auth);
    }

    handleFirstName(firstName) {
        this.setState({
            auth: {
                ...this.state.auth,
                fname: firstName
            }

        })
        console.log(this.state.auth);
    }

    handleLastName(lastName) {
        this.setState({
            auth: {
                ...this.state.auth,
                lname: lastName
            }

        })
        console.log(this.state.auth);
    }

    handleAddress(address) {
        this.setState({
            auth: {
                ...this.state.auth,
                address: address
            }

        })
        console.log(this.state.auth);
    }

    handleCity(city) {
        this.setState({
            auth: {
                ...this.state.auth,
                city: city
            }

        })
        console.log(this.state.auth);
    }

    handleState(state) {
        this.setState({
            auth: {
                ...this.state.auth,
                state: state
            }

        })
        console.log(this.state.auth);
    }

    handleZipcode(zipcode) {
        this.setState({
            auth: {
                ...this.state.auth,
                zipcode: zipcode
            }

        })
        console.log(this.state.auth);
    }

    handleAboutFarm(aboutFarm) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    about_farm: aboutFarm
                }
            }


        })

        console.log(this.state.auth);
    }

    handleSubmit = () => {
        console.log("state", this.state)
        console.log("auth", this.state.auth)
        const add = `${this.state.auth.address}, ${this.state.auth.city}, ${this.state.auth.state}, ${this.state.auth.zipcode}`
        // Geocoder.from(add)
        // .then(json => {
        //     var location = json.results[0].geometry.location;
        //     console.log(location);
        //     let userData = {
        //         ...this.state.auth,
        //         latitude: location.lat, 
        //         longitude: location.lng,
        //     }
        //     this.props.dispatch(action.userUpdateApiAction(userData)).then(            
        //         () => this.props.navigation.navigate("MyProfile")
        //       ).catch(err => {
        //         Alert.alert('Error', err)
        //         console.log("auth" , "......")     

        //       })
        // })
        // .catch(error => {
        //     console.warn(error)
        //     alert("Oops!", 'Please fill proper address.')
        // });

        this.props.dispatch(action.userUpdateApiAction(this.state.auth)).then(
            () => {
                Alert.alert('Done', 'Profile Updated Successfully')
                this.props.navigation.navigate("MyProfile")
            }

        ).catch(err => {
            Alert.alert('Error', err)
            console.log("auth", "......")

        })

    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <CommonTextInput
                            maxLength={15}
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='Farm Name' placeholder='Required'
                            onTextChange={farmName => this.handleFarmName(farmName)}
                            value={this.state.auth.farm.name}
                        />
                        <CommonTextInput
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='First Name'
                            placeholder='Required'
                            onTextChange={firstName => this.handleFirstName(firstName)}
                            value={this.state.auth.fname}
                        />
                        <CommonTextInput
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='Last Name'
                            placeholder='Required'
                            onTextChange={lastName => this.handleLastName(lastName)}
                            value={this.state.auth.lname}
                        />
                        <CommonTextInput
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='Address'
                            placeholder='Required'
                            onTextChange={address => this.handleAddress(address)}
                            value={this.state.auth.address}
                        />
                        <CommonTextInput
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='City'
                            placeholder='Required'
                            onTextChange={city => this.handleCity(city)}
                            value={this.state.auth.city}
                        />
                        <CommonTextInput
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='State'
                            placeholder='Required'
                            onTextChange={state => this.handleState(state)}
                            value={this.state.auth.state}
                        />
                        <CommonTextInput
                            style={{ width: Dimensions.get('screen').width - 20 }}
                            name='Zipcode'
                            placeholder='Required'
                            onTextChange={zipcode => this.handleZipcode(zipcode)}
                            value={this.state.auth.zipcode.toString()}
                        />
                        <CommonTextInputMultiline
                            style={{ width: Dimensions.get('screen').width - 20, maxLengh: 200 }}
                            name='About Farm'
                            placeholder='Required'
                            onTextChange={aboutFarm => this.handleAboutFarm(aboutFarm)}
                            value={this.state.auth.farm.about_farm}
                        />

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton
                                title='Submit'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                action={() => this.handleSubmit()}
                                disabled={
                                    //this.state.auth.farmName === '' ||
                                    this.state.auth.fname === '' ||
                                    this.state.auth.lname === '' ||
                                    this.state.auth.address === '' ||
                                    this.state.auth.city === '' ||
                                    this.state.auth.state === '' ||
                                    this.state.auth.zipcode === '' //||
                                    //this.state.auth.aboutFarm === ''
                                }
                            />
                        </View>


                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }

}

const Styles = StyleSheet.create({
    commen: {
        marginLeft: 5,
    }

});

const mapStateToProps = state => {
    return {
        auth: state.authreducers.auth
    };
};

export default connect(mapStateToProps, null)(AboutMe);


