import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import MyProfileDetailScreen from './MyProfileDetailScreen'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import MyProfileStoreScreen from './MyProfileStoreScreen'
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import * as action from '../../../store/actions';
import { connect } from 'react-redux';
import SimpleText from 'library/components/SimpleText'
import StarRating from 'react-native-star-rating';
import MyProfileDetailScreen1 from './MyProfileDetailScreen1'
import * as selector from '../FarmerDetailsScreen/FarmDetailsSelector';

class MyProfileHeaderScreen1 extends Component {
    state = {
        count: '',
        rating: ''
    }

    static navigationOptions = {
        title: 'My Account',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };


    componentDidMount() {
        console.log("willFocus runs")
        console.log('AUTHU inHeader==>', this.props.auth)

        const { navigation } = this.props;
        navigation.addListener('willFocus', async () => {
            console.log("willFocus runs")
            this.props.dispatch(action.getProductListApiAction(this.props.auth._id)).then((response) => {
                console.log('List', response)
                if (response.data.status === 1) {
                    this.setState({
                        productData: response.data.data
                    })

                }
            })
            this.props.dispatch(action.getReviewsListApiAction(this.props.auth._id)).then((response) => {
                console.log('ReviewList', response)
                if (response.data.status === 1) {
                    let array = [4.2, 3.2, 5.0, 4.1];
                    let sum = 0
                    response.data.data.forEach(({ rating }) => {
                        sum = sum + parseFloat(rating)
                        console.log(sum)
                    });
    
                    this.setState({
                        count: response.data.data.length,
                        rating: (sum / response.data.data.length).toFixed(2),
                    })
                    console.log(this.state.count)
                    console.log(this.state.rating)
                }
            })
        });

        


        // this.props.dispatch(action.getProductListApiAction(this.props.auth._id)).then((response) => {
        //     console.log('List', response)
        //     if (response.data.status === 1) {
        //         this.setState({
        //             productData: response.data.data
        //         })

        //     }
        // })

    }
    componentWillUnmount() {
        //didBlurSubscription.remove();
    }
    state = {
        selectedIndex: 0,
        productData: {
            upcomingList: [],
            currentList: []
        }
    }
    render() {
        let currentView = this.state.selectedIndex == 0 ? <MyProfileDetailScreen1 {...this.props} /> : <MyProfileStoreScreen {...this.props} productData={this.state.productData} />

        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.props.auth.isFarmUpdated ?
                    <React.Fragment>
                        <View style={{ height: 140, backgroundColor: colors.theme, paddingLeft: 20, paddingTop: 10 }}>
                            <Text style={{ fontSize: 24, color: colors.white, fontWeight: 'bold' }}>{this.props.auth.farm.name}</Text>
                            <Text style={{ fontSize: 18, color: colors.white, marginTop: 4 }}>{this.props.auth.address_list[0].address}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                <StarRating
                                    containerStyle={{ width: 140 }}
                                    starSize={22}
                                    halfStarEnabled={true}
                                    disabled={true}
                                    maxStars={5}
                                    rating={this.state.rating}
                                    starStyle={{ padding: 3, backgroundColor: '#e1dada', borderRadius: 5 }}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={Colors.white}
                                />
                                <Text style={{ fontSize: 18, color: colors.white, marginLeft: 12 }}>{this.state.rating==='Nan' ? 0 : this.state.rating}</Text>
                                <Text style={{ fontSize: 18, color: colors.white, marginLeft: 10 }}> &bull; {this.state.count} Review</Text>
                                
                            </View>
                            <TouchableOpacity onPress={()=>{
                                this.props.navigation.navigate('FarmDetails', {
                                    data: this.props.auth,
                                    id: this.props.auth._id
                                })
                            }}><Text style={{ fontSize: 16, color: colors.white, paddingTop: 10 }}>View Profile</Text></TouchableOpacity>
                        </View>
                    </React.Fragment>
                    : null
                }

                <View style={{ marginTop: 10, height: 40, alignItems: 'center', justifyContent: 'center', }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}>
                        <SegmentedControlTab
                            lastTabStyle={{ borderRadius: 5 }}
                            values={['Detail', 'Store']}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={(index) => {
                                this.setState({
                                    selectedIndex: index
                                })
                            }}
                            borderRadius={5}
                            tabsContainerStyle={{ height: 40, backgroundColor: '#F2F2F2', width: Dimensions.get('screen').width / 1.2, borderRadius: 5, }}
                            tabStyle={{
                                borderRadius: 5,
                                backgroundColor: '#F2F2F2',
                                borderWidth: 0,
                                borderColor: 'transparent',
                            }}
                            activeTabStyle={{ backgroundColor: 'white', margin: 2, borderRadius: 5 }}
                            tabTextStyle={{ color: 'black', fontWeight: 'bold' }}
                            activeTabTextStyle={{ color: colors.theme }}
                        />
                    </View>
                </View>
                {/* <View style={{marginTop: 10, height:1, backgroundColor: '#cacaca'}}></View> */}
                {currentView}
                <CartInfoBottomButton {...this.props} />
            </SafeAreaView>
        )
    }
}
const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        //farmsDetails: selector.getFarmDetails(state)
    }
}

export default connect(mapStatetoProps, null)(MyProfileHeaderScreen1);