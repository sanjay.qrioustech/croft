import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CommonTextInputMultiline from 'library/components/commonInputTextMultiline'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import ForTextInput from '../../../Library/components/ForTextInput'
import { connect } from 'react-redux'
import * as action from '../../../store/actions';
import Loader from 'library/components/loader'
import reactNativeModalDropdown from 'react-native-modal-dropdown'


const STRIPE_ERROR = 'Payment service error. Try again later.';
const SERVER_ERROR = 'Server error. Try again later.';
const STRIPE_PUBLISHABLE_KEY = 'pk_test_rSsOmdBHEarq2CfGId4zHYYJ00zuiJvqy1';

const getBankToken = (bankData) => {
    const bank = {
        "bank_account[country]": "US",
        "bank_account[currency]": "USD",
        "bank_account[account_holder_name]": bankData.name,
        "bank_account[account_holder_type]": "individual",
        "bank_account[routing_number]": bankData.routingNumber,
        "bank_account[account_number]": bankData.accountNumber
    };
    return fetch('https://api.stripe.com/v1/tokens', {
        headers: {
            // Use the correct MIME type for your server
            Accept: 'application/json',
            // Use the correct Content Type to send data to Stripe
            'Content-Type': 'application/x-www-form-urlencoded',
            // Use the Stripe publishable key as Bearer
            Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`
        },
        // Use a proper HTTP method
        method: 'post',
        // Format the credit card data to a string of key-value pairs
        // divided by &
        body: Object.keys(bank)
            .map(key => key + '=' + bank[key])
            .join('&')
    }).then(response => response.json())
};
/**
 * The method imitates a request to our server.
 *
 * @param bankToken
 * @return {Promise<Response>}
 */
const subscribeUser = (bankToken) => {
    return new Promise((resolve) => {
        console.log('Credit card token\n', bankToken);
        setTimeout(() => {
            resolve({ status: true });
        }, 1000)
    });
};

class BankDetailsScreen extends Component {
    static navigationOptions = {
        title: 'Bank Detail',
        headerTintColor: colors.theme,

        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    state = {
        
        bank: {
            routingNumber: '',
        accountNumber: '',
        name: '',
        },
        bankInfo: {
            token: '',
            user_id:this.props.Auth._id,
        },
        isLoading : false
    
    }
    componentDidMount(){
        this.props.dispatch(action.getBankDetailsApiAction({user_id: this.props.Auth._id})).then(res=>{
            console.log('Bank Details', res)
            if (res.data.status === 1){
                this.setState({
                    bank: {
                        routingNumber: res.data.data.routing_number,
                        accountNumber: 'xxxxxx'+res.data.data.account_number,
                        name: this.props.Auth.fname + ' ' + this.props.Auth.lname
                    }
                })
            }
        })
    }

    //number,123
    //name, tex
    handleInputChange(type, value) {
        this.setState({
            bank:{
                ...this.state.bank,
                [type]: value
            }
            
        })
        console.log(this.state.bank)
    }

   
    _onSubmit = async (creditCardInput) => {
        // this.setState({ card: form })
        console.log('CARD',this.state)
        this.setState({ isLoading : true })
        let bankToken;
        try {
            // Create a credit card token
            bankToken = await getBankToken(this.state.bank);
            this.setState({
                ...this.state,
                bankInfo: {
                    ...this.state.bankInfo,
                    token: bankToken.id
                }
            })
            console.log("TOKEN : ", bankToken)
            if (bankToken.error) {
                console.log('ERROR===>', bankToken.error)
                // Reset the state if Stripe responds with an error
                // Set submitted to false to let the user subscribe again
                this.setState({ submitted: false, error: STRIPE_ERROR });
                return;
            }
            this.props.dispatch(action.bankApiAction(this.state.bankInfo)).then(            
                res => {
                    this.setState({ isLoading : false })
                   // console.log('RES OF BANK=>',res.data)
                    if(res.data.status === 1){
                        // this.props.Auth.isCardUpdated=true
                        // this.props.navigation.goBack();
                        this.props.dispatch(action.userupdateaction(res.data.data))
                        Alert.alert("Sucess","Bank Account Details Updated",
                        [
                            {text: 'OK', onPress: () => {
                                this.props.navigation.goBack();
                            }},
                        ],
                        {cancelable: false})
                        //this.props.navigation.navigate('ReviewOrder')
                    }
                }
              ).catch(err => {
                //alert('Error', err)
                console.log("BANK SCREEN ERRO" , err)     
                this.setState({ isLoading : false })
              })

        } catch (e) {
            console.log('ERROR===>', e)
            // Reset the state if the request was sent with an error
            // Set submitted to false to let the user subscribe again
            this.setState({ submitted: false, error: STRIPE_ERROR, isLoading : false });
            return;
        }
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        {/* <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Card Number ' placeholder='Required' />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Account Number' placeholder='at least 8 characters' isSecure={true} />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Expire Date' placeholder='Required' />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='CVC' placeholder='Required' />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Adddress' placeholder='Required' /> */}

                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Name' placeholder='Required' onTextChange={value => this.handleInputChange('name', value)} value={this.state.bank.name}/>

                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Account Number' placeholder='Required' onTextChange={value => this.handleInputChange('accountNumber', value)} value={this.state.bank.accountNumber}/>
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Routing Number' placeholder='Required' onTextChange={value => this.handleInputChange('routingNumber', value)} value={this.state.bank.routingNumber} />


                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton disabled={
                                    this.state.bank.accountNumber.trim() === '' ||
                                    this.state.bank.name.trim() === '' ||
                                    this.state.bank.routingNumber.trim() === '' 
                                } title='Submit' style={{ width: Dimensions.get('screen').width - 40 }} action={()=>this._onSubmit()} />
                        </View>


                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
                </KeyboardAvoidingView>
                {this.state.isLoading ? <Loader/> : null}
            </SafeAreaView>
        );
    }

}

const Styles = StyleSheet.create({
    commen: {
        marginLeft: 5,
    }

});
const mapStateToProps = (state) => {
    return {
        Auth: state.authreducers.auth
    }
}

export default connect(mapStateToProps, null)(BankDetailsScreen)