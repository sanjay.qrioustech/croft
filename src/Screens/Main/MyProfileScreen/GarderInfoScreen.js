import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Dimensions,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  SectionList,
  LayoutAnimation
} from "react-native";
import assets from "res/Assets";
import colors from "res/Colors";
import CommonTextInput from "library/components/commonInputText";
import CommonTextInputMultiline from "library/components/commonInputTextMultiline";
import CircularCornerButton from "library/components/circularCornerButton";
import GoogleSignInButton from "library/components/googleSignInButton";
import FacebookLogInButton from "library/components/facebookLogInButton";
import SimpleText from "library/components/SimpleText";
import { CheckBox } from "react-native-elements";
import AntDesign from "react-native-vector-icons/AntDesign";
import Ionicons from "react-native-vector-icons/Ionicons";
import Expandable_ListView from "library/components/Expandable_listview";
import Lodash from 'lodash'
import { connect } from 'react-redux';
import { userUpdateApiAction } from '../../../store/actions'
// import { Icon } from 'react-native-vector-icons/Icon'

class GardeInfoScreeen extends Component {
  static navigationOptions = {
    title: "Garden Info",
    headerTintColor: colors.theme,
    headerTitleStyle: {
      fontWeight: "bold"
    }
  };

  constructor(props) {
    super(props);

    const array = [
      {
        expanded: true,
        category_Name: "Growing Methods",
        sub_Category: [
          { id: 1, name: "Inground Gardening", selected: true },
          { id: 2, name: "Certified Organic", selected: true },
          { id: 3, name: "Non-Certified Organic", selected: false },
          { id: 4, name: "Square Foot", selected: false },
          { id: 5, name: "Permaculture", selected: true },
          { id: 6, name: "Home Compost", selected: false },
          { id: 7, name: "Companion Planting", selected: false },
          { id: 8, name: "Intensive Gardening", selected: true },
          { id: 9, name: "Integrated Pest Management(IPM)", selected: false }
        ]
      },

      {
        expanded: false,
        category_Name: "Soil ammendments",
        sub_Category: [
          { id: 10, name: "Gypsum", selected: false },
          { id: 11, name: "BioChar", selected: true },
          { id: 12, name: "Humus", selected: false },
          { id: 13, name: "Sphagnum Peat Moss", selected: false },
          { id: 14, name: "Composted Manure", selected: false },
          { id: 15, name: "Mushroom Compost", selected: true },
          { id: 16, name: "Lime", selected: false },
          { id: 17, name: "Sulfur", selected: true },
          { id: 18, name: "Perlite", selected: false },
          { id: 19, name: "Vermiculture", selected: true },
          { id: 20, name: "Miracle-Gro", selected: false }
        ]
      },

      {
        expanded: false,
        category_Name: "Plant Spaces",
        sub_Category: [
          { id: 21, name: "Unframed Beds", selected: true },
          { id: 22, name: "Straw Bale Beds", selected: true },
          { id: 23, name: "Vertical Gardening", selected: true },
          { id: 24, name: "Raised Beds", selected: false },
          { id: 25, name: "Hydroponic", selected: false },
          { id: 26, name: "Aquaponic", selected: false },
          { id: 27, name: "Aeroponic", selected: false },
          { id: 28, name: "Coontainer", selected: false }
        ]
      }
    ];

    this.state = { 
      GrowingMethods: this.props.auth.farm.gardeninfo ? this.props.auth.farm.gardeninfo.growing_method : [], 
      PlantSpaces: this.props.auth.farm.gardeninfo ? this.props.auth.farm.gardeninfo.plant_spaces : [], 
      Soilammendments: this.props.auth.farm.gardeninfo ? this.props.auth.farm.gardeninfo.soil_ammendments : [], 
      AccordionData: [...array] 
    };
  }

  selectedListItem = (title, item, selectedItems) => {
    console.log('title ',title,'item ', item, 'selectedItems', selectedItems)
    if (title.replace(' ','')==="GrowingMethods") {
      const curr = this.state.GrowingMethods
      this.setState({
        GrowingMethods: curr.includes(item) ? Lodash.remove(curr, function (n){
          return n !== item
        }) : [...curr, item]
      })
    } else if (title.replace(' ','')==="Soilammendments") {
      const curr = this.state.Soilammendments
      this.setState({
        Soilammendments: curr.includes(item) ? Lodash.remove(curr, function (n){
          return n !== item
        }) : [...curr, item]
      })
    } else {
      const curr = this.state.PlantSpaces
      this.setState({
        PlantSpaces: curr.includes(item) ? Lodash.remove(curr, function (n){
          return n !== item
        }) : [...curr, item]
      })
    }

    console.log('methods', this.state)
  }

  update_Layout = index => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    const array = [...this.state.AccordionData];

    array[index]["expanded"] = !array[index]["expanded"];

    this.setState(() => {
      return {
        AccordionData: array
      };
    });
  };

  handleSubmit = () => {
    console.log("state",this.state)
    console.log("auth",this.state.auth)  
    let gardenInfo = {
      user_id: this.props.auth._id,
      ...this.props.auth,
      farm: {
        ...this.props.auth.farm,
        gardeninfo: 
          {
              growing_method: this.state.GrowingMethods,
          
              soil_ammendments: this.state.Soilammendments,
          
              plant_spaces: this.state.PlantSpaces,
          }
      ,
      }
      
    }
   
    this.props.dispatch(userUpdateApiAction(gardenInfo)).then(            
        () => this.props.navigation.navigate("MyProfile")
      ).catch(err => {
        alert('Error', err)
        console.log("auth" , "......")     

      })
    
}

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
        <ScrollView
          contentContainerStyle={{ paddingHorizontal: 10, paddingVertical: 5 }}
        >
          {this.state.AccordionData.map((item, key) => (
            <Expandable_ListView
              key={item.category_Name}
              onClickFunction={this.update_Layout.bind(this, key)}
              item={item}
              selectedItems={item.category_Name === 'Growing Methods' ? this.state.GrowingMethods : item.category_Name === 'Soil ammendments' ? this.state.Soilammendments : this.state.PlantSpaces}
              onItemSelect={(title, item, selectedItems)=>this.selectedListItem(title, item, selectedItems)}
              state={this.state}
            />
          ))}
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
            <CircularCornerButton
                title='Submit'
                style={{ width: Dimensions.get('screen').width - 40 }}
                action={() => this.handleSubmit()}
                disabled={
                    //this.state.auth.farmName === '' ||
                    Lodash.isEmpty(this.state.GrowingMethods) ||
                    Lodash.isEmpty(this.state.Soilammendments) ||
                    Lodash.isEmpty(this.state.PlantSpaces)
                } 
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStatetoProps = (state) => {
  return {
    auth: state.authreducers.auth,
    gardenInfo: state.farmReducers.gardenInfo
  }
}

export default connect(mapStatetoProps, null)(GardeInfoScreeen)

const Styles = StyleSheet.create({
  commen: {
    marginLeft: 5
  },
  container: {
    flex: 1,
    marginTop: 5,
    marginHorizontal: 16
  },
  item: {
    backgroundColor: "white",
    marginVertical: 2
  },
  header: {
    fontSize: 32
  },
  title: {
    fontSize: 16,
    padding: 5
  },
  sectionHeader: {
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
    backgroundColor: "black"
  }
});
