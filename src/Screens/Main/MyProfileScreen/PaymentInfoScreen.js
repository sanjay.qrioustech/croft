import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, Alert, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CommonTextInputMultiline from 'library/components/commonInputTextMultiline'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import ForTextInput from '../../../Library/components/ForTextInput'
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import { connect } from 'react-redux'
import * as action from '../../../store/actions';
import Loader from 'library/components/loader'


const STRIPE_ERROR = 'Payment service error. Try again later.';
const SERVER_ERROR = 'Server error. Try again later.';
const STRIPE_PUBLISHABLE_KEY = 'pk_test_rSsOmdBHEarq2CfGId4zHYYJ00zuiJvqy1';
/**
 * The method sends HTTP requests to the Stripe API.
 * It's necessary to manually send the payment data
 * to Stripe because using Stripe Elements in React 
 * Native apps isn't possible.
 *
 * @param creditCardData the credit card data
 * @return Promise with the Stripe data
 */
const getCreditCardToken = (creditCardData) => {
    const card = {
        'card[number]': creditCardData.values.number.replace(/ /g, ''),
        'card[exp_month]': creditCardData.values.expiry.split('/')[0],
        'card[exp_year]': creditCardData.values.expiry.split('/')[1],
        'card[cvc]': creditCardData.values.cvc
    };
    return fetch('https://api.stripe.com/v1/tokens', {
        headers: {
            // Use the correct MIME type for your server
            Accept: 'application/json',
            // Use the correct Content Type to send data to Stripe
            'Content-Type': 'application/x-www-form-urlencoded',
            // Use the Stripe publishable key as Bearer
            Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`
        },
        // Use a proper HTTP method
        method: 'post',
        // Format the credit card data to a string of key-value pairs
        // divided by &
        body: Object.keys(card)
            .map(key => key + '=' + card[key])
            .join('&')
    }).then(response => response.json());
};
/**
 * The method imitates a request to our server.
 *
 * @param creditCardToken
 * @return {Promise<Response>}
 */
const subscribeUser = (creditCardToken) => {
    return new Promise((resolve) => {
        console.log('Credit card token\n', creditCardToken);
        setTimeout(() => {
            resolve({ status: true });
        }, 1000)
    });
};


class PaymentInfoScreen extends Component {
    static navigationOptions = {
        title: 'Payment Info',
        headerTintColor: colors.theme,

        headerTitleStyle: {
            fontWeight: 'bold',
        },

    };

    state = {
        card: {},
        paymentInfo:{
            user_id:this.props.Auth._id,
            token:'',
            email:this.props.Auth.email
        },
        isLoading : false
    }

    _onChange = (form) => {
        this.setState({ card: form })
        console.log(form);
    }

    // Handles submitting the payment request
    _onSubmit = async (creditCardInput) => {
       // this.setState({ card: form })
        this.setState({ isLoading : true })
        let creditCardToken;
        try {
            // Create a credit card token
            creditCardToken = await getCreditCardToken(this.state.card);
            this.setState({
                ...this.state,
                paymentInfo:{
                    ...this.state.paymentInfo,
                    token:creditCardToken.id
                }
            })
            console.log("TOKEN : ", creditCardToken)
            if (creditCardToken.error) {
                console.log('ERROR===>',creditCardToken.error)
                // Reset the state if Stripe responds with an error
                // Set submitted to false to let the user subscribe again
                this.setState({ submitted: false, error: STRIPE_ERROR });
                return;
            }
            this.props.dispatch(action.paymentApiAction(this.state.paymentInfo)).then(            
                res => {
                    console.log('CARD=>',res.data)
                    this.setState({ isLoading : false })
                    if(res.data.status === 1){
                        //this.props.Auth.isCardUpdated=true
                        this.props.dispatch(action.userupdateaction(res.data.data))
                        Alert.alert("Sucess","Card Detail Updated",
                        [
                            {text: 'OK', onPress: () => {
                                this.props.navigation.goBack();
                            }},
                        ],
                        {cancelable: false})
                        
                        //this.props.navigation.navigate('ReviewOrder')
                    }
                }
              ).catch(err => {
                //alert('Error', err)
                console.log("auth" , err)     
                this.setState({ isLoading : false })

              })
        } catch (e) {
            // Reset the state if the request was sent with an error
            // Set submitted to false to let the user subscribe again
            this.setState({ submitted: false, error: STRIPE_ERROR, isLoading : false });
            return;
        }

        // console.log("TOKEN : ", creditCardToken)

        // const { error } = await subscribeUser(creditCardToken);
        // // Handle any errors from your server
        // if (error) {
        //     this.setState({ submitted: false, error: SERVER_ERROR });
        // } else {
        //     this.setState({ submitted: false, error: null });
        //     navigation.navigate('Home')
        // }

        //this.props.navigation.navigate('MyProfile')

        

    }




    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        {/* <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Card Number ' placeholder='Required' />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Account Number' placeholder='at least 8 characters' isSecure={true} />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Expire Date' placeholder='Required' />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='CVC' placeholder='Required' />
                        <ForTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Adddress' placeholder='Required' /> */}

                        {/* <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Card Number ' placeholder='Required' />
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Account Number' placeholder='at least 8 characters' isSecure={true} />
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Expire Date' placeholder='Required' />
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Adddress' placeholder='Required' />
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='CVC' placeholder='Required' /> */}

                        <CreditCardInput onChange={this._onChange} requiresName={true} allowScroll={true} />


                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton disabled={!this.state.card.valid} title='Submit' style={{ width: Dimensions.get('screen').width - 40 }} action={() => this._onSubmit()} />
                        </View>


                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
                </KeyboardAvoidingView>
                {this.state.isLoading ? <Loader/> : null}
            </SafeAreaView>
        );
    }

}

const Styles = StyleSheet.create({
    commen: {
        marginLeft: 5,
    }

});


const mapStateToProps = (state) => {
    return {
        Auth: state.authreducers.auth
    }
}

export default connect(mapStateToProps, null)(PaymentInfoScreen)