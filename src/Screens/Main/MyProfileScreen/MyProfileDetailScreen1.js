import React, { Component } from 'react'
import { SafeAreaView, View, Image, Alert, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet } from 'react-native'

import CommonTextInput from 'library/components/commonInputText'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import { Colors } from 'react-native/Libraries/NewAppScreen'

import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import assets from 'res/Assets'
import colors from 'res/Colors'
import Divider from 'library/components/divider'
import DividerSmall from 'library/components/divider_small_margin'
import Horizontalcommontext from 'library/components/HorizontalcommonText'
import HorizontalcommonTextLarge from 'library/components/HorizontalcommonText_large'
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { connect } from "react-redux";
import * as action from '../../../store/actions';
// import Myprofile from 'library/components/Myprofile'
import MyProfile from '../../../Library/components/myProfile'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'


class MyProfileDetailScreen1 extends Component {
    state = {
        popupvisible: false
    }
    render() {
        console.log('AUTH===>',this.props.auth);
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.backgroundGray }}>
                <ScrollView style={{ backgroundColor: 'white' }}>
                    <View>

                        <MyProfile text='About Me' action={() => this.props.navigation.navigate('AboutMe')}></MyProfile>
                        <Divider />

                        <MyProfile text='Garden Info' action={() => this.props.navigation.navigate('GardeInfoScreeen')}></MyProfile>
                        <Divider />

                        <MyProfile text='Pickup and Delivery Options' action={() => this.props.navigation.navigate('PickupdeliveryScreen')}></MyProfile>
                        <Divider />

                        <MyProfile text='Bank Details' action={() => this.props.navigation.navigate('BankDetailsScreen')}></MyProfile>
                        <Divider />

                        <MyProfile text='Payment Info' action={() => this.props.navigation.navigate('PaymentInfoScreen')}></MyProfile>
                        <Divider />

                        <MyProfile text='Change Password' action={() => this.props.navigation.navigate('ChangePassword')}></MyProfile>
                        <Divider />

                        <MyProfile text='Feedback' action={() => this.props.navigation.navigate('FeedbackScreen')}></MyProfile>
                        <Divider />

                        <TouchableOpacity onPress={() => Alert.alert(
                            'Logout',
                            'Are you sure want to logout?',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                {
                                    text: 'OK', onPress: () => {
                                        console.log('navigate', this.props)
                                        this.props.navigation.navigate('Auth')
                                        this.props.dispatch(action.logout())

                                    }
                                },
                            ],
                            { cancelable: false },
                        )}>
                            <View style={[Styles.containerSingle, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }]}>
                                <SimpleText style={Styles.titleSingle} text='Logout'></SimpleText>
                                <MaterialIcons name='keyboard-arrow-right' size={20} style={{ marginRight: 10, color: '#b6b6b6' }}></MaterialIcons>

                            </View>
                        </TouchableOpacity>

                        <View style={{ height: 100 }}></View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }

}

const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row', marginLeft: 10, marginTop: 10
    },
    containerSingle: {
        flexDirection: 'row', marginTop: 5, marginBottom: 5
    },
    title: {
        fontWeight: 'bold', color: colors.theme, fontSize: 14, marginRight: 10
    },
    titleSingle: {
        fontWeight: 'bold', paddingTop: 10, paddingHorizontal: 10, color: colors.black, alignSelf: 'center', fontSize: 14
    },
    subContainer:
    {
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 5, paddingLeft: 10
    },
    subTitle: {
        marginTop: 2, fontWeight: 'bold', paddingLeft: 15, color: Colors.text, fontSize: 13
    },
    text: {
        marginTop: 2, paddingLeft: 15, color: Colors.text, fontSize: 13,
    },
    view: {
        padding: 6,
        marginTop: 6
    }

});

const mapStateToProps = (state) => {
    return {
        auth: state.authreducers.auth
    };
};

export default connect(mapStateToProps, null)(MyProfileDetailScreen1);