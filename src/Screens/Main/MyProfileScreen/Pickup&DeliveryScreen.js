import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, Alert } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import SimpleText from 'library/components/SimpleText'
import { TextInput } from 'react-native-gesture-handler'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import TimePicker from 'react-native-simple-time-picker';
import { connect } from 'react-redux';
import moment from 'moment'
import isEmpty from 'lodash/isEmpty'
import Geocoder from 'library/Geocoder/Geocoder';
import * as action from '../../../store/actions';
import { CheckBox } from 'react-native-elements'

class PickupdeliveryScreen extends Component {
    componentDidMount() {
        Geocoder.init("AIzaSyCuO2RORekXW5FqBJDb_bDfPtSvu3hTp54");
        // this.setState({

        //     selectedPickup: this.state.auth.farm.deliveryType.includes('pickup','delivery') ? false : this.state.auth.farm.deliveryType.includes('pickup') ? true : false ,
        //     selectedDelivery: this.state.auth.farm.deliveryType.includes('pickup','delivery') ? false : this.state.auth.farm.deliveryType.includes('delivery') ? true : false,
        //     selectedPickupandDelivery:this.state.auth.farm.deliveryType.includes('pickup','delivery')

        // })
        this.setState({

            // selected: this.state.auth.farm.deliveryType.includes('pickup','delivery') ? 'pickupanddelivery' : this.state.auth.farm.deliveryType.includes('pickup') ? 'pickup' : this.state.auth.farm.deliveryType.includes('delivery') ? 'delivery':'pickup' ,
            selected: this.state.auth.farm.deliveryType.includes('pickup') && this.state.auth.farm.deliveryType.includes('delivery') ? 'pickupanddelivery' : this.state.auth.farm.deliveryType.includes('pickup') ? 'pickup' : 'delivery',

        })
        const daysFullArr = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
        if (this.state.auth.farm.days) {
            this.setState({
                days: daysFullArr.map(item => {
                    let data = {
                        name: item.substr(0, 3).toUpperCase(),
                        selected: this.state.auth.farm.days.includes(item)
                    }

                    return data

                })
            })
        }
    }

    state = {
        // selectedHours: 0,
        // //initial Hours
        // selectedMinutes: 0,
        // //initial Minutes
        startTime: '00:00',
        endTime: '00:00',
        // selectedPickup: true,
        // selectedDelivery: true,
        // selectedPickupandDelivery:true,
        // isDatePickerVisibleStart: false,
        selected: 'pickup',

        isDatePickerVisibleEnd: false,

        days: [
            {
                name: 'SUN',
                selected: false
            },
            {
                name: 'MON',
                selected: false
            },
            {
                name: 'TUE',
                selected: false
            },
            {
                name: 'WED',
                selected: false
            },
            {
                name: 'THU',
                selected: false
            },
            {
                name: 'FRI',
                selected: false
            },
            {
                name: 'SAT',
                selected: false
            },
        ],
        auth: {
            user_id: "",
            fname: "",
            lname: "",
            address: "",
            city: "",
            state: "",
            zipcode: "",
            farm: {
                pickup: {
                    address: "",
                    city: "",
                    state: "",
                    zipcode: "",

                },
                address: "",
                lat: "",
                long: "",
                days: [],
                time: {
                    from: '00:00',
                    to: '00:00'
                },
                delivery_range: "",
                delivery_charge: "",
                gardeninfo: [{ growing_method: [] }, { soil_ammendments: [] }, { plant_spaces: [] }],
                about_farm: ""
            },

            ...this.props.auth
        }
    }

    static navigationOptions = {
        title: 'Pickup and Delivery',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    daysMaintain(item, key) {
        let days = this.state.days;
        item.selected === true ? days[key].selected = false : days[key].selected = true;
        this.setState({
            days: days
        })
    }

    _handleDatePicked = time => {
        this.setState({ isDatePickerVisibleEnd: false })
        console.log(time)
    };
    handleAddressUpdate(text) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    pickup: {
                        ...this.state.auth.farm.pickup,
                        address: text
                    }
                }
            }
        })
    }

    handleCityUpdate(text) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    pickup: {
                        ...this.state.auth.farm.pickup,
                        city: text
                    }
                }
            }
        })
    }
    handleStateUpdate(text) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    pickup: {
                        ...this.state.auth.farm.pickup,
                        state: text
                    }
                }
            }
        })
    }
    handleZipcodeUpdate(text) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    pickup: {
                        ...this.state.auth.farm.pickup,
                        zipcode: text
                    }
                }
            }
        })
    }
    handleDeliveryChargeUpdate(text) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    delivery_charge: text
                }
            }
        })
        console.log('state', this.state)
    }
    handleDeliveryRangeUpdate(text) {
        this.setState({
            auth: {
                ...this.state.auth,
                farm: {
                    ...this.state.auth.farm,
                    delivery_range: text
                }
            }
        })
    }
    handleTime(date, key) {
        if (key === 'from') {

            let to = moment(this.state.auth.farm.time ? this.state.auth.farm.time.to : '00:00', 'hh:mm a')
            let from = moment(date)

            let diff = moment(to).diff(from, 'hours')

            if (diff > 0 || !this.state.auth.farm.time) {
                this.setState({
                    isDatePickerVisibleStart: false,
                    auth: {
                        ...this.state.auth,
                        farm: {
                            ...this.state.auth.farm,
                            time: {
                                to: this.state.auth.farm.time ? this.state.auth.farm.time.to : '00:00',
                                from: moment(date).format('hh:mm a'),
                            }
                        }
                    }
                })
            } else {
                Alert.alert('Oops!', 'Please Select proper time')
            }


        } else {

            let to = moment(date)
            let from = moment(this.state.auth.farm.time ? this.state.auth.farm.time.from : '00:00', 'hh:mm a')
            
            let diff = moment(to).diff(from, 'hours')




            if (diff > 0 || !this.state.auth.farm.time) {
                this.setState({
                    isDatePickerVisibleEnd: false,
                    auth: {
                        ...this.state.auth,
                        farm: {
                            ...this.state.auth.farm,
                            time: {
                                from: this.state.auth.farm.time ? this.state.auth.farm.time.from : '00:00',
                                to: moment(date).format('hh:mm a'),
                            }
                        }
                    }
                })
            } else {
                Alert.alert('Oops!', 'Please Select proper time')
            }


        }
    }

    disableButton(farm) {
        if (
            farm.pickup &&
            farm.pickup.address &&
            farm.pickup.address.trim() !== '' &&
            farm.pickup.city &&
            farm.pickup.city.trim() !== '' &&
            farm.pickup.state &&
            farm.pickup.state.trim() !== '' &&
            farm.pickup.zipcode &&
            farm.pickup.zipcode.trim() !== '' &&
            !isEmpty(this.state.days.filter((item) => {
                return item.selected
            })) &&
            farm.time &&
            farm.from !== '00:00' &&
            farm.to !== '00:00' &&
            farm.delivery_range &&
            farm.delivery_range.trim() !== '' &&
            farm.delivery_charge &&
            farm.delivery_charge.trim() !== ''
        ) {

            // if (this.state.selectedDelivery || this.state.selectedPickup || this.state.selectedPickupandDelivery) {
            //     return false
            // } else {
            //     return true
            // }

        } else {
            return true
        }
    }


    handleSubmit() {
        let ordertype = []
        if (this.state.selected === 'delivery') {
            ordertype.push('delivery')
        }
        if (this.state.selected === 'pickup') {
            ordertype.push('pickup')
        }
        if (this.state.selected === 'pickupanddelivery') {
            ordertype.push('pickup', 'delivery')
        }
        const add = `${this.state.auth.farm.pickup.address},${this.state.auth.farm.pickup.city},${this.state.auth.farm.pickup.state},${this.state.auth.farm.pickup.zipcode}`
        Geocoder.from(add)
            .then(json => {
                console.log('geocode', json)
                var location = json.results[0].geometry.location;
                console.log(location);
                this.setState({
                    auth: {
                        ...this.state.auth,
                        user_id: this.state.auth._id,
                        farm: {
                            ...this.state.auth.farm,
                            pickup: {
                                ...this.state.auth.farm.pickup,
                                lat: location.lat,
                                long: location.lng,
                            },
                            deliveryType: ordertype,
                            days: this.state.days.map((item) => {
                                let day = ''
                                if (item.selected) {
                                    switch (item.name) {
                                        case 'SUN':
                                            day = 'sunday'
                                            break;
                                        case 'MON':
                                            day = 'monday'
                                            break;
                                        case 'TUE':
                                            day = 'tuesday'
                                            break;
                                        case 'WED':
                                            day = 'wednesday'
                                            break;
                                        case 'THU':
                                            day = 'thursday'
                                            break;
                                        case 'FRI':
                                            day = 'friday'
                                            break;
                                        case 'SAT':
                                            day = 'saturday'
                                            break;
                                        default:
                                            break;
                                    }

                                    return day
                                }
                            }).filter((item) => item !== undefined)
                        }
                    }
                })
                console.log(this.state.auth)
                this.props.dispatch(action.userUpdateApiAction(this.state.auth)).then(
                    () => {
                        Alert.alert('Done', 'Profile Updated Successfully')
                        this.props.navigation.navigate("MyProfile")
                    }
                ).catch(err => {
                    Alert.alert('Error', err)
                    console.log("auth", "......")

                })
            })
            .catch(error => {
                console.warn(error)
                Alert.alert("Oops!", 'Please fill proper address.')
            });
    }

    render() {

        return (
            // <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
            //     <ScrollView style={{ backgroundColor: colors.backgroundGray }}>
            //         <View style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
            //             <View style={{ flex: 1, alignItems: 'center', backgroundColor: colors.backgroundGray }}>

            //                 <View style={{backgroundColor:colors.white,width:'100%'}}>
            //                     <SimpleText style={{ color: colors.theme, marginVertical: 10,fontSize:20,fontWeight:'bold',paddingLeft:10}} text='Hours of Operation '></SimpleText>
            //                 </View>
            //                 <View style={{ height: 90, width: '100%', justifyContent: 'center', backgroundColor: 'white', paddingLeft: 10 }}>
            //                     <SimpleText style={Styles.titleSingle} text='Days '></SimpleText>


            //                     <ScrollView horizontal={true}>

            //                         {
            //                             this.state.days.map((item, key) =>
            //                                 (
            //                                     <TouchableOpacity key={key} onPress={() => this.daysMaintain(item, key)}>
            //                                         <View style={{
            //                                             width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
            //                                             justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5, backgroundColor: item.selected === true ? colors.theme : colors.white
            //                                         }}>
            //                                             <SimpleText style={{ fontWeight: 'bold', color: item.selected === true ? colors.white : colors.text, fontSize: 8 }} text={item.name}></SimpleText>
            //                                         </View>
            //                                     </TouchableOpacity>
            //                                 ))
            //                         }

            //                     </ScrollView>
            //                 </View>
            //                 <CommonTextInput
            //                     style={{ width: Dimensions.get('screen').width - 20 }}
            //                     name='Address'
            //                     placeholder='Required'
            //                     value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.address : ''}
            //                     onTextChange={(text) => this.handleAddressUpdate(text)}
            //                 />
            //                 <CommonTextInput
            //                     style={{ width: Dimensions.get('screen').width - 20 }}
            //                     name='City'
            //                     placeholder='Required'
            //                     value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.city : ''}
            //                     onTextChange={(text) => this.handleCityUpdate(text)}
            //                 />
            //                 <CommonTextInput
            //                     style={{ width: Dimensions.get('screen').width - 20 }}
            //                     name='State'
            //                     placeholder='Required'
            //                     value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.state : ''}
            //                     onTextChange={(text) => this.handleStateUpdate(text)}
            //                 />
            //                 <CommonTextInput
            //                     style={{ width: Dimensions.get('screen').width - 20 }}
            //                     name='Zipcode'
            //                     placeholder='Required'
            //                     value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.zipcode : ''}
            //                     onTextChange={(text) => this.handleZipcodeUpdate(text)}
            //                 />
            //             </View>
            //             {/* <View style={{ height: 90, justifyContent: 'center', backgroundColor: 'white', paddingLeft: 10 }}>
            //                 <SimpleText style={Styles.titleSingle} text='Days '></SimpleText>


            //                 <ScrollView horizontal={true}>

            //                     {
            //                         this.state.days.map((item, key) =>
            //                             (
            //                                 <TouchableOpacity key={key} onPress={() => this.daysMaintain(item, key)}>
            //                                     <View style={{
            //                                         width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
            //                                         justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5, backgroundColor: item.selected === true ? colors.theme : colors.white
            //                                     }}>
            //                                         <SimpleText style={{ fontWeight: 'bold', color: item.selected === true ? colors.white : colors.text, fontSize: 8 }} text={item.name}></SimpleText>
            //                                     </View>
            //                                 </TouchableOpacity>
            //                             ))
            //                     }

            //                 </ScrollView>
            //             </View> */}

            //             <View style={{ height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
            //                 <SimpleText style={[Styles.titleSingle, { flex: 1 }]} text='Time '></SimpleText>
            //                 <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10, marginLeft: 10 }}>
            //                     <TouchableOpacity>
            //                         <Text style={{ fontSize: 22, paddingHorizontal: 10 }} onPress={() => this.setState({ isDatePickerVisibleStart: true })}>{this.state.auth.farm.time ? this.state.auth.farm.time.from : '00:00'}</Text>
            //                         {/* <Text>{selectedHours}hr:{selectedMinutes}min</Text>

            //                         <TimePicker
            //                             selectedHours={selectedHours}
            //                             //initial Hourse value
            //                             selectedMinutes={selectedMinutes}
            //                             //initial Minutes value
            //                             onChange={(hours, minutes) => this.setState({
            //                                 selectedHours: hours, selectedMinutes: minutes
            //                             })}
            //                         /> */}
            //                         <DateTimePickerModal
            //                             isVisible={this.state.isDatePickerVisibleStart}
            //                             mode="time"
            //                             onConfirm={(date) => this.handleTime(date, 'from')}
            //                             onCancel={() => this.setState({ isDatePickerVisibleStart: false })}
            //                         />

            //                     </TouchableOpacity>
            //                     <Text style={{ marginTop: 5, paddingHorizontal: 10 }}>To</Text>
            //                     <TouchableOpacity>
            //                         <Text style={{ fontSize: 22, paddingHorizontal: 10 }} onPress={() => this.setState({ isDatePickerVisibleEnd: true })}>{this.state.auth.farm.time ? this.state.auth.farm.time.to : '00:00'}</Text>
            //                         <DateTimePickerModal
            //                             isVisible={this.state.isDatePickerVisibleEnd}
            //                             mode="time"
            //                             onConfirm={(date) => this.handleTime(date, 'to')}
            //                             onCancel={() => this.setState({ isDatePickerVisibleEnd: false })}
            //                         />
            //                     </TouchableOpacity>
            //                 </View>
            //             </View>
            //             <View style={{ height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
            //                 <SimpleText style={[Styles.titleSingle, { flex: 1 }]} text='Order Type '></SimpleText>
            //                 <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginTop: 15, marginLeft: 20 }}>
            //                     <CheckBox
            //                         iconType="ionicon"
            //                         checkedIcon="md-checkbox"
            //                         uncheckedIcon="md-square-outline"
            //                         title='Delivery'
            //                         checkedColor={colors.theme}
            //                         uncheckedColor={colors.primary}
            //                         textStyle={{ fontSize: 15, fontWeight: '500' }}
            //                         // style={{marginTop:40,backgroundColor:'transparent', borderColor: 'transparent' }}
            //                         checked={this.state.selectedDelivery}
            //                         onPress={() => this.setState({ selectedDelivery: !this.state.selectedDelivery })}
            //                         containerStyle={{ padding: 0, margin: 0, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
            //                     ></CheckBox>
            //                     <CheckBox
            //                         iconType="ionicon"
            //                         checkedIcon="md-checkbox"
            //                         uncheckedIcon="md-square-outline"
            //                         title='Pickup'
            //                         textStyle={{ fontSize: 15, fontWeight: '500' }}
            //                         checkedColor={colors.theme}
            //                         uncheckedColor={colors.primary}
            //                         checked={this.state.selectedPickup}
            //                         onPress={() => this.setState({ selectedPickup: !this.state.selectedPickup })}
            //                         containerStyle={{ padding: 0, margin: 0, backgroundColor: 'transparent', borderColor: 'transparent' }}
            //                     ></CheckBox>
            //                 </View>
            //             </View>
            //             <View style={{ flexDirection: 'column', height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
            //                 <SimpleText style={Styles.titleSingle} text='Delivery Range '></SimpleText>
            //                 <View style={{ flexDirection: 'row' }}>
            //                     <View style={{
            //                         width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
            //                         justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5
            //                     }}>
            //                         <TextInput
            //                             keyboardType='number-pad'
            //                             style={{ fontWeight: 'bold', color: 'black', fontSize: 15, width: '90%', textAlign: 'center', padding: 0 }}
            //                             placeholder=''
            //                             value={this.state.auth.farm.delivery_range}
            //                             onChangeText={(text) => this.handleDeliveryRangeUpdate(text)}
            //                         ></TextInput>
            //                     </View>
            //                     <SimpleText style={{ marginTop: 15, marginLeft: 10, color: colors.theme }} text='Miles '></SimpleText>
            //                 </View>
            //             </View>

            //             <View style={{ flexDirection: 'column', height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
            //                 <SimpleText style={Styles.titleSingle} text='Delivery Charge '></SimpleText>
            //                 <View style={{ flexDirection: 'row' }}>
            //                     <View style={{
            //                         width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
            //                         justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5
            //                     }}>
            //                         <TextInput
            //                             keyboardType='number-pad'
            //                             style={{ fontWeight: 'bold', color: 'black', fontSize: 15, width: '90%', height: '100%', textAlign: 'center', padding: 0 }}
            //                             placeholder=''
            //                             value={this.state.auth.farm.delivery_charge}
            //                             onChangeText={(text) => this.handleDeliveryChargeUpdate(text)}
            //                         ></TextInput>
            //                     </View>
            //                     <SimpleText style={{ marginTop: 15, marginLeft: 10, color: colors.theme }} text='$ '></SimpleText>
            //                 </View>
            //             </View>

            //             <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20, backgroundColor: colors.backgroundGray }}>
            //                 <CircularCornerButton
            //                     title='Submit'
            //                     style={{ width: Dimensions.get('screen').width - 40 }}
            //                     action={() => this.handleSubmit()}
            //                     disabled={this.disableButton(this.state.auth.farm)}

            //                 // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.address.trim() === '' : true ||
            //                 // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.city ? this.state.auth.farm.pickup.city.trim() === '' : true : true ||
            //                 // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.state ? this.state.auth.farm.pickup.state.trim() === '' : true : true   ||
            //                 // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.zipcode ? this.state.auth.farm.pickup.zipcode.trim() === '' : true : true  ||


            //                 />
            //             </View>




            //             {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
            //                 <CircularCornerButton title='Sign Up' style={{ width: Dimensions.get('screen').width - 40 }} action={()=> {this.props.navigation.navigate('EmailVerification')}} />
            //             </View> */}
            //             {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', paddingVertical: 20, paddingLeft: 20, backgroundColor: colors.backgroundGray }}>

            //                    <Text style={{  color: colors.subtextGray, textAlign: 'left' }}>By tapping SignUp, Continue with Facebook, or Continue with Google, you agree to our <Text style={{  color: colors.theme, textAlign: 'left' }} >Terms and Conditions</Text> and <Text style={{  color: colors.theme, textAlign: 'left' }}>Privacy Statement</Text></Text>

            //             </View> */}
            //             {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
            //         </View>

            //     </ScrollView>
            // </SafeAreaView>


            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <ScrollView style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                        <View style={{ flex: 1, backgroundColor: colors.backgroundGray }}>

                            <View style={{ backgroundColor: colors.white, width: '100%' }}>
                                <SimpleText style={{ color: colors.theme, marginVertical: 10, fontSize: 20, fontWeight: 'bold', paddingLeft: 10 }} text='Hours of Operation '></SimpleText>
                            </View>
                            <View style={{ height: 90, width: '100%', justifyContent: 'center', backgroundColor: 'white', paddingLeft: 10 }}>
                                <SimpleText style={Styles.titleSingle} text='Days '></SimpleText>
                                <ScrollView horizontal={true}>
                                    {
                                        this.state.days.map((item, key) =>
                                            (
                                                <TouchableOpacity key={key} onPress={() => this.daysMaintain(item, key)}>
                                                    <View style={{
                                                        width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
                                                        justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5, backgroundColor: item.selected === true ? colors.theme : colors.white
                                                    }}>
                                                        <SimpleText style={{ fontWeight: 'bold', color: item.selected === true ? colors.white : colors.text, fontSize: 8 }} text={item.name}></SimpleText>
                                                    </View>
                                                </TouchableOpacity>
                                            ))
                                    }
                                </ScrollView>
                            </View>
                            <View style={{ height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
                                <SimpleText style={[Styles.titleSingle, { flex: 1 }]} text='Time '></SimpleText>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10, marginLeft: 10 }}>
                                    <TouchableOpacity>
                                        <Text style={{ fontSize: 22, paddingHorizontal: 10 }} onPress={() => this.setState({ isDatePickerVisibleStart: true })}>{this.state.auth.farm.time ? this.state.auth.farm.time.from : '00:00'}</Text>
                                        <DateTimePickerModal
                                            isVisible={this.state.isDatePickerVisibleStart}
                                            mode="time"
                                            onConfirm={(date) => this.handleTime(date, 'from')}
                                            onCancel={() => this.setState({ isDatePickerVisibleStart: false })}
                                        />
                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 5, paddingHorizontal: 10 }}>To</Text>
                                    <TouchableOpacity>
                                        <Text style={{ fontSize: 22, paddingHorizontal: 10 }} onPress={() => this.setState({ isDatePickerVisibleEnd: true })}>{this.state.auth.farm.time ? this.state.auth.farm.time.to : '00:00'}</Text>
                                        <DateTimePickerModal
                                            isVisible={this.state.isDatePickerVisibleEnd}
                                            mode="time"
                                            onConfirm={(date) => this.handleTime(date, 'to')}
                                            onCancel={() => this.setState({ isDatePickerVisibleEnd: false })}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ backgroundColor: colors.white, width: '100%' }}>
                                <SimpleText style={{ color: colors.theme, marginVertical: 10, fontSize: 20, fontWeight: 'bold', paddingLeft: 10 }} text='Order Options'></SimpleText>
                            </View>
                            <View style={{ height: 100, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
                                <View style={{ flexDirection: 'column', justifyContent: 'flex-start', marginLeft: 20 }}>
                                    <CheckBox
                                        iconType="ionicon"
                                        checkedIcon="md-checkbox"
                                        uncheckedIcon="md-square-outline"
                                        title='Delivery only'
                                        checkedColor={colors.theme}
                                        uncheckedColor={colors.primary}
                                        textStyle={{ fontSize: 15, fontWeight: '500' }}

                                        // checked={this.state.selectedDelivery}
                                        checked={this.state.selected === 'delivery'}
                                        // onPress={() => this.setState({ selectedDelivery: !this.state.selectedDelivery })}
                                        onPress={() => this.setState({ selected: 'delivery' })}
                                        containerStyle={{ padding: 0, margin: 0, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                    ></CheckBox>
                                    <CheckBox
                                        iconType="ionicon"
                                        checkedIcon="md-checkbox"
                                        uncheckedIcon="md-square-outline"
                                        title='Pickup only'
                                        textStyle={{ fontSize: 15, fontWeight: '500' }}
                                        checkedColor={colors.theme}
                                        uncheckedColor={colors.primary}
                                        // checked={this.state.selectedPickup}
                                        checked={this.state.selected === 'pickup'}
                                        // onPress={() => this.setState({ selectedPickup: !this.state.selectedPickup })}
                                        onPress={() => this.setState({ selected: 'pickup' })}
                                        containerStyle={{ padding: 0, margin: 0, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                    ></CheckBox>
                                    <CheckBox
                                        iconType="ionicon"
                                        checkedIcon="md-checkbox"
                                        uncheckedIcon="md-square-outline"
                                        title='Pickup and Delivery'
                                        textStyle={{ fontSize: 15, fontWeight: '500' }}
                                        checkedColor={colors.theme}
                                        uncheckedColor={colors.primary}
                                        // checked={this.state.selectedPickupandDelivery}
                                        checked={this.state.selected === 'pickupanddelivery'}
                                        // onPress={() => this.setState({ selectedPickupandDelivery: !this.state.selectedPickupandDelivery })}
                                        onPress={() => this.setState({ selected: 'pickupanddelivery' })}
                                        containerStyle={{ padding: 0, margin: 0, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                    ></CheckBox>
                                </View>
                            </View>
                            <View style={{ backgroundColor: colors.white, width: '100%' }}>
                                <SimpleText style={{ color: colors.theme, marginVertical: 10, fontSize: 20, fontWeight: 'bold', paddingLeft: 10 }} text='Pickup Location'></SimpleText>
                            </View>
                            <CommonTextInput
                                style={{ width: Dimensions.get('screen').width - 20 }}
                                name='Address'
                                placeholder='Required'
                                value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.address : ''}
                                onTextChange={(text) => this.handleAddressUpdate(text)}
                            />
                            <CommonTextInput
                                style={{ width: Dimensions.get('screen').width - 20 }}
                                name='City'
                                placeholder='Required'
                                value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.city : ''}
                                onTextChange={(text) => this.handleCityUpdate(text)}
                            />
                            <CommonTextInput
                                style={{ width: Dimensions.get('screen').width - 20 }}
                                name='State'
                                placeholder='Required'
                                value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.state : ''}
                                onTextChange={(text) => this.handleStateUpdate(text)}
                            />
                            <CommonTextInput
                                style={{ width: Dimensions.get('screen').width - 20 }}
                                name='Zipcode'
                                placeholder='Required'
                                value={this.state.auth.farm.pickup ? this.state.auth.farm.pickup.zipcode : ''}
                                onTextChange={(text) => this.handleZipcodeUpdate(text)}
                            />
                        </View>
                        {/* <View style={{ height: 90, justifyContent: 'center', backgroundColor: 'white', paddingLeft: 10 }}>
                            <SimpleText style={Styles.titleSingle} text='Days '></SimpleText>


                            <ScrollView horizontal={true}>

                                {
                                    this.state.days.map((item, key) =>
                                        (
                                            <TouchableOpacity key={key} onPress={() => this.daysMaintain(item, key)}>
                                                <View style={{
                                                    width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
                                                    justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5, backgroundColor: item.selected === true ? colors.theme : colors.white
                                                }}>
                                                    <SimpleText style={{ fontWeight: 'bold', color: item.selected === true ? colors.white : colors.text, fontSize: 8 }} text={item.name}></SimpleText>
                                                </View>
                                            </TouchableOpacity>
                                        ))
                                }

                            </ScrollView>
                        </View> */}


                        {
                            this.state.selected !== 'pickup' ?
                                <View>
                                    <View style={{ flexDirection: 'column', height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
                                        <SimpleText style={Styles.titleSingle} text='Delivery Range '></SimpleText>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{
                                                width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
                                                justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5
                                            }}>
                                                <TextInput
                                                    keyboardType='number-pad'
                                                    style={{ fontWeight: 'bold', color: 'black', fontSize: 15, width: '90%', textAlign: 'center', padding: 0 }}
                                                    placeholder=''
                                                    value={this.state.auth.farm.delivery_range}
                                                    onChangeText={(text) => this.handleDeliveryRangeUpdate(text)}
                                                ></TextInput>
                                            </View>
                                            <SimpleText style={{ marginTop: 15, marginLeft: 10, color: colors.theme }} text='Miles '></SimpleText>
                                        </View>
                                    </View>

                                    <View style={{ flexDirection: 'column', height: 90, justifyContent: 'center', backgroundColor: 'white', marginVertical: .5, paddingLeft: 10 }}>
                                        <SimpleText style={Styles.titleSingle} text='Delivery Charge '></SimpleText>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{
                                                width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
                                                justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5
                                            }}>
                                                <TextInput
                                                    keyboardType='number-pad'
                                                    style={{ fontWeight: 'bold', color: 'black', fontSize: 15, width: '90%', height: '100%', textAlign: 'center', padding: 0 }}
                                                    placeholder=''
                                                    value={this.state.auth.farm.delivery_charge}
                                                    onChangeText={(text) => this.handleDeliveryChargeUpdate(text)}
                                                ></TextInput>
                                            </View>
                                            <SimpleText style={{ marginTop: 15, marginLeft: 10, color: colors.theme }} text='$ '></SimpleText>
                                        </View>
                                    </View>
                                </View>
                                :
                                null
                        }


                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton
                                title='Submit'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                action={() => this.handleSubmit()}
                                disabled={this.disableButton(this.state.auth.farm)}

                            // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.address.trim() === '' : true ||
                            // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.city ? this.state.auth.farm.pickup.city.trim() === '' : true : true ||
                            // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.state ? this.state.auth.farm.pickup.state.trim() === '' : true : true   ||
                            // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.zipcode ? this.state.auth.farm.pickup.zipcode.trim() === '' : true : true  ||


                            />
                        </View>




                        {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton title='Sign Up' style={{ width: Dimensions.get('screen').width - 40 }} action={()=> {this.props.navigation.navigate('EmailVerification')}} />
                        </View> */}
                        {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', paddingVertical: 20, paddingLeft: 20, backgroundColor: colors.backgroundGray }}>
                            
                               <Text style={{  color: colors.subtextGray, textAlign: 'left' }}>By tapping SignUp, Continue with Facebook, or Continue with Google, you agree to our <Text style={{  color: colors.theme, textAlign: 'left' }} >Terms and Conditions</Text> and <Text style={{  color: colors.theme, textAlign: 'left' }}>Privacy Statement</Text></Text>
                            
                        </View> */}
                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    titleSingle: {
        fontWeight: 'bold', paddingTop: 10, paddingHorizontal: 10, color: colors.theme, fontSize: 14
    },

});

const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        gardenInfo: state.farmReducers.gardenInfo
    }
}

export default connect(mapStatetoProps, null)(PickupdeliveryScreen)
