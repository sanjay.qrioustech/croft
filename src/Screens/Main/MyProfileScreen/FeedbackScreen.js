import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, Button, Alert, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CommonTextInputMultiline from 'library/components/commonInputTextMultiline'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import CommonInputTextMultilineNoTitle from 'library/components/commonInputTextMultilineNoTitle'
import Divider from 'library/components/divider'
import DividerSmall from 'library/components/divider_small_margin'
import { getFeedbackApi } from '../../../store/actions';
import { connect } from 'react-redux'

import SimpleText from 'library/components/SimpleText'
import { TextInput } from 'react-native-gesture-handler'



class FeedbackScreen extends Component {
    static navigationOptions = {
        title: 'Feedback',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    state = {
        title: '',
        description: ''
    }

    handleSubmit(){
        this.props.dispatch(getFeedbackApi({
            user_id: this.props.auth._id,
            text: this.state.title,
            description: this.state.description
        })).then(() => {
            Alert.alert('Done', 'Thanks for your feedback')
            this.props.navigation.goBack()
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.white }}>
                    <View style={{ flex: 1, marginTop: 15, backgroundColor: colors.white , flexDirection:'row', alignItems: 'center',marginHorizontal: 15}}>
                        <Image resizeMode='cover' source={assets.testImageMango} style={{ width:60 , height: 60, alignSelf: 'center', marginVertical: 5, borderRadius: 60/2, overflow: 'hidden' }} />
                        <SimpleText text={this.props.auth.fname+" "+this.props.auth.lname} style={{fontWeight: 'bold', color: colors.text, marginLeft:10}} />
                    </View>
                    <View style={{ marginHorizontal:10 }}>
                        <Divider/>
                    </View>
                    <View style={{ flex: 1, marginTop: 15, backgroundColor: colors.white }}>
                        <View style={{ flexDirection: 'column', flex: 1, marginHorizontal: 15, justifyContent: 'space-between' }}>
                            <SimpleText text='Title' style={Styles.text} />
                            <TextInput value={this.state.title} style={Styles.input} onChangeText={(text)=>{this.setState({title: text})}} />
                        </View>
                    </View>
                    <View style={{ flex: 1, marginTop: 15, backgroundColor: colors.white }}>
                        <View style={{ flexDirection: 'column', flex: 1, marginHorizontal: 15, justifyContent: 'space-between' }}>
                            <SimpleText text='Description' style={Styles.text} />
                            <CommonInputTextMultilineNoTitle style={Styles.input} onChangeText={(text)=>{this.setState({description: text})}}/>
                            <Divider />
                        </View>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20 }}>
                            <CircularCornerButton
                                title='Submit'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                action={() => this.handleSubmit()}
                                disabled={
                                    this.state.title.trim() === '' ||
                                    this.state.description.trim() === ''
                                }

                            // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.address.trim() === '' : true ||
                            // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.city ? this.state.auth.farm.pickup.city.trim() === '' : true : true ||
                            // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.state ? this.state.auth.farm.pickup.state.trim() === '' : true : true   ||
                            // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.zipcode ? this.state.auth.farm.pickup.zipcode.trim() === '' : true : true  ||


                            />
                        </View>
                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        auth: state.authreducers.auth
    }
}

export default connect(mapStateToProps,null)(FeedbackScreen)

const Styles = StyleSheet.create({
    commen: {
        marginLeft: 5,
    },
    titleSingle: {
        paddingTop: 10, paddingHorizontal: 10, color: 'black', alignSelf: 'center', fontSize: 18, marginBottom: 20, flex: 1
    },
    text: {
        color: colors.subtextGray,
    },
    input: {
        borderBottomWidth: 3, borderBottomColor: colors.backgroundGray, width: '100%', paddingBottom: 5,marginTop:10
    },

});