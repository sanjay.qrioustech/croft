import React, { Component } from 'react'
import { SafeAreaView, View, Image, Alert, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet } from 'react-native'

import CommonTextInput from 'library/components/commonInputText'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import { Colors } from 'react-native/Libraries/NewAppScreen'

import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import assets from 'res/Assets'
import colors from 'res/Colors'
import Divider from 'library/components/divider'
import DividerSmall from 'library/components/divider_small_margin'
import Horizontalcommontext from 'library/components/HorizontalcommonText'
import HorizontalcommonTextLarge from 'library/components/HorizontalcommonText_large'
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { connect } from "react-redux";
import * as action from '../../../store/actions';


class MyProfileDetailScreen extends Component {
    state = {
        popupvisible: false
    }
    render() {
        console.log(this.props.auth);
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.backgroundGray }}>
                <ScrollView style={{ backgroundColor: 'white' }}>
                    <View>
                        <View>
                            <View style={Styles.container}>
                                <SimpleText style={Styles.title} text='About Me'></SimpleText>
                                <Ionicons name="md-create" size={20} color={colors.theme} onPress={() => { this.props.navigation.navigate('AboutMe') }} />
                            </View>
                            {/* <View style={Styles.subContainer}>
                                <SimpleText style={Styles.subTitle} text='Farm Name :'></SimpleText>
                                <SimpleText style={Styles.text} text="Jon's Farm"></SimpleText>
                            </View>
                            <View style={Styles.subContainer}>
                                <SimpleText style={Styles.subTitle} text='FullName :'></SimpleText>
                                <SimpleText style={Styles.text} text='Jon Doe'></SimpleText>
                            </View>
                            <View style={Styles.subContainer}>
                                <SimpleText style={Styles.subTitle} text='Location : '></SimpleText>
                                <SimpleText style={Styles.text} text='21 st Main Street'></SimpleText>
                            </View>
                            <View style={Styles.subContainer}>
                                <SimpleText style={Styles.subTitle} text='About Farm : '></SimpleText>
                                <SimpleTe
                                xt style={Styles.text} text='Description of farm'></SimpleText>
                            </View> */}
                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Farm Name :' text={this.props.auth.farm.name} placeholder='Required' />
                            </View>
                            <DividerSmall />
                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='FullName :' text={this.props.auth.fname + " " + this.props.auth.lname} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Location : ' text={this.props.auth.address_list[0].address} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='About Farm :' text={this.props.auth.farm.about_farm} placeholder='Required' />
                            </View>

                        </View>

                        <Divider />

                        <View>
                            <View style={Styles.container}>
                                <SimpleText style={Styles.title} text='Garden Info'></SimpleText>
                                <Ionicons name="md-create" size={20} color={colors.theme} onPress={() => { this.props.navigation.navigate('GardeInfoScreeen') }} />
                            </View>



                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Growing Methods :' text={this.props.auth.farm.gardeninfo ? this.props.auth.farm.gardeninfo.growing_method.join(', ') : ''} placeholder='Required' />
                            </View>
                            <DividerSmall />
                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Soil Ammendments :' text={this.props.auth.farm.gardeninfo ? this.props.auth.farm.gardeninfo.soil_ammendments.join(', ') : ''} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Plant Space : ' text={this.props.auth.farm.gardeninfo ? this.props.auth.farm.gardeninfo.plant_spaces.join(', ') : ''} placeholder='Required' />
                            </View>
                        </View>

                        <Divider />
                        <View>
                            <View style={Styles.container}>
                                <SimpleText style={Styles.title} text='Pickup and Delivery Options'></SimpleText>
                                <Ionicons name="md-create" size={20} color={colors.theme} onPress={() => { this.props.navigation.navigate('PickupdeliveryScreen') }} />
                            </View>

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Pickup Location :' text={this.props.auth.farm.pickup ? `${this.props.auth.farm.pickup.address}, ${this.props.auth.farm.pickup.city}, ${this.props.auth.farm.pickup.state}, ${this.props.auth.farm.pickup.zipcode}` : ''} placeholder='Required' />
                            </View>
                            <DividerSmall />
                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Days :' text={this.props.auth.farm.days.map(item => { return item.substr(0, 3).toUpperCase() }).join(' | ')} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Hours :' text={this.props.auth.farm.time ? `${this.props.auth.farm.time.from} To ${this.props.auth.farm.time.to}` : ''} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Order Type :' text={this.props.auth.farm.deliveryType ? `${this.props.auth.farm.deliveryType.join(", ")}` : ''} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Delivery Range :' text={this.props.auth.farm.delivery_range ? `${this.props.auth.farm.delivery_range} Mi` : ''} placeholder='Required' />
                            </View>
                            <DividerSmall />

                            <View style={Styles.subContainer}>
                                <HorizontalcommonTextLarge style={{ width: Dimensions.get('screen').width - 20 }} name='Delivery Charge :' text={this.props.auth.farm.delivery_charge ? `$${this.props.auth.farm.delivery_charge}` : ''} placeholder='Required' />
                            </View>


                        </View>

                        {this.props.auth.isFarmUpdated ?
                            <React.Fragment>

                                <Divider />

                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('BankDetailsScreen') }}>
                                    <View style={Styles.containerSingle}>
                                        <SimpleText style={Styles.titleSingle} text='Bank Details'></SimpleText>
                                    </View>
                                </TouchableOpacity>

                            </React.Fragment> : null}

                        <Divider />

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('PaymentInfoScreen') }}>
                            <View style={Styles.containerSingle}>
                                <SimpleText style={Styles.titleSingle} text='Payment Info'></SimpleText>
                            </View>
                        </TouchableOpacity>

                        <Divider />

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('ChangePassword') }}>
                            <View style={Styles.containerSingle}>
                                <SimpleText style={Styles.titleSingle} text='Change Password'></SimpleText>
                            </View>
                        </TouchableOpacity>

                        <Divider />

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('FeedbackScreen') }}>
                            <View style={Styles.containerSingle}>
                                <SimpleText style={Styles.titleSingle} text='Feedback'></SimpleText>
                            </View>
                        </TouchableOpacity>

                        <Divider />

                        <TouchableOpacity onPress={() => Alert.alert(
                            'Logout',
                            'Are you sure want to logout?',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                {
                                    text: 'OK', onPress: () => {
                                        console.log('navigate', this.props)
                                        this.props.navigation.navigate('Auth')
                                        this.props.dispatch(action.logout())

                                    }
                                },
                            ],
                            { cancelable: false },
                        )}>
                            <View style={Styles.containerSingle}>
                                <SimpleText style={Styles.titleSingle} text='Logout'></SimpleText>
                            </View>

                        </TouchableOpacity>

                        {/* <Dialog
                            visible={this.state.popupvisible}
                            footer={
                                <DialogFooter>
                                    <DialogButton
                                        text="CANCEL"
                                        onPress={() => { }}
                                    />
                                    <DialogButton
                                        text="OK"
                                        onPress={() => { }}
                                    />
                                </DialogFooter>
                            }
                        >
                            <DialogContent>
                                <Text>Logout</Text>
                            </DialogConte
                            nt>
                            
                        </Dialog> */}
                        <View style={{ height: 100 }}></View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }

}

const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row', marginLeft: 10, marginTop: 10
    },
    containerSingle: {
        flexDirection: 'row', marginTop: 5, marginBottom: 5
    },
    title: {
        fontWeight: 'bold', color: colors.theme, fontSize: 14, marginRight: 10
    },
    titleSingle: {
        fontWeight: 'bold', paddingTop: 10, paddingHorizontal: 10, color: colors.theme, alignSelf: 'center', fontSize: 14
    },
    subContainer:
    {
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 5, paddingLeft: 10
    },
    subTitle: {
        marginTop: 2, fontWeight: 'bold', paddingLeft: 15, color: Colors.text, fontSize: 13
    },
    text: {
        marginTop: 2, paddingLeft: 15, color: Colors.text, fontSize: 13,
    },
    view: {
        padding: 6,
        marginTop: 6
    }

});

const mapStateToProps = (state) => {
    return {
        auth: state.authreducers.auth
    };
};

export default connect(mapStateToProps, null)(MyProfileDetailScreen);