import React, { Component } from 'react'
import {Alert, SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CommonTextInputMultiline from 'library/components/commonInputTextMultiline'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import { connect } from "react-redux";
import * as action from '../../../store/actions';



  class ChangePassword extends Component {
    static navigationOptions = {
        title: 'Change Password',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    state = {
        auth: {
            old_password: '',
            new_password: '',
            confirmPassword: '',
            _id: ''

        }
    };

    handleOldPassword(old_password) {
        this.setState({ ...this.state, auth: { ...this.state.auth, old_password: old_password , _id: this.props.auth._id} })
        console.log(this.state.auth);
        
    }

    handleNewPassword(new_password) {
        this.setState({ ...this.state, auth: { ...this.state.auth, new_password: new_password } })
        console.log(this.state.auth);
    }

    handleConfirmPassword(confirmPassword) {
        this.setState({ ...this.state, auth: { ...this.state.auth, confirmPassword: confirmPassword } })
        console.log(this.state.auth);
    }

    handleSubmit = () => {
        console.log("state" +this.state)
        console.log("auth" +this.state.auth)     
        

        this.props.dispatch(action.changePasswordApiAction(this.state.auth)).then(
            () => this.props.navigation.replace("MyProfile")
          ).catch(err => {
            Alert.alert('Error', err)
            console.log("auth" + "......")     

          })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Old ' placeholder='Required'
                            isSecure={true}
                            onTextChange={old_password => this.handleOldPassword(old_password)} />
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='New ' placeholder='at least 8 characters'
                            isSecure={true}
                            onTextChange={new_password => this.handleNewPassword(new_password)} />
                        <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Confirm' placeholder='Required'
                            isSecure={true}
                            onTextChange={confirmPassword => this.handleConfirmPassword(confirmPassword)} />

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton
                                title='Submit'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                //action={() => this.props.navigation.navigate('MyProfile')}
                                action={() => this.handleSubmit()}
                                disabled={this.state.auth.old_password === '' || this.state.auth.new_password === '' || this.state.auth.confirmPassword === ''}

                            />
                        </View>


                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }

}

const Styles = StyleSheet.create({
    commen: {
        marginLeft: 5,
    }

});

const mapStateToProps = state => {
    return {
      auth: state.authreducers.auth
    };
  };
  
  export default connect(mapStateToProps, null)(ChangePassword);
  