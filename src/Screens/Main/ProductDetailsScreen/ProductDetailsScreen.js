import React, { Component } from 'react'
import { SafeAreaView, View, ScrollView, Image, Dimensions, TextInput, TouchableOpacity, Picker, KeyboardAvoidingView, Alert } from 'react-native'
import Colors from 'res/Colors'
import Assets from 'res/Assets'
import SimpleText from 'library/components/SimpleText'
import Ionicon from 'react-native-vector-icons/Ionicons'
import CircularCornerButton from 'library/components/circularCornerButton'
import ModalDropdown from 'react-native-modal-dropdown';
import { Pages } from 'react-native-pages';
import Horizontalcommontext from 'library/components/HorizontalcommonText'
import { colors } from 'react-native-elements'
import * as action from '../../../store/actions';
import { connect } from 'react-redux';
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'

class ProductDetailsScreen extends Component {
    static navigationOptions = {
        title: 'Product Details',
        headerTintColor: Colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    state = {
        quantity: ''
    }
    productDetails = this.props.navigation.getParam('data')
    handleSubmit() {
        if (this.props.cartList !== null) {
            if (this.props.cartList.products[0].product_id.user_id === this.productDetails.user_id) {
                let productInfo = {
                    owner_id: this.productDetails.user_id,
                    user_id: this.props.auth._id,
                    products: [
                        {
                            product_id: this.productDetails._id,
                            quantity: this.state.quantity,
                            total: `${Number(this.state.quantity) * Number(this.productDetails.price)}`
                        }
                    ]
                }
                this.props.dispatch(action.addToCartApiAction(productInfo)).then(() => {
                    Alert.alert('Done', 'Item added to cart successfully')
                    this.props.navigation.navigate('HomeScreen')
                })
            } else {
                Alert.alert(
                    'Oops!',
                    'This product is from different farm are you sure you want to add this. Adding this product will remove previous cart data?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                        {
                            text: 'OK', onPress: () => {
                                this.props.dispatch(action.removeAllFromCartApiAction(this.props.auth._id, this.props.cartList._id)).then(() => {

                                    let productInfo = {
                                        owner_id: this.productDetails.user_id,
                                        user_id: this.props.auth._id,
                                        products: [
                                            {
                                                product_id: this.productDetails._id,
                                                quantity: this.state.quantity,
                                                total: `${Number(this.state.quantity) * Number(this.productDetails.price)}`
                                            }
                                        ]
                                    }
                                    this.props.dispatch(action.addToCartApiAction(productInfo)).then(() => {
                                        Alert.alert('Done', 'Item added to cart successfully')
                                        this.props.navigation.navigate('HomeScreen')
                                    })
                                })

                            }
                        },
                    ],
                    { cancelable: false },
                )
            }
        } else {
            let productInfo = {
                owner_id: this.productDetails.user_id,
                user_id: this.props.auth._id,
                products: [
                    {
                        product_id: this.productDetails._id,
                        quantity: this.state.quantity,
                        total: `${Number(this.state.quantity) * Number(this.productDetails.price)}`
                    }
                ]
            }
            this.props.dispatch(action.addToCartApiAction(productInfo)).then(() => {
                Alert.alert('Done', 'Item added to cart successfully')
                this.props.navigation.navigate('HomeScreen')
            })
        }


    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, margin: 10 }}>
                    <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>
                        <ScrollView>

                            <View style={{ width: Dimensions.get('screen').width - 20, height: 200, borderRadius: 5 }}>
                                <Pages indicatorColor={Colors.theme}>
                                    {
                                        this.productDetails.image.map((item) => (
                                            <View style={{ backgroundColor: 'transparent' }}>
                                                <Image resizeMode='cover' source={{ uri: 'https://croft-products.s3-us-west-1.amazonaws.com/' + item }} style={{
                                                    width: Dimensions.get('screen').width - 20, height: 200
                                                    , marginVertical: 5, borderRadius: 5, overflow: 'hidden'
                                                }} />
                                            </View>

                                        ))
                                    }
                                </Pages>
                            </View>

                            {/* <SimpleText style={{ margin: 10, fontSize: 15 }} text="Organically grown: Yes" />
                        <SimpleText style={{ margin: 10, fontSize: 15 }} text="Available stock: 15 Pounds" />
                        <SimpleText style={{ margin: 10, fontSize: 15 }} text="Price: $5/pounds" />
                        <SimpleText style={{ margin: 10, fontSize: 15 }} text="Delivery charge: $2" /> */}

                            <View style={{ padding: 6, marginTop: 6 }}>
                                <Horizontalcommontext style={{ width: Dimensions.get('screen').width - 20 }} name='Organically grown:' text={this.productDetails.organically_grown ? 'Yes' : 'No'} placeholder='Required' />

                            </View>

                            <View style={{ padding: 6, marginTop: 6 }}>
                                <Horizontalcommontext style={{ width: Dimensions.get('screen').width - 20 }} name='Available stock:' text={this.productDetails.quantity + ' ' + this.productDetails.unit} placeholder='Required' />

                            </View>
                            <View style={{ padding: 6, marginTop: 6 }}>
                                <Horizontalcommontext style={{ width: Dimensions.get('screen').width - 20 }} name='Price:' text={'$' + this.productDetails.price + '/' + this.productDetails.unit} placeholder='Required' />

                            </View>
                            {/* <View style={{ padding: 6, marginTop: 6 }}>
                                <Horizontalcommontext style={{ width: Dimensions.get('screen').width - 20 }} name='Delivery charge:' text={'$2'} placeholder='Required' />

                            </View> */}
                            <View style={{ padding: 6, marginTop: 6 }}>
                                <Horizontalcommontext style={{ width: Dimensions.get('screen').width - 20 }} name='Description:' text={this.productDetails.description} placeholder='Required' />

                            </View>

                            {/* <View style={{ flexDirection: 'row', marginRight: 10 }}>
                                <SimpleText style={{ margin: 10, fontSize: 15 }} text="Description: " />
                                <SimpleText style={{ marginVertical: 10, paddingRight: 100, fontSize: 15 }} text="Delivery was very fast, i needed the products in one hour they delivered in time. All products are very fresh. Very nice farm. All products are very fresh and organic." />
                            </View> */}
                            <View style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
                                <SimpleText style={{ marginVertical: 10, fontSize: 13, marginLeft: 5 }} text="Quantity: " />
                                <TextInput value={this.state.quantity} style={{ marginLeft: 135, padding: 5, borderBottomWidth: 1, borderBottomColor: Colors.subtextGray, width: 50, height: 30, textAlign: 'center' }} onChangeText={(text) => {
                                    this.setState({
                                        quantity: text
                                    })
                                }}
                                    keyboardType='number-pad'
                                />
                                <TouchableOpacity disabled={true} style={{ marginLeft: 10, borderWidth: 0, height: 30, alignItems: 'center', flexDirection: 'row', borderRadius: 5 }}>
                                    <ModalDropdown
                                        defaultValue='Pounds'
                                        defaultIndex={0}
                                        style={{ marginHorizontal: 10 }}
                                        textStyle={{ fontSize: 14, color: Colors.subtextGray }}
                                        dropdownTextStyle={{ fontSize: 15 }}
                                        options={['Pounds', 'Dozens']}
                                    />
                                    {/* <SimpleText style={{ marginHorizontal: 10, fontSize: 15, fontWeight: '400' }} text="Pounds" /> */}
                                    {/* <View style={{ marginHorizontal: 10, height: 30, width: 1, backgroundColor: 'black' }}></View>
                                    <Ionicon name='md-arrow-dropdown' size={15} style={{ marginRight: 10 }} color='black' /> */}
                                </TouchableOpacity>
                                {/* <SimpleText style={{ marginVertical: 10, paddingRight: 100 ,fontSize: 15 }} text="Delivery was very fast, i needed the products in one hour they delivered in time. All products are very fresh. Very nice farm. All products are very fresh and organic." /> */}
                            </View>

                            <CircularCornerButton
                                title='Add to Cart'
                                style={{ width: Dimensions.get('screen').width - 40, marginVertical: 30 }}
                                action={() => {
                                    this.handleSubmit()

                                }}
                                disabled={
                                    this.state.quantity === '' ||
                                    Number(this.state.quantity) === 0 ||
                                    Number(this.state.quantity) > Number(this.productDetails.quantity)
                                }
                            />


                        </ScrollView>
                    </KeyboardAvoidingView>
                    <CartInfoBottomButton {...this.props} />
                </View>
            </SafeAreaView>
        )
    }
}
const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData
    }
}

export default connect(mapStatetoProps, null)(ProductDetailsScreen)
