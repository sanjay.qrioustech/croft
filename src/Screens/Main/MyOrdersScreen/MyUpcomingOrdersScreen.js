import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, FlatList, Alert } from 'react-native'
import CardView from 'react-native-cardview';

import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MyOrderCardView from 'library/components/myordercardview'
import * as action from '../../../store/actions';
import * as selector from './MyOrderSelector'
import { connect } from 'react-redux';
class MyUpcomingOrdersScreen extends Component {

    state = {
        selectedIndex: 0,
        boughtData: [],
        duplicateBoughtData: [],
        soldData: [],

    }

    componentDidMount() {
        console.log("willFocus runs")
        this.props.dispatch(action.getBoughtListApiAction(this.props.auth._id)).then((response) => {
            console.log('Bought List', response)
            if (response.data.status === 1) {
                this.setState({
                    boughtData: selector.getMyOrders(response.data.data),
                    duplicateBoughtData: response.data.data
                })
            }
        })
        // const { navigation } = this.props;
        // navigation.addListener('willFocus', async () => {
        //     console.log("willFocus runs")
            
        // });
    }
    onReject(id, message) {
        console.log(id,message)
        this.props.dispatch(action.rejectOrderApiAction(id, message)).then((resp) => {
            console.log("LOG => ", resp)
            if (resp.data.status === 1) {
                Alert.alert('Done', 'Order Rejected')
                this.props.dispatch(action.getBoughtListApiAction(this.props.auth._id)).then((response) => {
                    console.log('Bought List', response)
                    if (response.data.status === 1) {
                        this.setState({
                            boughtData: selector.getMyOrders(response.data.data),
                            duplicateBoughtData: response.data.data
                        })
                    }
                })
            }
        }).catch(err => console.log("LOG ERR => ", err))
    }

    render() {
        return (
            <ScrollView>
                <View>
                    <FlatList
                        data={this.state.boughtData}
                        keyExtractor={item => item.id}
                        ListEmptyComponent={
                            <View style={{flex: 1}}>
                                <Text style={{fontSize: 15, textAlign: 'center', padding: 20}}>No Data Found</Text>
                            </View>
                        }
                        renderItem={({ item, index }) =>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('OrderDetail',{
                            data: this.state.duplicateBoughtData[index],
                            from: 'bought',
                        }) }}>
                            <MyOrderCardView 
                            farmImage={item.farmImage}
                            total={item.total}
                            orderStatus={item.orderStatus}
                            orderDate={item.orderDate}
                            productName={item.productName}
                            farmName={item.farmName}
                            farmAddress={item.farmAddress}
                            onReject={(message) => {
                                this.onReject(item._id, message)
                            }}
                            ></MyOrderCardView>
                            </TouchableOpacity>
                        }
                    />
                    {/* <MyOrderCardView orderStatus='0'></MyOrderCardView>
                    <MyOrderCardView orderStatus='1'></MyOrderCardView>
                    <MyOrderCardView orderStatus='2'></MyOrderCardView> */}

                </View>
            </ScrollView>


        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData,
        deliveryType : state.farmReducers.cartData !== null ? state.farmReducers.cartData.farm_details.deliveryType : [],
    };
};

export default connect(mapStateToProps, null)(MyUpcomingOrdersScreen)

// 1) Use SimpleText on place of Text
// 2) Import from library, take a raferance of other screen for components, colors, assets
// 3) For MyOrder Details screen open full screen with back navigation