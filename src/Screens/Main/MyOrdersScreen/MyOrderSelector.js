import { createSelector } from 'reselect'
import moment from 'moment';
import { IMAGE_BASE_URL } from 'appconstant'

const getOrders = (Arr) => Arr

  

export const getMyOrders = createSelector(
    [ getOrders ],
    ( orders ) => {

        return orders.map(order => {
            // console.log('Selector', IMAGE_BASE_URL + order.products[0].product_id.image[0])
            console.log('Selector Order', order)
            return {
                _id: order._id,
                userName: order.user_name,
                userAddress: order.address,
                farmImage: IMAGE_BASE_URL + order.products[0].product_id.image[0],
                orderDate: moment(order.createdAt).format('DD MMM YYYY') + ' at ' + moment(order.createdAt).format('hh:mm A'),
                farmName : order.farm_name,
                farmAddress: order.farm_address.address,
                productName : order.products.map((product)=>{
                    let name = product.product_id.product_name
                    let quantity = product.quantity
                    let unit = product.product_id.unit
                    return name + ' (' + quantity + ' ' + unit + ')'
                }).join(', '),
                total: order.total,
                orderStatus: order.status.toString()
                // rating : 3,
                // isorganic : true,
                // deliverytype : order.farm.deliveryType.join(),
                // distance : (order.distance/1000).toFixed(1),
                // isopen : isopen,
                // image : order.products_images,
                // lat: order.farm.pickup.lat,
                // long: order.farm.pickup.long
            }
        })


        return addresses
    }
)