import React, { Component } from 'react'
import { SafeAreaView, View, ScrollView, FlatList, Image, Dimensions, TextInput, TouchableOpacity, Picker, KeyboardAvoidingView, Alert, Text } from 'react-native'
import Colors from 'res/Colors'
import Assets from 'res/Assets'
import SimpleText from 'library/components/SimpleText'
import Ionicon from 'react-native-vector-icons/Ionicons'
import CircularCornerButton from 'library/components/circularCornerButton'
import { Pages } from 'react-native-pages';
import Horizontalcommontext from 'library/components/HorizontalcommonText'
import { colors } from 'react-native-elements'
import * as action from '../../../store/actions';
import { connect } from 'react-redux';
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import Divider from 'library/components/divider'
import MyOrderDetailList from 'library/components/myOrderDetailList'
import moment from 'moment'

let orderDetails

class MyOrderDetail extends Component {
    static navigationOptions = {
        title: 'Order Details',
        headerTintColor: Colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    state = {
        orderArray: [
            {
                id: 1,
                name: "Mango",
                quantity: '1',
                price: "$5/Unit",
                total: '15'
            },
            {
                id: 2,
                name: "Mango",
                quantity: '1',
                price: "$5/Unit",
                total: '15'
            },

        ],
        orderDetail: this.props.navigation.getParam('data'),
        from: this.props.navigation.getParam('from'),
        productsArray: this.props.navigation.getParam('data').products.map((productsArray) => {
            return {
                name: productsArray.product_id.product_name,
                quantity: productsArray.quantity,
                price: productsArray.product_id.price,
                total: productsArray.total
            }
        })
    }
    //  productDetails = this.props.navigation.getParam('data')

    componentDidMount() {

        console.log('MyOrderDetail===>', this.state.orderDetail)
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: 'white' }}>
                    <View style={{ flex: 1, margin: 10 }}>
                        <SimpleText style={{ marginHorizontal: 10, fontSize: 22, marginTop: 5, color: Colors.black }} text={this.state.from === 'bought' ? this.state.orderDetail.farm_name : this.state.orderDetail.user_name} />
                        <SimpleText style={{ marginHorizontal: 10, marginBottom: 10, fontSize: 15, }} text={this.state.from === 'bought' ? this.state.orderDetail.farm_address.address : this.state.orderDetail.address} />
                        <Divider />

                        <SimpleText style={{ marginHorizontal: 10, marginVertical: 10, fontSize: 22, color: Colors.black }} text='Your Orders' />
                        <Divider />

                        <FlatList
                            data={this.state.productsArray}
                            keyExtractor={item => item.id}
                            ListEmptyComponent={
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontSize: 15, textAlign: 'center', padding: 20 }}>No Data Found</Text>
                                </View>
                            }
                            renderItem={({ item, index }) =>

                                <MyOrderDetailList
                                    name={item.name}
                                    quantity={item.quantity}
                                    price={item.price}
                                    total={item.total}


                                ></MyOrderDetailList>
                            }
                        />
                        {this.state.orderDetail.order_type === 'delivery' ? <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <SimpleText style={{ marginLeft: 22, color: Colors.black, fontSize: 16, marginTop: 5 }} text='Delivery Charges' />
                            </View>
                            <View>
                                <SimpleText style={{ marginHorizontal: 10, color: Colors.subtextGray, fontSize: 16, marginTop: 5 }} text={'$ ' + this.state.orderDetail.delivery_charge} />
                            </View>

                        </View> : null}
                    {this.state.orderDetail.order_type === 'delivery' ? <Divider/>: null}

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <SimpleText style={{ marginLeft: 22, color: Colors.black, fontSize: 16, marginTop: 5 }} text='Total' />
                            </View>
                            <View>
                                <SimpleText style={{ marginHorizontal: 10, color: Colors.black, fontSize: 16, marginTop: 5 }} text={'$ ' + this.state.orderDetail.total} />
                            </View>
                        </View>

                        <SimpleText style={{ marginHorizontal: 10, marginTop: 20, fontSize: 22, color: Colors.black }} text='Order Detail' />
                        <Divider />
                        <SimpleText style={{ marginHorizontal: 10, marginTop: 10, fontSize: 16, color: Colors.subtextGray }} text='Date' />
                        <SimpleText style={{ marginHorizontal: 10, fontSize: 18, color: Colors.black }} text={moment(this.state.orderDetail.createdAt).format('DD-MM-YYYY')} />

                        <SimpleText style={{ marginHorizontal: 10, marginTop: 10, fontSize: 16, color: Colors.subtextGray }} text='Order Type' />
                        <SimpleText style={{ marginHorizontal: 10, fontSize: 18, color: Colors.black }} text={this.state.orderDetail.order_type} />

                        {
                            this.state.orderDetail.order_type === 'pickup' ?
                                <View>
                                    <SimpleText style={{ marginHorizontal: 10, marginTop: 10, fontSize: 16, color: Colors.subtextGray }} text='Pickup Time' />
                                    <SimpleText style={{ marginHorizontal: 10, fontSize: 18, color: Colors.black }} text={this.state.orderDetail.time_slot} />
                                </View>
                                :
                                <View>
                                    <SimpleText style={{ marginHorizontal: 10, marginTop: 10, fontSize: 16, color: Colors.subtextGray }} text='Delivery Adderess' />
                                    <SimpleText style={{ marginHorizontal: 10, fontSize: 18, color: Colors.black }} text={this.state.orderDetail.address} />

                                    <SimpleText style={{ marginHorizontal: 10, marginTop: 10, fontSize: 16, color: Colors.subtextGray }} text='Delivery Time' />
                                    <SimpleText style={{ marginHorizontal: 10, fontSize: 18, color: Colors.black }} text={this.state.orderDetail.time_slot} />
                                </View>

                        }

                        {
                            this.state.orderDetail.reason === undefined ?
                                null
                                :
                                <View>
                                    <SimpleText style={{ marginHorizontal: 10, marginTop: 10, fontSize: 16, color: 'red' }} text={'Order Rejected : ' + this.state.orderDetail.reason} />
                                    {/* <SimpleText style={{ marginHorizontal: 10, fontSize: 18, color: 'red' }} text={this.state.orderDetail.reason} /> */}
                                </View>
                        }


                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData
    }
}

export default connect(mapStatetoProps, null)(MyOrderDetail)
