import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import MyUpComingOrdersScreen from './MyUpcomingOrdersScreen'
import MyPastOrdersScreen from './MyPastOrdersScreen'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import * as action from '../../../store/actions';
import { connect } from 'react-redux';
import * as selector from './MyOrderSelector'
class MyOrderHeaderScreen extends Component {

    static navigationOptions = {
        title: 'My Orders',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    state = {
        selectedIndex: 0,
        boughtData: [],
        soldData: [],
    }

    componentDidMount() {
        // console.log("willFocus runs")

        // const { navigation } = this.props;
        // navigation.addListener('willFocus', async () => {
        //     console.log("willFocus runs")
        //     this.props.dispatch(action.getBoughtListApiAction(this.props.auth._id)).then((response) => {
        //         console.log('Bought List', response)
        //         if (response.data.status === 1) {
        //             this.setState({
        //                 boughtData: selector.getMyOrders(response.data.data) 
        //             })
        //         }
        //     })
        //     this.props.dispatch(action.getSoldListApiAction(this.props.auth._id)).then((response) => {
        //         console.log('Sold List', response)
        //         if (response.data.status === 1) {
        //             this.setState({
        //                 soldData: selector.getMyOrders(response.data.data)
        //             })
        //         }
        //     })
        // });
    }

    render() {
        let currentView = this.state.selectedIndex == 0 ? <MyUpComingOrdersScreen {...this.props} /> : <MyPastOrdersScreen {...this.props} />

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ marginTop: 10, height: 40, alignItems: 'center', justifyContent: 'center', }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}>
                        <SegmentedControlTab
                            lastTabStyle={{ borderRadius: 5 }}
                            values={['Bought', 'Sold']}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={(index) => {
                                this.setState({
                                    selectedIndex: index
                                })
                            }}
                            borderRadius={5}
                            tabsContainerStyle={{ height: 40, backgroundColor: '#F2F2F2', width: Dimensions.get('screen').width / 1.2, borderRadius: 5, }}
                            tabStyle={{
                                borderRadius: 5,
                                backgroundColor: '#F2F2F2',
                                borderWidth: 0,
                                borderColor: 'transparent',
                            }}
                            activeTabStyle={{ backgroundColor: 'white', margin: 2, borderRadius: 5 }}
                            tabTextStyle={{ color: 'black', fontWeight: 'bold' }}
                            activeTabTextStyle={{ color: colors.theme }}
                        />
                    </View>
                </View>
                {/* <View style={{marginTop: 10, height:1, backgroundColor: '#cacaca'}}></View> */}
                {currentView}
                <CartInfoBottomButton {...this.props} />
            </SafeAreaView>
        )
    }
}

const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
    }
}

export default connect(mapStatetoProps, null)(MyOrderHeaderScreen);