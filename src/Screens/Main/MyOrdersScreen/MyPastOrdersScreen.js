import React, { Component } from 'react'
import {
    SafeAreaView,
    View,
    Image,
    Dimensions,
    ScrollView,
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Alert
} from 'react-native'
import CardView from 'react-native-cardview';

import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MyOrderCardViewWithButton from 'library/components/myordercardviewwithbutton'
import { connect } from 'react-redux';
import * as action from '../../../store/actions';
import * as selector from './MyOrderSelector'
class MyPastOredrsScreen extends Component {

    onAccept(id) {
        this.props.dispatch(action.acceptOrderApiAction(id)).then((resp) => {
            if (resp.data.status === 1) {
                Alert.alert('Done', 'Order Accepted')
                this.props.dispatch(action.getSoldListApiAction(this.props.auth._id)).then((response) => {
                    console.log('Sold List', response)
                    if (response.data.status === 1) {
                        this.setState({
                            soldData: selector.getMyOrders(response.data.data),
                            duplicateSoldData : response.data.data
                        })
                    }
                })
            }
        })
    }

    onReject(id, message) {
        console.log(id,message)
        this.props.dispatch(action.rejectOrderApiAction(id, message)).then((resp) => {
            console.log("LOG => ", resp)
            if (resp.data.status === 1) {
                Alert.alert('Done', 'Order Rejected')
                this.props.dispatch(action.getSoldListApiAction(this.props.auth._id)).then((response) => {
                    console.log('Sold List', response)
                    if (response.data.status === 1) {
                        this.setState({
                            soldData: selector.getMyOrders(response.data.data),
                            duplicateSoldData : response.data.data
                        })
                    }
                })
            }
        }).catch(err => console.log("LOG ERR => ", err))
    }

    onComplete(id) {
        let reqPara = {
            cart_id: id,
            user_id: this.props.auth._id
        }
        this.props.dispatch(action.completeOrderApiAction(reqPara)).then((resp) => {
            if (resp.data.status === 1) {
                Alert.alert('Done', 'Order Completed')
                this.props.dispatch(action.getSoldListApiAction(this.props.auth._id)).then((response) => {
                    console.log('Sold List', response)
                    if (response.data.status === 1) {
                        this.setState({
                            soldData: selector.getMyOrders(response.data.data),
                            duplicateSoldData : response.data.data
                        })
                    }
                })
            } else {
                console.log('Error', resp)
            }
        }).catch(err=>console.log('Error', err))
        console.log(id)
    }

    state = {
        selectedIndex: 0,
        boughtData: [],
        duplicateSoldData: [],
        soldData: [],
    }

    componentDidMount() {
        console.log("willFocus runs")
        this.props.dispatch(action.getSoldListApiAction(this.props.auth._id)).then((response) => {
            console.log('Sold List', response)
            if (response.data.status === 1) {
                this.setState({
                    soldData: selector.getMyOrders(response.data.data),
                    duplicateSoldData : response.data.data
                })
            }
        })
        // const { navigation } = this.props;
        // navigation.addListener('willFocus', async () => {
        //     console.log("willFocus run")

        // });
    }

    render() {
        return (
            <ScrollView>
                <View>
                    <FlatList
                        data={this.state.soldData}
                        keyExtractor={item => item.id}
                        ListEmptyComponent={
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontSize: 15, textAlign: 'center', padding: 20 }}>No Data Found</Text>
                            </View>
                        }
                        renderItem={({ item, index }) =>
                            <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('OrderDetail', {
                                    data: this.state.duplicateSoldData[index],
                                    from: 'sold',
                                })
                            }}>
                                <MyOrderCardViewWithButton
                                    farmImage={item.farmImage}
                                    total={item.total}
                                    orderStatus={item.orderStatus}
                                    orderDate={item.orderDate}
                                    productName={item.productName}
                                    farmName={item.userName}
                                    farmAddress={item.userAddress}
                                    onAccept={() => {
                                        this.onAccept(item._id)
                                    }}
                                    onReject={(message) => {
                                        this.onReject(item._id, message)
                                    }}
                                    onComplete={() => {
                                        this.onComplete(item._id)
                                    }}
                                ></MyOrderCardViewWithButton>
                            </TouchableOpacity>
                        }
                    />

                    {/* <MyOrderCardViewWithButton orderStatus='1'></MyOrderCardViewWithButton>
                    <MyOrderCardViewWithButton orderStatus='2'></MyOrderCardViewWithButton> */}
                </View>
            </ScrollView>

        );
    }
}

const Styles = StyleSheet.create({
    image: {
        height: 40,
        width: 40,
    },
    card: {
        marginHorizontal: 30,
        marginVertical: 10,
        backgroundColor: Colors.white,
        padding: 5,


    },
    farmContainer: {
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: Colors.linegray,
        borderBottomWidth: 0.5,

    }

})
const mapStateToProps = state => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData,
        deliveryType: state.farmReducers.cartData !== null ? state.farmReducers.cartData.farm_details.deliveryType : [],

    };
};

export default connect(mapStateToProps, null)(MyPastOredrsScreen)