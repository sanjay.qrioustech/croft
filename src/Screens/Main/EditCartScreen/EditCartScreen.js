import React from 'react';
import { Text, View, Image, TouchableOpacity, SafeAreaView, ScrollView, TextInput,Alert, KeyboardAvoidingView } from 'react-native';
import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Button from 'library/components/buttons'
import CircularCornerButton from 'library/components/circularCornerButton'
import EditCartItem from 'library/components/EditCartItem'
import * as action from '../../../store/actions';
import { connect } from 'react-redux';
class EditCartScreen extends React.Component {
    static navigationOptions = {
        title: 'My Cart',
        headerTintColor: Colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },

    };

    state = {
        cart_quantity: this.props.navigation.getParam('cart_quantity'),
        farm_name: this.props.navigation.getParam('farm_name'),
        quantity: '',
        image:[],
        price: '',
        product_name: '',
        unit: '',
        _id: '',
        user_id: '',
        ...this.props.navigation.getParam('data'),
        updateDisable: false
    }
    componentDidMount(){
        console.log('Edit Cart', this.props.navigation.getParam('data'))
    }
    // onTextChange = (value) => {
    //     this.setState({
    //         Quantity: value
    //     })
    // }
    onUpdateCart() {
        console.log('Qun', this.state.cart_quantity)
        console.log('Price', this.state.price)
        let productInfo={
            owner_id : this.state.user_id,
            user_id: this.props.auth._id,
            products: [
                {
                    product_id: this.state._id,
                    quantity: this.state.cart_quantity,
                    total: `${Number(this.state.cart_quantity) * Number(this.state.price)}`
                }
            ]
        }
        console.log('Product Info', productInfo)
        this.props.dispatch(action.addToCartApiAction(productInfo)).then(()=>{
            Alert.alert('Done', 'Item added to cart successfully')
            this.props.navigation.goBack()
        })
        
    }
    onRemoveCartItem(){ 
        let productInfo={
            cart_id : this.props.cartList._id,
            user_id: this.props.auth._id,
            product_id: this.state._id,
            
        }
        this.props.dispatch(action.removeFromCartApiAction(productInfo)).then(()=>{
            Alert.alert('Done', 'Item removed from cart successfully')
            this.props.navigation.goBack()
        })
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: Colors.backgroundGray }}>
                <EditCartItem
                        farmName={this.state.farm_name}
                        productName={this.state.product_name}
                        productImage={this.state.image[0]}
                        productPrice={this.state.price}
                        quantity={this.state.cart_quantity}
                        unit={this.state.unit}
                        updateDisable={this.state.updateDisable}
                        availableStock={this.state.quantity}
                        onTextChange={(text)=>{
                             let update = false
                            if (text===''){
                                update = true
                            } else if (Number(text)>Number(this.state.quantity)) {
                                update = true
                            }
                            this.setState({
                                cart_quantity: text,
                                updateDisable: update
                            })
                        }}
                        onUpdate={()=>{this.onUpdateCart()}}
                        onRemove={()=>{this.onRemoveCartItem()}}
                        />
                   {/* <FlatList
                    data={this.state.productArray}
                    keyExtractor={item => item.id}
                    style={{marginBottom:20}} 
                    renderItem={({ item }) =>
                        }
                   /> */}
                    {/* <CircularCornerButton title='Order' style={{ width: '80%', alignSelf: 'center', marginTop: 5 }} /> */}
                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView >
        );
    }
}

const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData
    }
}

export default connect(mapStatetoProps, null)(EditCartScreen)