import React, { Component } from 'react'
import { SafeAreaView, View, Text, Image, TouchableOpacity, FlatList, Alert, Modal, TouchableHighlight, SearchBar, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicon from 'react-native-vector-icons/Ionicons'
import HomePageFarmItem from 'library/components/homePageFarmItem'
import Assets from 'res/Assets'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import SimpleText from 'library/components/SimpleText'
import HomeMapScreen from './HomeMapScreen'
import { connect } from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { getGardenInfoApiAction } from '../../../store/actions/farmActions'
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import Geolocation from '@react-native-community/geolocation'
import * as action from '../../../store/actions';
import farmreducer from '../../../store/reducers/farmReducers'
import * as selector from './homeselector';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Geocoder from 'library/Geocoder/Geocoder';
import {
    check,
    PERMISSIONS,
    RESULTS,
    //openSettings,
    request,
} from 'react-native-permissions';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } } };
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } } };





class HomeScreen extends Component {


    static navigationOptions = { header: null }

    state = {
        showMap: false,
        search: '',
        address: '',
        placeid: '',
        placeLat: '',
        placeLong: '',
        modalVisible: false,
        location: {
            latitude: '',
            longitude: '',
        }
    }

    getUserLocation() {
        Geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                const currentLongitude = JSON.stringify(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = JSON.stringify(position.coords.latitude);
                this.setState({ ...this.state, location: { ...this.state.location, latitude: currentLatitude, longitude: currentLongitude } })
                Geocoder.from(currentLatitude, currentLongitude)
                    .then(json => {
                        var addressComponent = json.results[0].address_components;
                        this.setState({
                            address: addressComponent[0].long_name + ", " + addressComponent[1].long_name
                        })
                        console.log("address", addressComponent);
                    })
                    .catch(error => console.warn(error));
                this.props.dispatch(action.getFarmInfoApiAction(this.state.location)).then(
                    () => {
                        console.log('FARMINFO====>', this.props.farmsInfoArray)
                        //alert('successfull')
                    }
                ).catch(err => {
                    alert(err)
                })
            },
            (error) => alert(error.message),
            {
                enableHighAccuracy: false, timeout: 20000, maximumAge: 1000
            }
        );
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    componentDidMount() {

        const { navigation } = this.props;
        navigation.addListener('willFocus', async () => {
            console.log("willFocus runs")
            this.props.dispatch(action.getCartListApiAction(this.props.auth._id))
            if (Platform.OS === 'ios') {
                check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
                    .then(result => {
                        switch (result) {
                            case RESULTS.UNAVAILABLE:
                                console.log(
                                    'This feature is not available (on this device / in this context)',
                                );
                                break;
                            case RESULTS.DENIED:
                                request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then(result => {
                                    if (result === 'granted') {
                                        this.getUserLocation();
                                    }
                                });
                                console.log(
                                    'The permission has not been requested / is denied but requestable',
                                );
                                break;
                            case RESULTS.GRANTED:
                                this.getUserLocation();
                                console.log('The permission is granted');
                                break;
                            case RESULTS.BLOCKED:
                                console.log(
                                    'The permission is denied and not requestable anymore',
                                );
                                break;
                        }
                    })
                    .catch(error => {
                        // …
                    });
            } else {
                check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                    .then(result => {
                        switch (result) {
                            case RESULTS.UNAVAILABLE:
                                console.log(
                                    'This feature is not available (on this device / in this context)',
                                );
                                break;
                            case RESULTS.DENIED:
                                request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(
                                    result => {
                                        if (result === 'granted') {
                                            this.getUserLocation();
                                        }
                                    },
                                );
                                console.log(
                                    'The permission has not been requested / is denied but requestable',
                                );
                                break;
                            case RESULTS.GRANTED:
                                this.getUserLocation();
                                console.log('The permission is granted');
                                break;
                            case RESULTS.BLOCKED:
                                request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(
                                    result => {
                                        if (result === 'granted') {
                                            this.getUserLocation();
                                        }
                                    },
                                );
                                console.log(
                                    'The permission is denied and not requestable anymore',
                                );
                                break;
                        }
                    })
                    .catch(error => {
                        // …
                    });
            }

        });

        this.props.dispatch(getGardenInfoApiAction())
        Geocoder.init("AIzaSyCuO2RORekXW5FqBJDb_bDfPtSvu3hTp54");

    }

    updateSearch = search => {
        this.setState({ search });
    };

    autocomplete_latLong(lat,lng,address) {
        this.setState({ ...this.state, address: address , location: { ...this.state.location, latitude: lat, longitude: lng }, modalVisible: false })
        console.log(this.state.location)
        this.props.dispatch(action.getFarmInfoApiAction(this.state.location)).then(
            () => {
                console.log('FARMINFO====>', this.props.farmsInfoArray)
                //alert('successfull')
            }
        ).catch(err => {
            alert(err)
        })
    }


    render() {
        const { search } = this.state;
        let currentView = this.state.showMap ? <HomeMapScreen {...this.props} location={this.state.location} /> : (
            <FlatList
                data={this.props.farmFilteredArray}
                keyExtractor={item => item._id}
                ListEmptyComponent={
                    <View style={{ flex: 1, height: Dimensions.get('screen').height * 0.8 , justifyContent: 'center' }}>
                        <Text style={{ fontSize: 15, textAlign: 'center', padding: 20}}>Oh no! Nobody in your area has listed their food yet, be the first to farm in your area!</Text>
                    </View>
                }
                renderItem={({ item, index }) =>
                    <HomePageFarmItem
                        style={{ width: '100%' }}
                        image={item.image}
                        farmName={item.farmname}
                        farmProducts={item.productname}
                        distance={item.distance}
                        productType={'organic'}
                        orderType={item.deliverytype}
                        isOpen={item.isopen}
                        star={4}
                        openTime= {item.openTime}
                        closeTime= {item.closeTime}
                        action={() => {
                            //this.props.dispatch(action.getFarmDetailsApiAction(item._id))
                            this.props.navigation.navigate('FarmDetails', {
                                data: this.props.farmsInfoArray[index],
                                id: item._id
                            })
                        }}
                    />

                }
            />)


        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: Colors.backgroundGray }}>
                    <View style={{ flexDirection: 'row', height: 50, backgroundColor: 'white', alignItems: 'center' }}>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                            <SimpleText style={{ textAlign: 'center', color: Colors.theme, fontWeight: 'bold', fontSize: 10 }} text='DELIVERING TO' />
                            <TouchableOpacity disabled={false} style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => {
                                this.setModalVisible(true);
                            }}>
                                <SimpleText numberOfLines={2} style={{ maxWidth: '70%', textAlign: 'center', color: 'black', fontWeight: 'bold', fontSize: 15 }} text={this.state.address} />
                                <Ionicon name='ios-arrow-down' size={15} style={{ marginLeft: 5, marginTop: 3 }} color='black' />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => {
                            this.setState({
                                showMap: !this.state.showMap
                            })
                        }}>
                            {this.state.showMap ? <Ionicon name='ios-list' size={25} style={{ marginRight: 10, alignSelf: 'center' }} color={Colors.theme} /> : <Ionicon name='md-map' size={25} style={{ marginRight: 10, alignSelf: 'center' }} color={Colors.theme} />}

                        </TouchableOpacity>
                    </View>
                    {currentView}
                </View>

                <View>
                    <Modal
                        animationType='slide'
                        // transparent={false}
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            backgroundColor: 'rgba(211,211,211,0.4)',
                            justifyContent: 'flex-end',
                            // alignItems: 'center'
                        }}>
                            <View style={{ height: '70%', backgroundColor: 'white', borderTopRightRadius: 2, borderTopLeftRadius: 2 }}>
                                <View style={{ height: '7%', width: '100%', justifyContent: 'space-around', alignItems: 'center', backgroundColor: Colors.theme, flexDirection: 'row' }}>
                                    <Text style={{ color: 'white', fontSize: 16, flex: 1, alignSelf: 'center', marginLeft: 20, textAlign: 'center' }}>Select Location</Text>
                                    <AntDesign name='close' size={20} style={{ padding: 10, }} color={Colors.white} onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }}
                                    ></AntDesign>
                                </View>

                                <GooglePlacesAutocomplete
                                    placeholder='Search'
                                    minLength={2} // minimum length of text to search
                                    autoFocus={false}
                                    returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                    keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
                                    listViewDisplayed={true}    // true/false/undefined
                                    fetchDetails={true}
                                    renderDescription={row => row.description} // custom description render
                                    onPress={(data, details) => { // 'details' is provided     when fetchDetails = true
                                        console.log(details);
                                        // this.setState({
                                        //     address: data.description,
                                        //     placeid: data.place_id
                                        // });
                                        // console.log(this.state.address)
                                        this.autocomplete_latLong(details.geometry.location.lat,details.geometry.location.lng,details.address_components[0].long_name)
                                    }}


                                    //getDefaultValue={() => ''}

                                    query={{
                                        // available options: https://developers.google.com/places/web-service/autocomplete
                                        key: 'AIzaSyCuO2RORekXW5FqBJDb_bDfPtSvu3hTp54',
                                        language: 'en', // language of the results
                                        types: ('geocode') // default: 'geocode'
                                    }}

                                    styles={{
                                        textInputContainer: {
                                            width: '100%',
                                            backgroundColor: Colors.backgroundGray,
                                            height: 54
                                        },
                                        textInput: {
                                            marginLeft: 0,
                                            marginRight: 0,
                                            height: 38,
                                            fontSize: 16
                                          },
                                        description: {
                                            fontWeight: 'bold'
                                        },
                                        predefinedPlacesDescription: {
                                            color: '#1faadb'
                                        }
                                    }}

                                    currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                                    currentLocationLabel="Current location"
                                    //nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                    //GoogleReverseGeocodingQuery={{
                                    // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                    //}}
                                    // GooglePlacesSearchQuery={{
                                    //     // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                    //     rankby: 'distance',
                                    //     type: 'cafe'
                                    // }}

                                    //GooglePlacesDetailsQuery={{
                                    // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                                    //fields: 'formatted_address',
                                    //}}

                                    //filterReverseGeocodingByTypes={['postal_code', 'sublocality', 'locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

                                    debounce={0} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                // renderLeftButton={() => <Image source={Assets.testImageApple}/>}

                                />
                            </View>
                        </View>
                    </Modal>
                    <CartInfoBottomButton {...this.props} />
                </View>

            </SafeAreaView>
        )
    }
}

const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        farmsInfoArray: state.farmReducers.farmInfo,
        farmFilteredArray: selector.getFarms(state)
    }
}

export default connect(mapStatetoProps, null)(HomeScreen)