import React, { Component } from 'react'
import { SafeAreaView, View, Dimensions, TouchableOpacity, FlatList, Text, Image } from 'react-native'
import SimpleText from 'library/components/SimpleText'
import CommonInputText from 'library/components/commonInputText'
import Colors from 'res/Colors'
import { ScrollView } from 'react-native-gesture-handler'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import ReviewsListItem from 'library/components/reviewsListItem'
import Assets from 'res/Assets'
import MapView, { Marker, Callout } from 'react-native-maps';
import { Pages } from 'react-native-pages';
import HomePageFarmItem from 'library/components/homePageFarmItem'
import MarkerIcon from 'react-native-vector-icons/FontAwesome5'
import {Icon} from 'react-native-elements'
class HomeMapScreen extends Component {

    state = {
        farmsArray: [
            {
                latlng: {
                    latitude: 37.78825,
                    longitude: -122.4324,
                },
                id: 1,
                image: [ 
                    {
                        name: Assets.testImageMango
                    },
                    {
                        name: Assets.testImageApple
                    },
                    {
                        name: Assets.testImageLichi
                    },
                ],
                farmName: "John's Farm",
                farmProducts: "Mango, Apple, Orange",
                distance: "3 km",
                productType: 'Organic',
                orderType: 'Delivery, Pickup',
                isOpen: true,
                star: '4.5'
            },
            {
                latlng: {
                    latitude: 37.78834,
                    longitude: -122.4345,
                },
                id: 2,
                image: [ 
                    {
                        name: Assets.testImageLichi
                    },
                    {
                        name: Assets.testImageApple
                    },
                    {
                        name: Assets.testImageMango
                    },
                ],
                farmName: "John's Farm",
                farmProducts: "Mango, Apple, Orange",
                distance: "3 km",
                productType: 'Organic',
                orderType: 'Delivery, Pickup',
                isOpen: false,
                star: '4.0'
            }
        ],
        selectedFarm: -1,
        showFarm: false
    }

    render() {

        let unSelectedIcon = 'map-marker'
        let selectedIcon = 'map-marker-alt'
        let farm = this.props.farmFilteredArray[this.state.selectedFarm]
        let farmView = this.state.showFarm ?
            (<ScrollView style={{ width: '95%', bottom: 8, position: 'absolute', borderRadius: 10, backgroundColor: 'white' }} bounces={false}>
                <HomePageFarmItem
                    style={{ width: '100%' }}
                    image={farm.image}
                    farmName={farm.farmname}
                    farmProducts={farm.productname}
                    distance={farm.distance}
                    productType={'organic'}
                    orderType={farm.deliverytype}
                    isOpen={farm.isopen}
                    star={4}
                    action={() => {
                        //this.props.dispatch(action.getFarmDetailsApiAction(item._id))
                        this.props.navigation.navigate('FarmDetails', {
                            data: this.props.farmsInfoArray[this.state.selectedFarm],
                            id: farm._id
                        })
                    }}
                />
            </ScrollView>) :
            null
        return (
            <View style={{ flex: 1, alignItems: "center" }}>
                <MapView
                    onPress={(event) => {
                        if (event.nativeEvent.action == undefined) {
                            this.setState({
                                showFarm: false,
                                selectedFarm: -1
                            })
                        }
                    }}
                    style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute' }}
                    initialRegion={{
                        latitude: Number(this.props.location.latitude),
                        longitude: Number(this.props.location.longitude),
                        latitudeDelta: 0.3,
                        longitudeDelta: 0.35,

                    }}>
                        <Marker
                            coordinate={{
                                latitude: Number(this.props.location.latitude),
                                longitude: Number(this.props.location.longitude),
                            }}

                        >
                            <Icon type={'material-community'} name={'home-map-marker'} size={50} color={'green'} />
                        </Marker>
                    {this.props.farmFilteredArray.map((farm, index) => (
                        <Marker
                            onPress={() => {

                                this.setState({
                                    selectedFarm: index,
                                    showFarm: true
                                })
                            }}
                            coordinate={{
                                latitude: farm.lat,
                                longitude: farm.long,
                            }}

                        >
                            {this.state.selectedFarm === index ?
                                farm.isopen === true ?
                                    <MarkerIcon name={selectedIcon} size={40} color={'green'} /> : <MarkerIcon name={selectedIcon} size={40} color={'gray'} />
                                : farm.isopen === true ?
                                    <MarkerIcon name={unSelectedIcon} size={30} color={'green'} /> : <MarkerIcon name={unSelectedIcon} size={30} color={'gray'} />

                            }
                        </Marker>
                    ))}
                </MapView>
                {farmView}


            </View>
        )
    }
}

export default HomeMapScreen