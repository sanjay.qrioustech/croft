import React, { Component } from 'react'
import { SafeAreaView, View, FlatList, Text, ScrollView, TouchableOpacity } from 'react-native'
import ProductListItem from 'library/components/productListItem'
import Assets from 'res/Assets'
import SimpleText from 'library/components/SimpleText'
// import {  } from 'react-native-gesture-handler'
import colors from 'res/Colors'


class FarmStoreScreen extends Component {
    state = {
        showAddReview: false,
        starCount: 0,
        productArray: [
            {
                id: 1,
                image: Assets.testImageMango,
                name: "Mango",
                price: "$5/Unit",
                date: '01/01/2020'
            },
            {
                id: 2,
                image: Assets.testImageApple,
                name: "Apple",
                price: "$10/Unit",
                date: '01/01/2020'
            },

        ]
    };
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17 }} text="Current Products" />
                            {/* <SimpleText style={{ margin: 10, fontSize: 12, color: colors.theme }} text="View All" /> */}

                        </View>
                        <FlatList
                            data={this.props.productData.currentList}
                            keyExtractor={item => item.id}
                            ListEmptyComponent={
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontSize: 15, textAlign: 'center', padding: 20 }}>No Data Found</Text>
                                </View>
                            }
                            renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProductDetails',{
                                    data: item
                                }) }}>
                                    <ProductListItem style={{ width: '100%' }}
                                        image={item.image}
                                        name={item.product_name}
                                        price={'$' + item.price + '/' + item.unit}
                                        date={item.harvesting_date}

                                    />
                                </TouchableOpacity>
                            }
                        />
                        {/* <View style={{ backgroundColor: colors.backgroundGray }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17 }} text="Upcoming Harvests" />
                                <SimpleText style={{ margin: 10, fontSize: 12, color: colors.theme }} text="View All" />

                            </View>
                            <FlatList
                                data={this.state.productArray}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) =>
                                    <ProductListItem style={{ width: '100%' }}
                                        image={item.image}
                                        name={item.name}
                                        price={item.price}
                                        date={item.date}
                                    />}

                            />
                        </View> */}

                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}
export default FarmStoreScreen