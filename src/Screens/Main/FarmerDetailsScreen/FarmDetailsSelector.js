import { createSelector } from 'reselect'
import moment from 'moment';

const getFarmDetail = (data) => data


export const getFarmDetails = createSelector(
    [ getFarmDetail ],
    (farmDetail) => {
        let deliveryType = ''
        switch (farmDetail.farm.deliveryType.join(',')) {
            case 'delivery':
                deliveryType = 'Available for: Delivery Only'
                break;
            case 'pickup':
                deliveryType = 'Available for: Pickup Only'
                break;
            default:
                deliveryType = 'Available for: Pickup and Delivery'
                break;
        } 

        let day = dayOfWeekAsString(moment(Date.now()).weekday());
            return {
                _id: farmDetail._id,
                ownerName: farmDetail.fname + " " + farmDetail.lname,
                farmname : farmDetail.farm.name,
                growingMethod: farmDetail.farm.gardeninfo.growing_method.join(', '),
                soilAmmendment: farmDetail.farm.gardeninfo.soil_ammendments.join(', '),
                plantSpaces: farmDetail.farm.gardeninfo.plant_spaces.join(', '),
                deliveryRange: farmDetail.farm.delivery_range,
                deliveryCharge: farmDetail.farm.delivery_charge,
                operatingTime: farmDetail.farm.time.from.replace(farmDetail.farm.time.from.substr(2,3), '').toUpperCase() + " to " + farmDetail.farm.time.to.replace(farmDetail.farm.time.to.substr(2,3), '').toUpperCase(),
                days: farmDetail.farm.days,
                rating : farmDetail.avg_rating,
                reviews: farmDetail.review,
                deliverytype : deliveryType,
                //distance : farmDetail.distance,
                lat: farmDetail.farm.pickup.lat,
                long: farmDetail.farm.pickup.long
            }

    }
  )



export const dayOfWeekAsString = (dayIndex) => {
    return ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"][dayIndex];
}
