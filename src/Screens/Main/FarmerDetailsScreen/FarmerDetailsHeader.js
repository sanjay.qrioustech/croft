import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import FarmerDetailsScreen from './FarmerDetailsScreen'
import FarmStoreScreen from './FarmStore'
import { connect } from 'react-redux';
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import * as action from '../../../store/actions';

class FarmerDetailsHeader extends Component {
    static navigationOptions = {
        title: 'Farm Details',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    componentDidMount(){
        this.props.dispatch(action.getProductListApiAction(this.props.navigation.getParam('id'))).then((response) => {
            console.log('List', response)
            if (response.data.status === 1) {
                this.setState({
                    productData: response.data.data
                })

            }
        })
        
    }
    state = {
        selectedIndex: 0, 
        productData: {
            upcomingList: [],
            currentList: []
        }
    }
    render() {
        let currentView = this.state.selectedIndex == 0 ? <FarmerDetailsScreen {...this.props} productData={this.state.productData} /> : <FarmStoreScreen {...this.props} productData={this.state.productData} />

        return (
            <SafeAreaView style={{ flex: 1 ,backgroundColor:colors.white,}}>
                <View style={{marginTop: 10, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{   alignItems: 'center', justifyContent: 'center',  borderRadius: 5}}>
                        <SegmentedControlTab
                            lastTabStyle={{ borderRadius: 5}}
                            values={['Details', 'Store']}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={(index) => { 
                                this.setState({
                                    selectedIndex: index
                                })
                             }}
                            borderRadius={5}
                            tabsContainerStyle={{ height: 40, backgroundColor: '#F2F2F2',width: Dimensions.get('screen').width/1.2, borderRadius: 5,}}
                            tabStyle={{
                                borderRadius: 5,
                                backgroundColor: '#F2F2F2',
                                borderWidth: 0,
                                borderColor: 'transparent',
                            }}
                            activeTabStyle={{ backgroundColor: 'white', margin: 2, borderRadius: 5 }}
                            tabTextStyle={{ color: 'black', fontWeight: 'bold' }}
                            activeTabTextStyle={{ color: colors.theme }}
                        />
                    </View>
                </View>
                {/* <View style={{marginTop: 10, height:1, backgroundColor: '#cacaca'}}></View> */}
                { currentView }
                <CartInfoBottomButton {...this.props} />
            </SafeAreaView>
        )
    }
}
const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        //farmsDetails: selector.getFarmDetails(state)
    }
}

export default connect(mapStatetoProps, null)(FarmerDetailsHeader)