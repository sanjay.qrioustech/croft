import React, { Component } from 'react'
import { SafeAreaView, View, Dimensions, StyleSheet, TouchableOpacity, FlatList, Text, Alert, KeyboardAvoidingView } from 'react-native'
import SimpleText from 'library/components/SimpleText'
import CommonInputText from 'library/components/commonInputText'
import Colors from 'res/Colors'
import { ScrollView } from 'react-native-gesture-handler'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import ReviewsListItem from 'library/components/reviewsListItem'
import Assets from 'res/Assets'
import MapView from 'react-native-maps';
import HorizontalcommontextSamecolor from 'library/components/HorizontalcommonTextSamecolor'
import ProductListItem from 'library/components/productListItemhorizontal'
import { connect } from 'react-redux';
import * as selector from './FarmDetailsSelector';
import * as action from '../../../store/actions';
import moment from 'moment';
class FarmDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.farmDetails = selector.getFarmDetails(this.props.navigation.getParam('data'))
        this.state = {
            review: '',
            showAddReview: false,
            starCount: 0,
            days: [
                {
                    name: 'SUN',
                    selected: false
                },
                {
                    name: 'MON',
                    selected: false
                },
                {
                    name: 'TUE',
                    selected: false
                },
                {
                    name: 'WED',
                    selected: false
                },
                {
                    name: 'THU',
                    selected: false
                },
                {
                    name: 'FRI',
                    selected: false
                },
                {
                    name: 'SAT',
                    selected: false
                },
            ],
            productArray: [
                {
                    id: 1,
                    image: Assets.testImageMango,
                    name: "Mango",
                    price: "$5/Unit",
                    date: '01/01/2020'
                },
                {
                    id: 2,
                    image: Assets.testImageMango,
                    name: "Apple",
                    price: "$10/Unit",
                    date: '01/01/2020'
                },
                {
                    id: 3,
                    image: Assets.testImageApple,
                    name: "Apple",
                    price: "$10/Unit",
                    date: '01/01/2020'
                },
                {
                    id: 4,
                    image: Assets.testImageMango,
                    name: "Apple",
                    price: "$10/Unit",
                    date: '01/01/2020'
                },

            ],
            reviewsArray: []
        };

    }

    componentDidMount() {
        console.log(selector.getFarmDetails(this.props.navigation.getParam('data')))
        this.props.dispatch(action.getReviewsListApiAction(this.farmDetails._id)).then((response) => {
            console.log('ReviewList', response)
            if (response.data.status === 1) {
                this.setState({
                    reviewsArray: response.data.data
                })
            }
        })
        const daysFullArr = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
        if (this.farmDetails.days) {
            this.setState({
                days: daysFullArr.map(item => {
                    let data = {
                        name: item.substr(0, 3).toUpperCase(),
                        selected: this.farmDetails.days.includes(item)
                    }

                    return data

                })
            })
        }
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }
    daysMaintain(item, key) {
        let days = this.state.days;
        item.selected === true ? days[key].selected = false : days[key].selected = true;
        this.setState({
            days: days
        })
    }

    onAddReview() {
        if (this.state.review !== '' && this.state.starCount !== 0) {
            let reviewData = {
                "given_id": this.farmDetails._id,
                "user_id": this.props.auth._id,
                "text": this.state.review,
                "rating": this.state.starCount
            }
            this.setState({
                showAddReview: false,
                review: '',
                starCount: 0,
            })
            this.props.dispatch(action.addReviewsApiAction(reviewData)).then((response) => {
                console.log('ReviewAdded', response)
                if (response.data.status === 1) {
                    this.props.dispatch(action.getReviewsListApiAction(this.farmDetails._id)).then((response) => {
                        console.log('ReviewList', response)
                        if (response.data.status === 1) {
                            this.setState({
                                showAddReview: false,
                                review: '',
                                starCount: 0,
                                reviewsArray: response.data.data
                            })
                        }
                    })
                }
            })
        } else {
            Alert.alert('Oops!', 'Please fill all the details for review')
        }

    }

    render() {
        let ReviewView = <Text style={{ color: Colors.theme, margin: 10, fontWeight: 'bold' }} onPress={() => {
            this.setState({
                showAddReview: true
            })
        }} >Write a Review</Text>
        if (this.state.showAddReview) {
            ReviewView = <View >
                <CommonInputText style={{ width: Dimensions.get('screen').width - 10 }} name="Review" placeholder='Start a review' isFullInput={true} value={this.state.review} onChangeText={(text) => {
                    this.setState({
                        review: text
                    })
                }} />
                <StarRating
                    containerStyle={{ width: Dimensions.get('screen').width / 3, margin: 10 }}
                    animation='tada'
                    starSize={25}
                    halfStarEnabled={true}
                    disabled={false}
                    maxStars={5}
                    rating={this.state.starCount}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                    fullStarColor={Colors.theme}
                />
                <TouchableOpacity style={{ backgroundColor: Colors.theme, justifyContent: 'center', alignItems: 'center', marginHorizontal: 10, width: 70, height: 30 }} onPress={() =>
                    this.onAddReview()}>
                    <SimpleText style={{ color: 'white', fontSize: 13 }} text="Submit" />
                </TouchableOpacity>
            </View>
        } else {
            <Text style={{ color: Colors.theme }} onPress={() => {
                this.setState({
                    showAddReview: true
                })
            }} >Write a Review</Text>
        }

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
                <View style={{ margin: 10 }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        //style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                    <ScrollView keyboardDismissMode='interactive'>
                        <View style={{ marginHorizontal: 6 }}>
                            <View style={{ justifyContent: 'space-between', flexDirection: 'column' }}>
                                <View >
                                    <SimpleText style={{ fontWeight: 'bold', fontSize: 20 }} text={this.farmDetails.farmname} />

                                    <SimpleText style={{ color: Colors.subtextGray, fontSize: 13, marginLeft: 10, marginBottom: 10 }} text={`(${this.farmDetails.ownerName})`} />

                                    <HorizontalcommontextSamecolor style={{ width: Dimensions.get('screen').width - 20, marginBottom: 5 }} name='Growing Method :' text={this.farmDetails.growingMethod} placeholder='Required' />
                                    <HorizontalcommontextSamecolor style={{ width: Dimensions.get('screen').width - 20, marginBottom: 5 }} name='Plant Space :' text={this.farmDetails.plantSpaces} placeholder='Required' />
                                    <HorizontalcommontextSamecolor style={{ width: Dimensions.get('screen').width - 20, marginBottom: 5 }} name='Soil Ammendments :' text={this.farmDetails.soilAmmendment} placeholder='Required' />
                                </View>
                                <View>
                                    <StarRating
                                        containerStyle={{ width: 80, marginTop: 7 }}
                                        starSize={14}
                                        halfStarEnabled={true}
                                        disabled={true}
                                        maxStars={5}
                                        rating={this.farmDetails.rating}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        fullStarColor={Colors.theme}
                                    />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: "700" }}> ${this.farmDetails.deliveryCharge}</Text>
                                        <Text style={{ fontSize: 12 }} >charge</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', marginLeft: 16, alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: "700" }}>{this.farmDetails.deliveryRange} Mi</Text>
                                        <Text style={{ fontSize: 12 }} >Distance</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', marginLeft: 16, alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: '700' }}>{this.farmDetails.operatingTime}</Text>
                                        <Text style={{ fontSize: 12 }} >Operating hours</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'column', marginTop: 20, alignItems: 'center' }}>
                                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>{this.farmDetails.deliverytype}</Text>
                                    <Text style={{ fontSize: 12 }} >Order Type</Text>
                                </View>


                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    {
                                        this.props.productData.upcomingList.length === 0 ? null :

                                            <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17, marginTop: 20 }} text="Upcoming Harvests" />
                                    }
                                </View>
                                <FlatList

                                    data={this.props.productData.upcomingList}
                                    keyExtractor={item => item.id}
                                    horizontal={true}
                                    style={{ marginBottom: 20 }}
                                    renderItem={({ item }) =>
                                        <ProductListItem
                                            image={item.image}
                                            name={item.product_name}
                                            price={'$' + item.price + '/' + item.unit}
                                            date={item.harvesting_date}
                                        />}
                                />

                                <View style={{ height: 90, justifyContent: 'center', backgroundColor: 'white', marginTop: 8 }}>
                                    <SimpleText style={Styles.titleSingle} text='Days '></SimpleText>
                                    <ScrollView horizontal={true}>
                                        {
                                            this.state.days.map((item, key) =>
                                                (
                                                    <TouchableOpacity disabled={true} key={key} onPress={() => this.daysMaintain(item, key)}>
                                                        <View style={{
                                                            width: 30, marginLeft: 10, borderWidth: 1, height: 30, marginTop: 10,
                                                            justifyContent: 'center', alignItems: 'center', flexDirection: 'row', borderRadius: 5, backgroundColor: item.selected === true ? Colors.theme : Colors.white
                                                        }}>
                                                            <SimpleText style={{ fontWeight: 'bold', color: item.selected === true ? Colors.white : Colors.text, fontSize: 8 }} text={item.name}></SimpleText>
                                                        </View>
                                                    </TouchableOpacity>
                                                ))
                                        }

                                    </ScrollView>
                                </View>
                            </View>
                            <View style={{ marginVertical: 5, height: 200 }}>
                                <MapView
                                    scrollEnabled={false}
                                    style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute' }}
                                    initialRegion={{
                                        latitude: this.farmDetails.lat,
                                        longitude: this.farmDetails.long,
                                        latitudeDelta: 0.0122,
                                        longitudeDelta: 0.0121,
                                    }}
                                />
                            </View>

                            {/* <SimpleText style={{ marginVertical: 2.5, paddingTop: 10, color: Colors.subtextGray, fontSize: 13 }} text="Operating Hours: 10AM to 5PM" />
                            <SimpleText style={{ marginVertical: 2.5, color: Colors.subtextGray, fontSize: 13 }} text="Operating Days: Mon, Tue, Wed, Thu, Fri" />
                            <SimpleText style={{ marginVertical: 2.5, paddingTop: 10, color: Colors.subtextGray, fontSize: 13 }} text="Growing Method: Certified Organic" />
                            <SimpleText style={{ marginVertical: 2.5, color: Colors.subtextGray, fontSize: 13 }} text="Soil Ammendments: Lime" />
                            <SimpleText style={{ marginVertical: 2.5, color: Colors.subtextGray, fontSize: 13 }} text="Plant Space: Aeroponic" /> */}

                        </View>
                        {/* <View style={{ margin: 10, height: 200 }}>
                        <MapView
                        style={{ left:0, right: 0, top:0, bottom: 0, position: 'absolute' }}
                            initialRegion={{
                                latitude: 37.78825,
                                longitude: -122.4324,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                        />
                    </View> */}
                        {ReviewView}
                        <View style={{ marginHorizontal: 10, marginTop: 10 }}>
                            <SimpleText style={{ fontWeight: 'bold', marginVertical: 2.5, fontSize: 17 }} text="All Reviews" />
                        </View>
                        <FlatList
                            data={this.state.reviewsArray}
                            keyExtractor={item => item._id}
                            renderItem={({ item }) =>

                                <ReviewsListItem
                                    style={{ width: '100%' }}
                                    image={Assets.testImageMango}
                                    name={item.name}
                                    review={item.text}
                                    stars={item.rating}
                                />}
                        />

                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <SimpleText style={{ fontWeight: 'bold', margin: 10, fontSize: 17, marginTop: 10 }} text="Upcoming Harvests" />

                        </View>
                        <FlatList

                            data={this.props.productData.upcomingList}
                            keyExtractor={item => item.id}
                            horizontal={true}
                            style={{ marginBottom: 20 }}
                            renderItem={({ item }) =>
                                <ProductListItem
                                    image={item.image}
                                    name={item.product_name}
                                    price={'$'+item.price+'/'+item.unit}
                                    date={item.harvesting_date}
                                />}
                        /> */}
                    </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        )
    }
}
const Styles = StyleSheet.create({
    titleSingle: {
        fontWeight: 'bold', paddingTop: 10, paddingHorizontal: 10, color: Colors.theme, fontSize: 14
    },

});

const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        //farmsDetails: selector.getFarmDetails(state)
    }
}

export default connect(mapStatetoProps, null)(FarmDetailsScreen)
