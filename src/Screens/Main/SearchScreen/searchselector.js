import { createSelector } from 'reselect'
import moment from 'moment';

const getFarmList = (state) => state.farmReducers.farmfilteredInfo


export const getFarms = createSelector(
    [ getFarmList ],
    ( farmlist ) => {

        let day = dayOfWeekAsString(moment(Date.now()).weekday());
      
        return farmlist.map(data => {

            let isopen = false;
            if(data.farm.days.includes(day)){
                if (moment(new Date(), 'hh:mm a').isBetween(moment(data.farm.time.from, 'hh:mm a'), moment(data.farm.time.to, 'hh:mm a'))) {
                    isopen =  true;
                } else {
                    isopen =  false;
                }
                
            }else {
                isopen = false;
            }
            console.log('selector', data)
            return {
                _id: data._id,
                farmname : data.farm.name,
                productname : data.products_name,
                rating : 3,
                isorganic : true,
                deliverytype : data.farm.deliveryType.join(),
                distance : (data.distance * 0.00062137).toFixed(1)+' Mi\n $'+data.farm.delivery_charge+' delivery',
                isopen : isopen,
                image : data.products_images,
                lat: data.farm.pickup.lat,
                long: data.farm.pickup.long,
                openTime: data.farm.time.from,
                closeTime: data.farm.time.to,
            }
        })

    }
  )



export const dayOfWeekAsString = (dayIndex) => {
    return ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"][dayIndex];
}