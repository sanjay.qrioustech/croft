import React, { Component } from 'react'
import { SafeAreaView, View, Text, Alert, TouchableOpacity, FlatList, Modal, TextInput } from 'react-native'
import Colors from 'res/Colors'
import Ionicon from 'react-native-vector-icons/Ionicons'
import Foundation from 'react-native-vector-icons/Foundation'
import HomePageFarmItem from 'library/components/homePageFarmItem'
import Assets from 'res/Assets'
import { Icon } from 'react-native-elements'
import SimpleText from 'library/components/SimpleText'

import SearchBar from 'react-native-search-bar';
import { CheckBox } from 'react-native-elements'
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import { connect } from 'react-redux';
import Geocoder from 'library/Geocoder/Geocoder';
import * as action from '../../../store/actions';
import * as selector from './searchselector';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import Geolocation from '@react-native-community/geolocation'
import {
    check,
    PERMISSIONS,
    RESULTS,
    //openSettings,
    request,
} from 'react-native-permissions';

const deliveryType = ['Pickup', 'Delivery']

class SearchScreen extends Component {
    static navigationOptions = { header: null }
    state = {
        address: '',
        showMap: false,
        search: '',
        popupvisible: false,
        modalVisible: false,
        filtertype: '',
        selectedDeliveryTypeIndex: 0,
        pickup: true,
        type: [],
        organic: '',
        distance: '',
        delivery: true,
        selectedDeliveryOrganicIndex: 0,
        selectedDeliveryDistanceIndex: 0,
        farmsArray: [],
        locationmodalvisible: false,
        location: {
            latitude: '',
            longitude: '',
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    setLocationModalVisible(visible) {
        this.setState({ locationmodalvisible: visible });
    }

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = (text) => {
        this._menu.hide();
        this.setState({
            filtertype: text
        });
    };

    showMenu = () => {
        this._menu.show();
    };
    onSelect(index, value) {
        this.setState({
            text: `Selected index: ${index} , value: ${value}`
        })
    }

    // updateSearch = search => {
    //     this.setState({ search });
    //     console.log(search)
    //   };

    getFilteredList() {
        let deliveryType = []
        if (this.state.delivery) {
            deliveryType.push('delivery')
        }
        if (this.state.pickup) {
            deliveryType.push('pickup')
        }
        let filterData = {
            latitude: this.state.location.latitude,
            longitude: this.state.location.longitude,
            delivery_type: deliveryType.join(' '),
            delivery_distance: this.state.selectedDeliveryDistanceIndex === 0 ? "asc" : "desc",
            organically_grown: this.state.selectedDeliveryOrganicIndex === 0 ? "true" : "false"
        }

        this.props.dispatch(action.getFarmInfoFilterApiAction(filterData)).then(
            () => {
                console.log('FARM_FILTER_INFO====>', this.props.farmFilteredArray)
                //alert('successfull')
                this.setState({
                    farmsArray: this.props.farmFilteredArray
                })
            }
        ).catch(err => {
            alert(err)
        })
    }
    getUserLocation() {
        Geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                const currentLongitude = JSON.stringify(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = JSON.stringify(position.coords.latitude);
                this.setState({ ...this.state, location: { ...this.state.location, latitude: currentLatitude, longitude: currentLongitude } })
                Geocoder.from(currentLatitude, currentLongitude)
                    .then(json => {
                        var addressComponent = json.results[0].address_components;
                        this.setState({
                            address: addressComponent[0].long_name + ", " + addressComponent[1].long_name
                        })
                        console.log("address", addressComponent);
                    })
                    .catch(error => console.warn(error));
                this.getFilteredList()
                // this.props.dispatch(action.getFarmInfoFilterApiAction(this.state.location)).then(
                //     () => {
                //         console.log('FARMINFO====>', this.props.farmsInfoArray)
                //         //alert('successfull')
                //     }
                // ).catch(err => {
                //     alert(err)
                // })
            },
            (error) => alert(error.message),
            {
                enableHighAccuracy: false, timeout: 20000, maximumAge: 1000
            }
        );
    }

    autocomplete_latLong(lat, lng, address) {
        this.setState({ ...this.state, address: address, location: { ...this.state.location, latitude: lat, longitude: lng }, locationmodalvisible: false })
        this.getFilteredList();
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    componentDidMount() {
        console.log('search')
        console.log(this.state.farmsArray)
        this.getUserLocation();
        // 
    }
    // componentDidMount() {


    //     //this.props.dispatch(action.getGardenInfoApiAction())
    //     Geocoder.init("AIzaSyCuO2RORekXW5FqBJDb_bDfPtSvu3hTp54");

    // }

    searchText = e => {
        let text = e.toLowerCase();
        let filtertype = this.state.filtertype;

        let farms = this.props.farmFilteredArray;
        let filteredName = farms.filter(item => {

            return item.farmname.toLowerCase().match(text);
            // switch (filtertype) {
            //     case 'PickUp Type':
            //         return item.deliverytype.toLowerCase().match(text);
            //     case 'Delivery Distance':
            //         return item.distance.toLowerCase().match(text);
            //     case 'Organically Grown':
            //         return item.productType.toLowerCase().match(text);
            //     default:
            //         return item.farmname.toLowerCase().match(text);

            // }

        });
        if (!text || text === '') {
            this.setState({
                search: e,
                farmsArray: this.props.farmFilteredArray,
            });
        } else if (
            (!Array.isArray(filteredName) && !filteredName.length) ||
            filteredName.length == 0
        ) {
            // set no data flag to true so as to render flatlist conditionally
            this.setState({
                search: e,
                farmsArray: [],
            });
        } else if (Array.isArray(filteredName)) {
            this.setState({
                search: e,
                farmsArray: filteredName,
            });
        }
    };



    render() {

        let currentView = (<FlatList
            data={this.state.farmsArray}
            keyExtractor={item => item.id}
            ListEmptyComponent={
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 15, textAlign: 'center', padding: 20 }}>No Data Found</Text>
                </View>
            }
            renderItem={({ item, index }) =>

                <HomePageFarmItem
                    style={{ width: '100%' }}
                    image={item.image}
                    farmName={item.farmname}
                    farmProducts={item.productname}
                    distance={item.distance}
                    productType={'organic'}
                    orderType={item.deliverytype}
                    isOpen={item.isopen}
                    star={4}
                    openTime={item.openTime}
                    closeTime={item.closeTime}
                    action={() => {
                        //this.props.dispatch(action.getFarmDetailsApiAction(item._id))
                        this.props.navigation.navigate('FarmDetails', {
                            data: this.props.farmsInfoArray[index],
                            id: item._id
                        })
                    }}
                />

            }
        />)


        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: Colors.backgroundGray }}>
                    <View style={{ flexDirection: 'row', height: 50, backgroundColor: 'white', alignItems: 'center' }}>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                            <SimpleText style={{ textAlign: 'center', color: Colors.theme, fontWeight: 'bold', fontSize: 10 }} text='DELIVERING TO' />
                            <TouchableOpacity disabled={false} style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => {
                                this.setLocationModalVisible(true);
                            }}>
                                <SimpleText numberOfLines={2} style={{ maxWidth: '70%', textAlign: 'center', color: 'black', fontWeight: 'bold', fontSize: 15 }} text={this.state.address} />
                                <Ionicon name='ios-arrow-down' size={15} style={{ marginLeft: 5, marginTop: 3 }} color='black' />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => {
                            // <ModalDropdown
                            //     style={{ marginHorizontal: 10 }}
                            //     textStyle={{ fontSize: 14, color: Colors.subtextGray }}
                            //     dropdownTextStyle={{ fontSize: 15 }}
                            //     options={['Pounds', 'Dozens']}
                            // />
                        }}>
                            <Foundation name='filter' size={25} style={{ marginRight: 15, alignSelf: 'center' }} color={Colors.theme} onPress={() => { this.setModalVisible(true) }} />



                            {/* <Menu
                                ref={this.setMenuRef}
                                style={{ marginRight: 15, alignSelf: 'center' }}
                                button={<Foundation name='filter' size={25} style={{ marginRight: 15, alignSelf: 'center' }} color={Colors.theme} onPress={this.showMenu} />}
                            >
                                <MenuItem onPress={() => this.hideMenu('PickUp Type')}>PickUp Type</MenuItem>
                                <MenuItem onPress={() => this.hideMenu('Delivery Distance')}>Delivery Distance</MenuItem>
                                <MenuItem onPress={() => this.hideMenu('Organically Grown')}>Organically Grown</MenuItem>
                            </Menu> */}
                        </TouchableOpacity>
                    </View>



                    <SearchBar
                        ref="searchBar"
                        onChangeText={this.searchText}
                        placeholder="Search"
                    />
                    {currentView}

                    <View>
                        <Modal
                            animationType="slide"
                            // transparent={false}
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                this.setModalVisible(!this.state.modalVisible);
                            }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                backgroundColor: 'rgba(211,211,211,0.4)',
                                alignItems: 'center'
                            }}>
                                <View style={{
                                    width: '80%', backgroundColor: Colors.white, borderTopEndRadius: 10, borderTopStartRadius: 10,
                                    borderWidth: 1,
                                    borderColor: '#fff',
                                }}>
                                    <View style={{ height: '10%', width: '100%' }}>
                                        <View style={{ width: '100%', height: 40, backgroundColor: Colors.theme, borderTopEndRadius: 10, borderTopStartRadius: 10, alignItems: 'center', flexDirection: 'row' }}>
                                            <Text style={{ flex: 0.98, color: 'white', fontSize: 16, textAlign: 'center' }}>Filter</Text>
                                            <Icon style={{ alignSelf: 'center' }} name={'close'} type='material' color='white' onPress={() => {
                                                this.setState({ modalVisible: !this.state.modalVisible });
                                            }} />
                                        </View>
                                    </View>
                                    <View style={{ paddingHorizontal: 20, flexDirection: 'column' }}>
                                        <SimpleText style={{ color: 'black', fontSize: 15, }} text='Delivery Type :' />
                                        <CheckBox
                                            title='PickUp'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            // style={{marginTop:40,backgroundColor:'transparent', borderColor: 'transparent' }}
                                            checked={

                                                this.state.pickup}
                                            onPress={() => this.setState({
                                                pickup: !this.state.pickup
                                            })}
                                            containerStyle={{ marginTop: 5, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>

                                        <CheckBox
                                            title='Delivery'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            checked={
                                                this.state.delivery}
                                            onPress={() => this.setState({
                                                delivery: !this.state.delivery
                                            })}
                                            containerStyle={{ marginTop: -20, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>

                                        <SimpleText style={{ color: 'black', fontSize: 15, marginTop: 6 }} text='Delivery Distance :' />
                                        <CheckBox
                                            title='Accending'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            // style={{marginTop:40,backgroundColor:'transparent', borderColor: 'transparent' }}
                                            checked={this.state.selectedDeliveryDistanceIndex === 0}
                                            onPress={() => this.setState({ selectedDeliveryDistanceIndex: 0 })}
                                            containerStyle={{ marginTop: 5, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>

                                        <CheckBox
                                            title='Descending'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            checked={this.state.selectedDeliveryDistanceIndex === 1}
                                            onPress={() => this.setState({ selectedDeliveryDistanceIndex: 1 })}
                                            containerStyle={{ marginTop: -20, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>
                                        <SimpleText style={{ color: 'black', fontSize: 15, marginTop: 6 }} text='Organicaly Grown :' />
                                        <CheckBox
                                            title='Yes'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            // style={{marginTop:40,backgroundColor:'transparent', borderColor: 'transparent' }}
                                            checked={this.state.selectedDeliveryOrganicIndex === 0}
                                            onPress={() => this.setState({ selectedDeliveryOrganicIndex: 0 })}
                                            containerStyle={{ marginTop: 5, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>

                                        <CheckBox
                                            title='No'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            checked={this.state.selectedDeliveryOrganicIndex === 1}
                                            onPress={() => this.setState({ selectedDeliveryOrganicIndex: 1 })}
                                            containerStyle={{ marginTop: -20, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>
                                    </View>
                                </View>
                                <View style={{ height: '10%', width: '80%' }}>
                                    <TouchableOpacity style={{ width: '100%', height: 40, backgroundColor: Colors.theme, alignSelf: 'flex-end', borderBottomEndRadius: 10, borderBottomStartRadius: 10, justifyContent: 'center', alignItems: 'center' }} onPress={() => {
                                        this.setState({ modalVisible: !this.state.modalVisible });
                                        this.getFilteredList()
                                        // this.setModalVisible(!this.state.modalVisible);
                                    }}>
                                        <Text style={{ color: 'white', fontSize: 16 }} >Apply Filter</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>

                    <View>
                        <Modal
                            animationType='slide'
                            // transparent={false}
                            transparent={true}
                            visible={this.state.locationmodalvisible}
                            onRequestClose={() => {
                                this.setLocationModalVisible(!this.state.locationmodalvisible);
                            }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                backgroundColor: 'rgba(211,211,211,0.4)',
                                justifyContent: 'flex-end',
                                // alignItems: 'center'
                            }}>
                                <View style={{ height: '70%', backgroundColor: 'white', borderTopRightRadius: 2, borderTopLeftRadius: 2 }}>
                                    <View style={{ height: '7%', width: '100%', justifyContent: 'space-around', alignItems: 'center', backgroundColor: Colors.theme, flexDirection: 'row' }}>
                                        <Text style={{ color: 'white', fontSize: 16, flex: 1, alignSelf: 'center', marginLeft: 20, textAlign: 'center' }}>Select Location</Text>
                                        <AntDesign name='close' size={20} style={{ padding: 10, }} color={Colors.white} onPress={() => {
                                            this.setLocationModalVisible(!this.state.locationmodalvisible);
                                        }}
                                        ></AntDesign>
                                    </View>

                                    <GooglePlacesAutocomplete
                                        placeholder='Search'
                                        minLength={2} // minimum length of text to search
                                        autoFocus={false}
                                        returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                        keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
                                        listViewDisplayed={true}    // true/false/undefined
                                        fetchDetails={true}
                                        renderDescription={row => row.description} // custom description render
                                        onPress={(data, details) => { // 'details' is provided     when fetchDetails = true
                                            console.log(details);
                                            // this.setState({
                                            //     address: data.description,
                                            //     placeid: data.place_id
                                            // });
                                            // console.log(this.state.address)
                                            this.autocomplete_latLong(details.geometry.location.lat, details.geometry.location.lng, details.address_components[0].long_name)
                                        }}


                                        //getDefaultValue={() => ''}

                                        query={{
                                            // available options: https://developers.google.com/places/web-service/autocomplete
                                            key: 'AIzaSyCuO2RORekXW5FqBJDb_bDfPtSvu3hTp54',
                                            language: 'en', // language of the results
                                            types: ('geocode') // default: 'geocode'
                                        }}

                                        styles={{
                                            textInputContainer: {
                                                width: '100%',
                                                backgroundColor: Colors.backgroundGray,
                                                height: 54
                                            },
                                            textInput: {
                                                marginLeft: 0,
                                                marginRight: 0,
                                                height: 38,
                                                fontSize: 16
                                            },
                                            description: {
                                                fontWeight: 'bold'
                                            },
                                            predefinedPlacesDescription: {
                                                color: '#1faadb'
                                            }
                                        }}

                                        currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                                        currentLocationLabel="Current location"
                                        //nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                        //GoogleReverseGeocodingQuery={{
                                        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                        //}}
                                        // GooglePlacesSearchQuery={{
                                        //     // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                        //     rankby: 'distance',
                                        //     type: 'cafe'
                                        // }}

                                        //GooglePlacesDetailsQuery={{
                                        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                                        //fields: 'formatted_address',
                                        //}}

                                        //filterReverseGeocodingByTypes={['postal_code', 'sublocality', 'locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

                                        debounce={0} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                    // renderLeftButton={() => <Image source={Assets.testImageApple}/>}

                                    />
                                </View>
                            </View>
                        </Modal>
                        <CartInfoBottomButton {...this.props} />
                    </View>

                    {/* <CartInfoBottomButton {...this.props} /> */}
                </View>
            </SafeAreaView>
        )
    }
}

const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        farmsInfoArray: state.farmReducers.farmfilteredInfo,
        farmFilteredArray: selector.getFarms(state)
    }
}

export default connect(mapStatetoProps, null)(SearchScreen)