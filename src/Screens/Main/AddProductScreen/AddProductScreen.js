import React, { Component } from 'react'
import { Platform, Text, SafeAreaView, View, FlatList, Image, Dimensions, ScrollView, TextInput, TouchableOpacity, StyleSheet, Alert, KeyboardAvoidingView } from 'react-native'
import { CheckBox, colors, Icon } from 'react-native-elements'
import ImagePicker from 'react-native-image-picker';
import assets from 'res/Assets'
import Colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import SimpleText from 'library/components/SimpleText'
import { Assets } from 'react-navigation-stack'
import HomePageFarmItem from 'library/components/homePageFarmItem'

import Ionicon from 'react-native-vector-icons/Ionicons'
import { Dropdown } from 'react-native-material-dropdown';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Entypo from 'react-native-vector-icons/Entypo'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import TimePicker from 'react-native-simple-time-picker';
import Divider from 'library/components/divider'
import DividerSmall from 'library/components/divider_small_margin'
import HorizontalcommonTextIcon from 'library/components/HorizontalcommonTextIcon'
import CommonInputIcon from 'library/components/commonInputTextIcon'
import AntDesign from 'react-native-vector-icons/AntDesign'
import ImageItem from 'library/components/ImageItem'
import ModalDropdown from 'react-native-modal-dropdown';
import moment from 'moment'
import lodash from 'lodash'
import * as Actions from '../../../store/actions'
import { connect } from 'react-redux'
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import CartInfoBottomButton from 'library/components/cartInfoBottomButton'
import MaterialCommunityIcons
    from 'react-native-vector-icons/MaterialCommunityIcons'



const options = {
    title: 'Select',
    takePhotoButtonTitle: 'Take picture from camera',
    choosefromgallery: 'Choose from gallery'
}

class AddProductScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Add Product',
            headerTintColor: Colors.theme,
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    };

    componentDidMount() {
        if (Platform.OS === 'ios') {
            console.log('ios')
            check(PERMISSIONS.IOS.PHOTO_LIBRARY && PERMISSIONS.IOS.CAMERA)
                .then(result => {
                    switch (result) {
                        case RESULTS.UNAVAILABLE:
                            console.log(
                                'This feature is not available (on this device / in this context)',
                            );
                            break;
                        case RESULTS.DENIED:
                            request(PERMISSIONS.IOS.PHOTO_LIBRARY && PERMISSIONS.IOS.CAMERA).then(result => {
                                if (result === 'granted') {

                                }
                            });
                            console.log(
                                'The permission has not been requested / is denied but requestable',
                            );
                            break;
                        case RESULTS.GRANTED:

                            console.log('The permission is granted');
                            break;
                        case RESULTS.BLOCKED:
                            console.log(
                                'The permission is denied and not requestable anymore',
                            );
                            break;
                    }
                })
                .catch(error => {
                    // …
                    console.log(error)
                });
        } else {
            check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE && PERMISSIONS.ANDROID.CAMERA)
                .then(result => {
                    switch (result) {
                        case RESULTS.UNAVAILABLE:
                            console.log(
                                'This feature is not available (on this device / in this context)',
                            );
                            break;
                        case RESULTS.DENIED:
                            request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE && PERMISSIONS.ANDROID.CAMERA).then(
                                result => {
                                    if (result === 'granted') {
                                        this.getUserLocation();
                                    }
                                },
                            );
                            console.log(
                                'The permission has not been requested / is denied but requestable',
                            );
                            break;
                        case RESULTS.GRANTED:
                            this.getUserLocation();
                            console.log('The permission is granted');
                            break;
                        case RESULTS.BLOCKED:
                            request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then(
                                result => {
                                    if (result === 'granted') {
                                        this.getUserLocation();
                                    }
                                },
                            );
                            console.log(
                                'The permission is denied and not requestable anymore',
                            );
                            break;
                    }
                })
                .catch(error => {
                    // …
                });
        }
    }

    state = {
        name: '',
        description: '',
        price: '',
        quantity: '',
        unit: 'Pounds',
        organicallyGrown: true,
        selectedIndex: 0,
        selectedPickup: true,
        selectedDelivery: true,
        date: new Date(),
        avatarSource: ['add'],
        uploadingImages: [],
        seededDate: moment().format('YYYY-MM-DD'),
        harvestingDate: 'Select',
        showAddReview: false,
        isDatePickerVisibleStart: false,
        isDatePickerVisibleEnd: false,
        filePath: {},
    }
    myfun = () => {


        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log("image Data", response)
                const progressCallback = progressEvent => {
                    const percentFraction = progressEvent.loaded / progressEvent.total;
                    const percent = Math.floor(percentFraction * 100);
                    console.log(percent)
                }



                this.props.dispatch(Actions.imageUploadApiAction(response, progressCallback)).then(resp => {
                    console.log(resp)
                    console.log([...resp.data, ...this.state.uploadingImages])
                    this.setState({

                        uploadingImages: [...resp.data, ...this.state.uploadingImages],

                    });
                })
                    .catch((err) => {
                        Alert.alert('Oops!', err)
                    })
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({

                    avatarSource: [response.uri, ...this.state.avatarSource],
                    showAddReview: true,
                });
            }
        });
    }



    _handleDatePicked(date, key) {
        console.log(key)
        if (key === 'seed') {

            let harvest = moment(this.state.harvestingDate !== 'Select' ? this.state.harvestingDate : new Date(), 'YYYY-MM-DD')
            let seed = moment(date)

            let diff = moment(harvest).diff(seed, 'hours')

            if (diff > 0 || this.state.harvestingDate === 'Select') {
                this.setState({
                    isDatePickerVisibleStart: false,
                    seededDate: moment(date).format('YYYY-MM-DD'),
                })
            } else {
                Alert.alert('Oops!', 'Please Select proper date')
            }


        } else {

            let harvest = moment(date)
            let seed = moment(this.state.seededDate !== 'Select' ? this.state.seededDate : new Date(), 'YYYY-MM-DD')

            let diff = moment(harvest).diff(seed, 'hours')




            this.setState({
                isDatePickerVisibleEnd: false,
                harvestingDate: moment(date).format('YYYY-MM-DD'),
            })


        }
    }

    disableButton(state) {
        if (
            state.avatarSource.length !== 1 &&
            state.name.trim() !== '' &&
            state.description.trim() !== '' &&
            state.price.trim() !== '' &&
            state.quantity.trim() !== '' &&
            //state.seededDate !== 'Select' &&
            state.harvestingDate !== 'Select'

        ) {
            console.log('order', state.selectedDelivery || state.selectedPickup)
            if (state.selectedDelivery || state.selectedPickup) {
                return false
            } else {
                return true
            }

        } else {
            return true
        }
    }

    handleSubmit() {
        let ordertype = []
        if (this.state.selectedDelivery) {
            ordertype.push('delivery')
        }
        if (this.state.selectedPickup) {
            ordertype.push('pickup')
        }
        let productInfo = {
            user_id: this.props.auth._id,
            product_name: this.state.name,
            seeded_date: this.state.seededDate,
            harvesting_date: this.state.harvestingDate,
            organically_grown: this.state.organicallyGrown,
            order_type: ordertype,
            price: this.state.price,
            description: this.state.description,
            image: this.state.uploadingImages,
            unit: this.state.unit,
            quantity: this.state.quantity
        }

        console.log(productInfo)
        this.props.dispatch(Actions.addProductApiAction(productInfo)).then(
            (response) => {
                if (response.data.status === 1) {
                    this.setState({

                        name: '',
                        description: '',
                        price: '',
                        quantity: '',
                        unit: 'Pounds',
                        organicallyGrown: true,
                        selectedIndex: 0,
                        selectedPickup: true,
                        selectedDelivery: true,
                        date: new Date(),
                        avatarSource: ['add'],
                        uploadingImages: [],
                        //seededDate: 'Select',
                        harvestingDate: 'Select',
                        showAddReview: false,
                        isDatePickerVisibleStart: false,
                        isDatePickerVisibleEnd: false,
                        filePath: {},

                    })
                    this.props.navigation.navigate("HomeNav")
                    Alert.alert('Done', response.data.msg)
                } else {
                    Alert.alert('Oops!', response.data.msg)
                }

            }
        ).catch(err => {
            Alert.alert('Error', err)
            console.log("auth", "......")

        })
    }

    render() {
        let ImagesView =
            <View style={{ flex: 1, marginHorizontal: 20 }}>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}
                    data={this.state.avatarSource}
                    keyExtractor={item => item.id}
                    horizontal={true}
                    style={{ marginBottom: 20 }}
                    renderItem={({ item, index }) =>
                        item === 'add' ? <TouchableOpacity onPress={() => this.myfun()} >
                            <View style={{ height: Dimensions.get('screen').width / 3, width: Dimensions.get('screen').width / 3, margin: 20, alignSelf: 'center', backgroundColor: Colors.linegray, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }} >
                                <AntDesign size={20} name={'plus'} />
                            </View>
                        </TouchableOpacity>
                            : <View>
                                <ImageItem
                                    image={{ uri: item }}
                                />
                                {this.state.avatarSource.length - 1 === this.state.uploadingImages.length ? <TouchableOpacity
                                    onPress={() => {
                                        this.setState({
                                            avatarSource: lodash.remove(this.state.avatarSource, function (n) {
                                                return n !== item
                                            }),
                                            uploadingImages: this.state.uploadingImages.splice(index, 1)
                                        })
                                        console.log('updated Upload image', this.state.uploadingImages.splice(index, 1))
                                    }}
                                    style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: 'red', position: 'absolute', alignSelf: 'flex-end', alignItems: 'center', justifyContent: 'center' }}>
                                    <Icon type='ionicon' name={'md-close'} size={25} color={'white'} style={{ alignSelf: 'center', flex: 1 }} />
                                </TouchableOpacity> : null}
                            </View>}
                />
            </View>



        return (
            this.props.auth.isFarmUpdated ?
                <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
                    <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior={Platform.OS === 'ios' ? "padding" : ''}>
                        <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: Colors.white }}>
                            <View>
                                <View style={Styles.main}>
                                    {ImagesView}
                                    <DividerSmall />
                                    <CommonInputIcon
                                        name='Product Name'
                                        iconName='apple-o'
                                        iconType='antdesign'
                                        placeholder='Required'
                                        value={this.state.name}
                                        onTextChange={(text) => {
                                            this.setState({
                                                name: text
                                            })
                                        }}
                                    />
                                    <DividerSmall />

                                    <CommonInputIcon
                                        name='Product Description'
                                        iconName='ios-leaf'
                                        iconType='ionicon'
                                        placeholder='Required'
                                        value={this.state.description}
                                        onTextChange={(text) => {
                                            this.setState({
                                                description: text
                                            })
                                        }}
                                    />
                                    <DividerSmall />

                                    {/* <CommonInputIcon
                                        name='Retail Price'
                                        iconName='attach-money'
                                        iconType='materialicons'
                                        placeholder='Required'
                                        value={this.state.price}
                                        onTextChange={(text) => {
                                            this.setState({
                                                price: text
                                            })
                                        }}
                                    />
                                    <DividerSmall /> */}
                                    <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white', paddingLeft: 10 }}>
                                        {/* <View style={{ flexDirection: "row", height: 50, flex: 1, width: "100%", alignItems: 'center', justifyContent: 'flex-end', backgroundColor: 'white' }}> */}
                                        <Icon size={20} name='attach-money' type='materialicons' color={Colors.subtextGray}></Icon>
                                        <Text style={{ marginLeft: 10, width: '35%', paddingRight: 10, color: Colors.subtextGray }}>Retail Price</Text>
                                        <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                            <TextInput
                                                style={{ borderColor: colors.grey5, borderWidth: 1, borderRadius: 5, width: 50, height: 35, textAlign: 'center', fontSize: 15 }}
                                                keyboardType='number-pad'
                                                value={this.state.price}
                                                onChangeText={(text) => {
                                                    this.setState({
                                                        price: text
                                                    })
                                                }}
                                            />
                                            <TouchableOpacity disabled={true} style={{ marginTop: 3, marginLeft: 18, borderBottomWidth: 0, borderBottomColor: colors.grey4, height: 30, alignItems: 'center', flexDirection: 'row', borderRadius: 5 }}>
                                                {/* <ModalDropdown
                                                    defaultValue='Pounds'
                                                    defaultIndex={0}
                                                    style={{ paddingHorizontal: 7, paddingVertical: 1 }}
                                                    textStyle={{ color: Colors.text, fontSize: 15 }}
                                                    dropdownTextStyle={{ fontSize: 15 }}
                                                    dropdownStyle={{ width: 100 }}
                                                    options={['Pounds', 'Bunch', 'Dozen', 'Bushels', 'Each', 'Pints', 'Ounces', 'Bags']}
                                                    onSelect={(index, item) => {
                                                        this.setState({
                                                            unit: item
                                                        })
                                                    }}
                                                /> */}
                                                <SimpleText style={{ marginHorizontal: 10, fontSize: 15, fontWeight: '400' }} text={'/ ' + this.state.unit} />
                                                {/* <View style={{ marginHorizontal: 10, height: 30, width: 1, backgroundColor: 'black' }}></View> */}
                                                {/* <Ionicon name='md-arrow-dropdown' size={15} style={{ marginRight: 10 }} color={colors.grey4} /> */}
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <DividerSmall />
                                    {/* <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white', paddingLeft: 10 }}>
                                    <Ionicon size={20} name={'md-calendar'} color={Colors.subtextGray}></Ionicon>
                                    <Text style={{ marginLeft: 10, width: '35%', paddingRight: 10, color: Colors.subtextGray }}>Seeded Date</Text>
                                    <TouchableOpacity onPress={() => this.setState({ isDatePickerVisibleStart: true })}>
                                        <Text style={{ width: '100%' }}>
                                            {this.state.seededDate}
                                        </Text>
                                        <DateTimePickerModal
                                            isVisible={this.state.isDatePickerVisibleStart}
                                            mode="date"
                                            onConfirm={(date) => this._handleDatePicked(date, 'seed')}
                                            onCancel={() => this.setState({ isDatePickerVisibleStart: false })}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <DividerSmall /> */}

                                    <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white', paddingLeft: 10 }}>
                                        {/* <View style={{ flexDirection: "row", height: 50, flex: 1, width: "100%", alignItems: 'center', justifyContent: 'flex-end', backgroundColor: 'white' }}> */}
                                        <Ionicon size={20} name={'md-calendar'} color={Colors.subtextGray}></Ionicon>
                                        <Text style={{ marginLeft: 10, width: '35%', paddingRight: 10, color: Colors.subtextGray }}>Harvesting Date</Text>
                                        <TouchableOpacity onPress={() => this.setState({ isDatePickerVisibleEnd: true })}>
                                            <Text style={{ width: '100%' }}>
                                                {this.state.harvestingDate}
                                            </Text>
                                            <DateTimePickerModal
                                                isVisible={this.state.isDatePickerVisibleEnd}
                                                mode="date"
                                                onConfirm={(date) => this._handleDatePicked(date, 'harvest')}
                                                onCancel={() => this.setState({ isDatePickerVisibleEnd: false })}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <DividerSmall />

                                    <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white', paddingLeft: 10 }}>
                                        {/* <View style={{ flexDirection: "row", height: 50, flex: 1, width: "100%", alignItems: 'center', justifyContent: 'flex-end', backgroundColor: 'white' }}> */}
                                        <Ionicon size={20} name={'md-calendar'} color={Colors.subtextGray}></Ionicon>
                                        <Text style={{ marginLeft: 10, width: '35%', paddingRight: 10, color: Colors.subtextGray }}>Organicaly Grown</Text>
                                        <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                            <CheckBox
                                                title='Yes'
                                                checkedColor={Colors.theme}
                                                uncheckedColor={Colors.primary}
                                                // style={{marginTop:40,backgroundColor:'transparent', borderColor: 'transparent' }}
                                                checked={this.state.organicallyGrown}
                                                onPress={() => this.setState({ organicallyGrown: !this.state.organicallyGrown })}
                                                containerStyle={{ padding: 0, margin: 0, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                            ></CheckBox>
                                            <CheckBox
                                                title='No'
                                                checkedColor={Colors.theme}
                                                uncheckedColor={Colors.primary}
                                                checked={!this.state.organicallyGrown}
                                                onPress={() => this.setState({ organicallyGrown: !this.state.organicallyGrown })}
                                                containerStyle={{ padding: 0, margin: 0, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                            ></CheckBox>
                                        </View>
                                    </View>
                                    <DividerSmall />

                                    {/* <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', width: '100%', backgroundColor: 'white', paddingLeft: 10 }}>
                                    <MaterialCommunityIcons
                                        size={20} name={'truck-delivery'} color={Colors.subtextGray}></MaterialCommunityIcons>

                                    <Text style={{ marginLeft: 10, width: '35%', paddingRight: 10, color: Colors.subtextGray }}>Delivery Type</Text>
                                    <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                        <CheckBox
                                            title='Delivery'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            checked={this.state.selectedDelivery}
                                            onPress={() => this.setState({ selectedDelivery: !this.state.selectedDelivery })}
                                            containerStyle={{ padding: 0, margin: 0, marginLeft: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>
                                        <CheckBox
                                            title='Pickup'
                                            checkedColor={Colors.theme}
                                            uncheckedColor={Colors.primary}
                                            checked={this.state.selectedPickup}
                                            onPress={() => this.setState({ selectedPickup: !this.state.selectedPickup })}
                                            containerStyle={{ padding: 0, margin: 0, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                        ></CheckBox>
                                    </View>
                                </View>
                                <DividerSmall /> */}
                                    <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white', paddingLeft: 10 }}>
                                        {/* <View style={{ flexDirection: "row", height: 50, flex: 1, width: "100%", alignItems: 'center', justifyContent: 'flex-end', backgroundColor: 'white' }}> */}
                                        <Ionicon size={20} name={'ios-basket'} color={Colors.subtextGray}></Ionicon>
                                        <Text style={{ marginLeft: 10, width: '35%', paddingRight: 10, color: Colors.subtextGray }}>Available Stock</Text>
                                        <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                            <TextInput
                                                style={{ borderColor: colors.grey5, borderWidth: 1, borderRadius: 5, width: 50, height: 35, textAlign: 'center', fontSize: 15 }}
                                                keyboardType='number-pad'
                                                value={this.state.quantity}
                                                onChangeText={(text) => {
                                                    this.setState({
                                                        quantity: text
                                                    })
                                                }}
                                            />
                                            <TouchableOpacity style={{ marginTop: 3, marginLeft: 18, borderBottomWidth: 1, borderBottomColor: colors.grey4, height: 30, alignItems: 'center', flexDirection: 'row', borderRadius: 5 }}>
                                                <ModalDropdown
                                                    defaultValue='Pounds'
                                                    defaultIndex={0}
                                                    style={{ paddingHorizontal: 7, paddingVertical: 1 }}
                                                    textStyle={{ color: Colors.text, fontSize: 15 }}
                                                    dropdownTextStyle={{ fontSize: 15 }}
                                                    dropdownStyle={{ width: 100 }}
                                                    options={['Pounds', 'Bunch', 'Dozen', 'Bushels', 'Each', 'Pints', 'Ounces', 'Bags']}
                                                    onSelect={(index, item) => {
                                                        this.setState({
                                                            unit: item
                                                        })
                                                    }}
                                                />
                                                {/* <SimpleText style={{ marginHorizontal: 10, fontSize: 15, fontWeight: '400' }} text="Pounds" /> */}
                                                {/* <View style={{ marginHorizontal: 10, height: 30, width: 1, backgroundColor: 'black' }}></View> */}
                                                <Ionicon name='md-arrow-dropdown' size={15} style={{ marginRight: 10 }} color={colors.grey4} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <DividerSmall />
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20, backgroundColor: colors.backgroundGray }}>
                                    <CircularCornerButton
                                        title='Submit'
                                        style={{ width: Dimensions.get('screen').width - 40 }}
                                        action={() => this.handleSubmit()}
                                        disabled={this.disableButton(this.state)}

                                    // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.address.trim() === '' : true ||
                                    // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.city ? this.state.auth.farm.pickup.city.trim() === '' : true : true ||
                                    // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.state ? this.state.auth.farm.pickup.state.trim() === '' : true : true   ||
                                    // // this.state.auth.farm.pickup ? this.state.auth.farm.pickup.zipcode ? this.state.auth.farm.pickup.zipcode.trim() === '' : true : true  ||


                                    />
                                </View>
                            </View>
                            <View style={{ height: 50 }}></View>

                        </ScrollView>
                    </KeyboardAvoidingView>
                    <CartInfoBottomButton {...this.props} />
                </SafeAreaView >
                : <SafeAreaView style={{ flex: 1, backgroundColor: Colors.backgroundGray, alignItems: 'center' }}>
                    <View style={{ flex: 1, backgroundColor: Colors.backgroundGray, alignItems: 'center', justifyContent: 'space-evenly' }}>
                        <Image resizeMode='contain' source={assets.noFarm} style={{ width: Dimensions.get('screen').width / 1.5, height: 200 }} />
                        <Text numberOfLines={0} style={{ width: Dimensions.get('screen').width - 20, marginLeft: 10, paddingRight: 10, color: Colors.subtextGray, fontSize: 20 }}>You havn't filled all the farm details please fill all farm details to add a product</Text>
                        <CircularCornerButton
                            title='Fill farm details'
                            style={{ width: Dimensions.get('screen').width - 40, height: 40 }}
                            action={() => this.props.navigation.navigate('MyAccount')} />
                    </View>
                    <CartInfoBottomButton {...this.props} />
                </SafeAreaView>
        );


    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.authreducers.auth
    }
}

export default connect(mapStateToProps, null)(AddProductScreen)


const Styles = StyleSheet.create({
    main: {
        margin: 10,
        flexDirection: 'column'
    },
    textinput: {
        borderBottomWidth: .5,
        width: '90%',
        borderColor: Colors.subtextGray,
        color: 'black'
    },
    textheading: {
        color: Colors.theme, fontSize: 15, marginLeft: 10,
    }
    ,
    image: {
        width: 44,
        height: 44,
        borderRadius: 44 / 2,
    },
    icon: {
        height: 30,
        width: 30,
        margin: 10,
    },
    view: {
        marginTop: 20, backgroundColor: Colors.white, flexDirection: 'row', width: '100%', marginLeft: 15, alignItems: 'center'
    },
    text: {
        color: Colors.subtextGray,
    },
    input: {
        borderBottomWidth: 1, borderBottomColor: Colors.backgroundGray, width: '100%', marginTop: Platform.OS === 'ios' ? 15 : -5, marginLeft: Platform.OS === 'ios' ? 0 : -5,
    },
    subView: {
        width: '60%'
    }

});
