import { createSelector } from 'reselect'
import moment from 'moment';

const getProductDetails = (data) => data


export const getProduct = createSelector(
    [ getProductDetails ],
    ( productDetail ) => {
      
        

            return {
                _id: productDetail._id,
                product_name : productDetail.product_name,
                description : productDetail.description,
                price : `${productDetail.price}`,
                seededDate : moment(productDetail.seeded_date).format('YYYY-MM-DD'),
                harvestingDate : moment(productDetail.harvesting_date).format('YYYY-MM-DD'),
                quantity: productDetail.quantity === undefined ? '' : productDetail.quantity,
                unit: productDetail.unit === undefined ? '' : productDetail.unit,
                organicallyGrown: productDetail.organically_grown,
                selectedPickup: productDetail.order_type.includes('pickup'),
                selectedDelivery: productDetail.order_type.includes('delivery'),
                uploadingImages : productDetail.image,
            }

    }
  )



export const dayOfWeekAsString = (dayIndex) => {
    return ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"][dayIndex];
}