import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Dimensions,
  ScrollView,
  Text,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import assets from "res/Assets";
import colors from "res/Colors";
import CommonTextInput from "library/components/commonInputText";
import CircularCornerButton from "library/components/circularCornerButton";
import GoogleSignInButton from "library/components/googleSignInButton";
import FacebookLogInButton from "library/components/facebookLogInButton";
import { connect } from "react-redux";
import * as action from '../../../store/actions';
import { ThemeProvider } from "react-native-elements";

class LoginScreen extends Component {
  state = {
    slectedtab: 0,
    auth: {
      email: '',
      password: '',
      fcm_id:''
    }
  };

  componentDidMount() {
    console.log("Data from redux", this.props.data);
    console.log('FCM IN LOGIN=>',this.props.fcmToken)
    this.setState({ ...this.state, auth: { ...this.state.auth, fcm_id: this.props.fcmToken } })

  }

  handleEmail(email) {
    this.setState({ ...this.state, auth: { ...this.state.auth, email: email } })
    console.log(this.state.auth);
  }

  handlePassword(password) {
    this.setState({ ...this.state, auth: { ...this.state.auth, password: password } })

    // this.state.auth.password = password
    console.log(this.state.auth);
  }

  handleSubmit = () => {
    console.log(this.state)
    console.log(this.state.auth)

    this.props.dispatch(action.loginApiAction(this.state.auth)).then(
      (res) => {
        console.log('Res Login', res)
        if (res === 'Done'){
          this.props.navigation.navigate("Home")
        } else if(res==='not verified'){
        this.props.navigation.replace("EmailVerification")
      }else{
        Alert.alert('Error', res)

      }
        // this.props.navigation.navigate("Home")
      }
    ).catch(err => {
      
      if(err==='not verified'){
        this.props.navigation.replace("EmailVerification")
      }else{
        Alert.alert('Error', err)

      }
    })
  }

  handleForgot = () => {
    this.props.navigation.navigate("ForgotPassword");
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
        <Image source={assets.transperentlogo} style={{ height: 120, width: 120, marginTop: 15, alignSelf: 'center', marginBottom: 10 }}></Image>
        <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
        <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
          <View
            style={{
              flex: 1,
              paddingTop: 15,
              backgroundColor: colors.backgroundGray
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                paddingTop: 15,
                backgroundColor: colors.backgroundGray
              }}
            >
              <FacebookLogInButton
                style={{
                  marginBottom: 10,
                  height: 40,
                  width: Dimensions.get("screen").width / 1.2
                }}
              />
              <GoogleSignInButton
                style={{
                  height: 40,
                  width: Dimensions.get("screen").width / 1.2
                }}
              />
              <Text
                style={{
                  color: "#777777",
                  paddingVertical: 20,
                  fontWeight: "500",
                  fontSize: 15
                }}
              >
                or continue with email
              </Text>
              <CommonTextInput
                style={{ width: Dimensions.get("screen").width - 20 }}
                name="Email"
                placeholder="Required"
                value={this.state.auth.email}
                onTextChange={email => this.handleEmail(email)}
                keyboardType='email-address'

              />
              <CommonTextInput
                style={{ width: Dimensions.get("screen").width - 20 }}
                name="Password"
                placeholder="at least 8 characters"
                value={this.state.auth.password}
                isSecure={true}
                onTextChange={password => this.handlePassword(password)}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                paddingTop: 30,
                backgroundColor: colors.backgroundGray
              }}
            >
              <CircularCornerButton
                disabled={
                  this.state.auth.email.trim() === '' || 
                  this.state.auth.password.trim() === ''
                }
                title="Sign In"
                style={{ width: Dimensions.get("screen").width - 40 }}
                action={() => this.handleSubmit()}
                // action={()=>this.props.navigation.navigate("Home")}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-start",
                paddingTop: 20,
                paddingLeft: 20,
                backgroundColor: colors.backgroundGray
              }}
            >
              <TouchableOpacity
                onPress={() => this.handleForgot()}
              >
                <Text
                  style={{
                    color: colors.theme,
                    textAlign: "left",
                    fontWeight: "500"
                  }}
                >
                  Forgot your password?
                </Text>
              </TouchableOpacity>
            </View>
            {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
          </View>
        </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    fcmToken: state.fcmReducer.fcmToken
  }
}

export default connect(mapStateToProps, null)(LoginScreen)
