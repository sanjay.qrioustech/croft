import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import LoginScreen from './LoginScreen'
import SignUpScreen from '../SignUp/SignUpScreen'
class LoginSignUpHeader extends Component {
    static navigationOptions = { header: null }
    state = {
        selectedIndex: 0
    }
    render() {
        let currentView = this.state.selectedIndex == 0 ? <LoginScreen {...this.props} /> : <SignUpScreen {...this.props} />

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ height: 40, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{   alignItems: 'center', justifyContent: 'center',  borderRadius: 5}}>
                        <SegmentedControlTab
                            lastTabStyle={{ borderRadius: 5}}
                            values={['Sign In', 'Sign Up']}
                            selectedIndex={this.state.selectedIndex}
                            onTabPress={(index) => { 
                                this.setState({
                                    selectedIndex: index
                                })
                             }}
                            borderRadius={5}
                            tabsContainerStyle={{ height: 40, backgroundColor: '#F2F2F2',width: 150, borderRadius: 5,}}
                            tabStyle={{
                                borderRadius: 5,
                                backgroundColor: '#F2F2F2',
                                borderWidth: 0,
                                borderColor: 'transparent',
                            }}
                            activeTabStyle={{ backgroundColor: 'white', margin: 2, borderRadius: 5 }}
                            tabTextStyle={{ color: 'black', fontWeight: 'bold' }}
                            activeTabTextStyle={{ color: colors.theme }}
                        />
                    </View>
                </View>
                <View style={{marginTop: 10, height:1, backgroundColor: '#cacaca'}}></View>
                { currentView }
            </SafeAreaView>
        )
    }
}

export default LoginSignUpHeader