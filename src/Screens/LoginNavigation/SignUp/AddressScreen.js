import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, StyleSheet, Alert, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'

import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import SimpleText from 'library/components/SimpleText'
import Autocomplete from 'react-native-autocomplete-input'
import axios from 'axios'
import { connect } from "react-redux";
import * as action from '../../../store/actions';

class AddressScreen extends Component {

    // handleSelectItem(item, index) {
    //     const { onDropdownClose } = this.props;
    //     onDropdownClose();
    //     console.log(item);
    // }

    static navigationOptions = {
        title: 'More Details',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };


    state = {
        registerData: {
            fname: '',
            lname: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            city: '',
            state: '',
            zipcode: '',
            ...this.props.navigation.getParam('state', 'hellly')
        }
    }

    handleAddress(address) {
        this.setState({ ...this.state, registerData: { ...this.state.registerData, address: address } })
        console.log(this.state);
    }
    handleCity(city) {
        this.setState({ ...this.state, registerData: { ...this.state.registerData, city: city } })
        console.log(this.state);
    }
    handleState(state) {
        this.setState({ ...this.state, registerData: { ...this.state.registerData, state: state } })
        console.log(this.state);
    }
    handleZipcode(zipcode) {
        this.setState({ ...this.state, registerData: { ...this.state.registerData, zipcode: zipcode } })
        console.log(this.state);
    }

    componentDidMount() {
        console.log("Data from redux", this.props.data);
    }

    // handleSubmit = () => {
    //     console.log("API CALLLL")

    //     this.props.dispatch(action.signupApiAction(this.state.registerData)).then(
    //         (response) => {
    //             console.log("API REPONSE => ", JSON.stringify(response))
    //             if (response.data.status === 1) {
    //                 console.log("REGISTER STATUS => ", response.data.status)
    //                 this.props.dispatch(action.signupaction(response.data.data))
    //                 this.props.navigation.navigate("EmailVerification")
    //             }
    //             else {
    //                 console.log("VALIDATE MSG => ", response.data.msg);
    //             }
    //         }
    //     )
    //         .catch(
    //             err => {
    //                 console.log('ERROR MSG => ', err)
    //                 Alert.alert(err)
    //             }
    //         )
    // }

    handleSubmit = () => {
        console.log(this.state)
        console.log(this.state.registerData)
    
        this.props.dispatch(action.signupApiAction(this.state.registerData)).then(
          () => this.props.navigation.replace("EmailVerification")
        ).catch(err => {
          Alert.alert('Error', err)
        })
      }
    
    onAutocompleteChangeHandler = (text) => {

        if (text.length < 3) {
            this.setState({ ...this.state, autocompleteText: text, autocompleteData: [] })
            return
        }

        axios.get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + text + "&    types=address%7Cgeocode&radius=25000&language=en&key=AIzaSyAQ4zdRDkuoPojGvGTnKYFFWdtjbJDPl7k").then(response => {
            let arr = response.data.predictions;
            console.log(arr)
            let dataarr = [];
            if (arr.length > 0) {

                arr.map(data => {

                    axios.get("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + data.place_id + "&key=AIzaSyAQ4zdRDkuoPojGvGTnKYFFWdtjbJDPl7k")
                        .then(res => {


                            let plcecomp = []
                            if ('address_components' in res.data.result) {
                                plcecomp = res.data.result.address_components
                            }
                            console.log("data", res);
                            let zipcode = '';
                            let state = '';
                            let city = '';
                            if (plcecomp.length > 0) {
                                zipcode = plcecomp[plcecomp.length - 1].long_name
                            }

                            if (plcecomp.length > 3) {
                                state = plcecomp[plcecomp.length - 3].long_name
                            }

                            if (plcecomp.length > 4) {
                                state = plcecomp[plcecomp.length - 4].long_name
                            }



                            dataarr.push({
                                description: data.description,
                                place_id: data.place_id,
                                zipcode: zipcode,
                                state: state,
                                city: city
                            })

                            this.setState({ ...this.state, autocompleteText: text, autocompleteData: dataarr })
                            console.log(this.state)

                        }).catch(err => console.log(err))
                })



            }


            // this.setState({ ...this.state , autocompleteText : text, autocompleteData : arr })
            // autocompleteData.length ? this.state.autocompleteData.candidates.map(m =>  console.log("=>" + m.place_id)) : null          

        }).catch((err) => {
            console.log("error", err);
        })


        console.log(this.state)
    }

    onAutoCompleteSelectHandler = (e, item) => {
        e.preventDefault();

        this.setState({
            ...this.state,
            autocompleteText: item.description,
            autocompleteData: [],
            zipcode: item.zipcode,
            state: item.state,
            city: item.city
        })

    }

    render() {

        // const autocompletes = [...Array(1).keys()];

        // const apiUrl = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key=";

        // const { scrollToInput, onDropdownClose, onDropdownShow } = this.props;

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <Image source={assets.transperentlogo} style={{ height: 120, width: 120, alignSelf: 'center', marginBottom: 10 }}></Image>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                            <FacebookLogInButton
                                style={{
                                    marginBottom: 10,
                                    height: 40,
                                    width: Dimensions.get('screen').width / 1.2
                                }}
                            />
                            <GoogleSignInButton
                                style={{
                                    height: 40,
                                    width: Dimensions.get('screen').width / 1.2
                                }}

                            />
                            <Text style={{ color: '#777777', paddingVertical: 20, fontWeight: '500', fontSize: 15 }}>
                                or continue with email
                            </Text>


                            {/* <Autocomplete
                                defaultValue={this.state.autocompleteText}
                                onChangeText = { text =>  this.onAutocompleteChangeHandler(text) }
                                data = {this.state.autocompleteData}
                                renderItem={({ item, i }) => (
                                    <TouchableOpacity onPress={(e) => this.onAutoCompleteSelectHandler(e,item)} >
                                      <Text>{item.description}</Text>
                                    </TouchableOpacity>
                                  )}    
                            /> */}
                            <CommonTextInput defaultValue={this.state.address} style={{ width: Dimensions.get('screen').width - 20 }} name='Address' placeholder='Required'
                                onTextChange={address => this.handleAddress(address)}
                            />
                            <CommonTextInput defaultValue={this.state.city} style={{ width: Dimensions.get('screen').width - 20 }} name='City' placeholder='Required'
                                onTextChange={city => this.handleCity(city)}
                            />
                            <CommonTextInput defaultValue={this.state.state} style={{ width: Dimensions.get('screen').width - 20 }} name='State' placeholder='Required'
                                onTextChange={state => this.handleState(state)}
                            />
                            <CommonTextInput defaultValue={this.state.zipcode} style={{ width: Dimensions.get('screen').width - 20 }} name='Zipcode' placeholder='Required'
                                keyboardType='number-pad'
                                onTextChange={zipcode => this.handleZipcode(zipcode)}
                            />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton
                                disabled={
                                    this.state.registerData.address.trim() === '' ||
                                    this.state.registerData.city.trim() === '' ||
                                    this.state.registerData.state.trim() === '' ||
                                    this.state.registerData.zipcode.trim() === ''}                                    
                                title='Sign Up'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                action={() => this.handleSubmit()} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', paddingVertical: 20, paddingLeft: 20, backgroundColor: colors.backgroundGray }}>

                            <Text style={{ color: colors.subtextGray, textAlign: 'left' }}>By tapping SignUp, Continue with Facebook, or Continue with Google, you agree to our <Text style={{ color: colors.theme, textAlign: 'left' }} >Terms and Conditions</Text> and <Text style={{ color: colors.theme, textAlign: 'left' }}>Privacy Statement</Text></Text>

                        </View>
                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}



export default connect(null, null)(AddressScreen)

//export default SignUpScreen
// export default SignUpScreen;


