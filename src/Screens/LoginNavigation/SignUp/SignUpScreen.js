import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import GooglePlacesAutocomplete from 'library/components/googlePlacesAutocomplete'

import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import SimpleText from 'library/components/SimpleText'
import { connect } from "react-redux";
import * as action from '../../../store/actions';
class SignUpScreen extends Component {

    state = {
        disabled: '',
        signup: {
            fname: '',
            lname: '',
            email: '',
            password: '',
            phone: '',
            fcm_id:''
        }

    };

    handleFirstName(fname) {
        this.setState({ ...this.state, signup: { ...this.state.signup, fname: fname } })
        console.log(this.state.signup);
    }

    handleLastName(lname) {
        this.setState({ ...this.state, signup: { ...this.state.signup, lname: lname } })
        console.log(this.state.signup);
    }

    handleEmail(email) {

        this.setState({ ...this.state, signup: { ...this.state.signup, email: email } })
        console.log('Verify Email',!this.state.signup.email.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/));
    }

    handlePassword(password) {
        this.setState({ ...this.state, signup: { ...this.state.signup, password: password } })
        console.log(this.state.signup);
    }

    handlePhone(phone) {
        this.setState({ ...this.state, signup: { ...this.state.signup, phone: phone } })
        console.log(this.state.signup);
    }


    componentDidMount(){
        this.setState({ ...this.state, signup: { ...this.state.signup, fcm_id: this.props.fcmToken } })
        console.log('SIGNUP=>',this.state.signup.fcm_id)
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <KeyboardAvoidingView
                        keyboardVerticalOffset={100}
                        style={{ flex: 1 }}
                        behavior= {Platform.OS === 'ios' ? "padding": ''}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <Image source={assets.transperentlogo} style={{ height: 120, width: 120, alignSelf: 'center', marginBottom: 10 }}></Image>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                            <FacebookLogInButton
                                style={{
                                    marginBottom: 10,
                                    height: 40,
                                    width: Dimensions.get('screen').width / 1.2
                                }}
                            />
                            <GoogleSignInButton
                                style={{
                                    height: 40,
                                    width: Dimensions.get('screen').width / 1.2
                                }}

                            />
                            <Text style={{ color: '#777777', paddingVertical: 20, fontWeight: '500', fontSize: 15 }}>
                                or continue with email
                        </Text>
                            <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='First Name' placeholder='Required' onTextChange={fname => this.handleFirstName(fname)}
                            />
                            <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Last Name' placeholder='Required'
                                onTextChange={lname => this.handleLastName(lname)} />
                            <CommonTextInput
                                style={{ width: Dimensions.get('screen').width - 20 }}
                                name='Email' placeholder='Required'
                                keyboardType='email-address'
                                onTextChange={email => this.handleEmail(email)} />
                            <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Password' placeholder='at least 8 characters' isSecure={true}
                                onTextChange={password => this.handlePassword(password)} />
                            <CommonTextInput style={{ width: Dimensions.get('screen').width - 20 }} name='Phone' placeholder='Required' keyboardType='number-pad'
                                onTextChange={phone => this.handlePhone(phone)} />

                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton
                                disabled={
                                    this.state.signup.fname.trim() === '' ||
                                    this.state.signup.lname.trim() === '' ||
                                    this.state.signup.email.trim() === '' ||
                                    !this.state.signup.email.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/) ||
                                    this.state.signup.password.trim() === '' ||
                                    this.state.signup.password.length < 8 ||
                                    this.state.signup.phone.trim() === '' ||
                                    this.state.signup.phone.length !== 10
                                }
                                title='Next'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                action={() => { this.props.navigation.navigate('Address' , { state:this.state.signup }) }} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', paddingVertical: 20, paddingLeft: 20, backgroundColor: colors.backgroundGray }}>

                            <Text style={{ color: colors.subtextGray, textAlign: 'left' }}>By tapping SignUp, Continue with Facebook, or Continue with Google, you agree to our <Text style={{ color: colors.theme, textAlign: 'left' }} >Terms and Conditions</Text> and <Text style={{ color: colors.theme, textAlign: 'left' }}>Privacy Statement</Text></Text>

                        </View>
                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}
const mapStateToProps = (state) => {
    return {
      fcmToken: state.fcmReducer.fcmToken
    }
  }
  
  export default connect(mapStateToProps, null)(SignUpScreen)
  
