import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from './LoginScreen/LoginSignUpHeader'
import SignUpScreen from './SignUp/SignUpScreen'
import AddressScreen from './SignUp/AddressScreen'
import ForgotPasswordScreen from './ForgotPassword/ForgotPasswordScreen'
import EmailVerificationScreen from './EmailVerification/EmailVerificationScreen'
import HomeNavigator from '../Main/HomeNavigation'

const LoginNavigator = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null,
   },
  },
  SignUp: {
    screen: SignUpScreen
  },
  Address: {
    screen: AddressScreen
  },
  ForgotPassword: {
    screen: ForgotPasswordScreen
  },
  EmailVerification: {
    screen: EmailVerificationScreen
  },
  // Home: {
  //   screen: HomeNavigator,
  //   navigationOptions: {
  //     header: null,
  //   }
  // },
},
{
    headerLayoutPreset: 'center',
    initialRouteName: 'Login',
});

export default createAppContainer(LoginNavigator);