import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, Alert } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import * as action from '../../../store/actions';
import { connect } from "react-redux";

class ForgotPasswordScreen extends Component {

    static navigationOptions = {
        title: 'Forgot Password',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    state = {
        slectedtab: 0,
        auth: {
            email: ''
        }
    }

    handleEmail(email) {
        this.setState({ ...this.state, auth: { ...this.state.auth, email: email } })
        console.log(this.state.auth);
    }

    handleReset = () => {
        this.props.dispatch(action.forgotPasswordApiAction(this.state.auth)).then(msg => {
            Alert.alert('Sucess', 'Please check your email for new password');
            this.props.navigation.navigate("Login");
        }).catch(err => {
            Alert.alert('Error', err)
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                            <Text style={{ color: '#777777', paddingVertical: 20, paddingHorizontal: 10, fontWeight: '500', fontSize: 15, textAlign: 'left' }}>
                                Please enter your registered Email to reset your password
                        </Text>
                            <CommonTextInput 
                            style={{ width: Dimensions.get('screen').width - 20 }} 
                            name='Email' 
                            placeholder='Required' 
                            onTextChange={email => this.handleEmail(email)}
                            keyboardType='email-address'
                            />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton
                                title='Reset'
                                style={{ width: Dimensions.get('screen').width - 40 }}
                                action={() => this.handleReset()}
                                disabled={this.state.auth.email === ''} />
                        </View>

                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default connect(null, null)(ForgotPasswordScreen);