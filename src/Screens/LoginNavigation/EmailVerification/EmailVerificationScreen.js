import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, ScrollView, Text, TouchableOpacity, Alert } from 'react-native'
import assets from 'res/Assets'
import colors from 'res/Colors'
import CommonTextInput from 'library/components/commonInputText'
import CircularCornerButton from 'library/components/circularCornerButton'
import GoogleSignInButton from 'library/components/googleSignInButton'
import FacebookLogInButton from 'library/components/facebookLogInButton'
import axios from 'axios'
import { connect } from "react-redux";
import * as action from '../../../store/actions';

class EmailVerificationScreen extends Component {

    static navigationOptions = {
        title: 'Email Verification',
        headerTintColor: colors.theme,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    state = {
        // slectedtab: 0
        emailverification: {
            email_verification_code: '',
            _id: ''
        },
    }

    verificationcodeHandler(email_verification_code) {
        this.setState({
            ...this.state,
            emailverification: {
                ...this.state.emailverification,
                email_verification_code: email_verification_code,
                _id: this.props.auth._id
            }
        })
        // this.state.emailverification.email_verification_code = email_verification_code;
        console.log(this.state.emailverification)
    }

    handleSubmit = () => {
        let reqPara={
            email:this.props.auth.email,
            code:this.state.emailverification.email_verification_code
        }
        this.props.dispatch(action.verifyCodeApiAction(reqPara)).then(
            (res) => {
                if (res.data.status === 1){
                    this.props.dispatch(action.emailVerificationApiAction(this.state.emailverification)).then(
                        () => this.props.navigation.navigate("Home")
                    )
                        .catch(
                            err => {
                                console.log('errrrrr', err)
                                Alert.alert('Hii')
                            }
                        )
                } else {
                    Alert.alert('Oops!', res.data.msg)
                }
            }
        )
            .catch(
                err => {
                    console.log('errrrrr', err)
                    Alert.alert('Hii')
                }
            )
        // if (this.props.auth.email_verification_code === this.state.emailverification.email_verification_code) {
        //     console.log('REQUEST DATA => ',this.state.emailverification)
        //     console.log("API CALLLL")
        //     this.props.dispatch(action.emailVerificationApiAction(this.state.emailverification)).then(
        //         () => this.props.navigation.navigate("Home")
        //     )
        //         .catch(
        //             err => {
        //                 console.log('errrrrr', err)
        //                 Alert.alert('Hii')
        //             }
        //         )
        // } else {
        //     Alert.alert("worng code")
        // }

    }

    componentDidMount(){
        console.log('SUCCESS REGSTER => ',this.props.auth)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: colors.backgroundGray }}>
                <ScrollView keyboardDismissMode='interactive' style={{ backgroundColor: colors.backgroundGray }}>
                    <View style={{ flex: 1, paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 15, backgroundColor: colors.backgroundGray }}>
                            <Text style={{ color: '#777777', paddingVertical: 20, paddingHorizontal: 10, fontWeight: '500', fontSize: 15, textAlign: 'left' }}>
                                Please enter verification code sent to your registered Email
                        </Text>
                            <CommonTextInput style={{ width: Dimensions.get('screen').width - 20, }} name='Verification Code' placeholder='Required' onTextChange={email_verification_code => this.verificationcodeHandler(email_verification_code)} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30, backgroundColor: colors.backgroundGray }}>
                            <CircularCornerButton title='Verify' style={{ width: Dimensions.get('screen').width - 40 }} action={() => this.handleSubmit()} />
                        </View>

                        {/* <Image resizeMode='contain' source={assets.googleSignIn} style={{ width: Dimensions.get('screen').width / 1.8 }}></Image> */}
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}
const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth
    }
}

export default connect(mapStatetoProps, null)(EmailVerificationScreen)
// export default EmailVerificationScreen