import { GET_GARDENINFO_DATA,GET_FARM_LIST, GET_FARM_DETAILS, EMPTY_FARM_DETAILS, STORE_CART_LIST, GET_FARM_FILTERED_LIST } from '../actions/types'

const initialState = {
  gardenInfo: {},
  farmInfo:[],
  farmDetails: {},
  cartData: null,
  farmfilteredInfo:[],
}

const farmReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_GARDENINFO_DATA:
      return {
        ...state,
        gardenInfo: action.data
      }
      case GET_FARM_LIST:
      return {
        ...state,
        farmInfo: action.data
      }
      case GET_FARM_FILTERED_LIST:
      return {
        ...state,
        farmfilteredInfo: action.data
      }
      case GET_FARM_DETAILS:
      return {
        ...state,
        farmDetails: action.data
      }
      case STORE_CART_LIST:
      return {
        ...state,
        cartData: action.data
      }
    default:
      return state
  }
}

export default farmReducers;