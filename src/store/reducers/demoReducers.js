import { DEMO_ACTION } from '../actions/types'

const initialState = {
  demodata: "Demo text"
}

const demoReducers = (state = initialState, action) => {
  switch (action.type) {
    case DEMO_ACTION:
      return {
        demodata: action.data
      };
    default:
      return state;
  }
};

export default demoReducers;