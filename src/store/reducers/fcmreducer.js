import {GET_FCM_TOKEN } from '../actions/types'

const initialState = {
 fcmToken:''
}
const fcmReducers = (state = initialState, action) => {
    switch (action.type) {
      case GET_FCM_TOKEN:
        return {
          ...state,
          fcmToken: action.data
        }
        default:
            return state
    }
}

    export default fcmReducers;