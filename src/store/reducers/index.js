import { combineReducers } from 'redux';
import demoReducers from './demoReducers'
import authreducers from './authreducers'
import farmReducers from './farmReducers'
import fcmReducer from './fcmreducer'
import paymentReducer from './paymentreducer'
const rootReducer = combineReducers({
  demoReducers,
  authreducers,
  farmReducers,
  fcmReducer,
  paymentReducer
});

export default rootReducer;