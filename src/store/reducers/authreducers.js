import { LOGIN_ACTION, SIGNUP_ACTION, USER_UPDATE, EMAILVERIFICATION_ACTION, LOGOUT } from '../actions/types'

const initialState = {
  auth: {},
}

const loginreducers = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_ACTION:
      return {
        ...state,
        auth: action.data
      }
    case SIGNUP_ACTION:
      return {
        ...state,
        auth: action.data
      }
    case USER_UPDATE:
      return {
        ...state,
        auth: action.data
      }
    case EMAILVERIFICATION_ACTION:
      return {
        ...state,
        auth: {
          ...state.auth,
          email_verification_status: 1
        }
      }
    case LOGOUT:
      return {
        auth: undefined
      }
    default:
      return state
  }
}

export default loginreducers;