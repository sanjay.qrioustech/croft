import axios from 'axios'
import { PAYMENT, BANK_DETAIL } from './types';
import { BASE_URL } from 'appconstant'
import { Platform } from 'react-native';

export const paymentInfo = (response) => {
    return {
        type: PAYMENT,
        data: response
    }
}

export const paymentApiAction = (request) => async dispatch => {
    console.log('kinjal => ', request)

    return await axios.post(BASE_URL + 'user/save-token', {
        user_id: request.user_id,
        token: request.token,
        email: request.email
    })
        // .then(function (response) {
        //     console.log(response);
        //     console.log('STATUS => ', response.data.status);
        //     if (response.data.status === 1) {
        //         dispatch(paymentInfo(response.data.data))
        //         return Promise.resolve();

        //     } else {
        //         console.log('ERROR => ', response)
        //         return Promise.reject(response.data.msg);
        //     }
        // })
};

export const bankInfo = (response) => {
    return {
        type: BANK_DETAIL,
        data: response
    }
}

export const bankApiAction = (request) => async dispatch => {
    console.log('kinjal => ', request)

    return await axios.post(BASE_URL + 'user/save-bank-details', {
        user_id: request.user_id,
        token: request.token,
    })};
export const getBankDetailsApiAction = (reqPara) => async dispatch => {
    return await axios.post(BASE_URL + 'user/get-bank-detail', reqPara)
    };