import { DEMO_ACTION } from './types';

export const demoactions = (response) => {
    return {
        type: DEMO_ACTION,
        data: response
    };
};
