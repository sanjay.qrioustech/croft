import axios from 'axios'
import { LOGIN_ACTION, FORGOT_PASSWORD_ACTION, SIGNUP_ACTION, EMAILVERIFICATION_ACTION, CHANGE_PASSWORD_ACTION, USER_UPDATE, LOGOUT,GET_FCM_TOKEN } from './types';
import { BASE_URL } from 'appconstant'

export const loginaction = (response) => {
  return {
    type: LOGIN_ACTION,
    data: response
  }
}

export const fcmtokenaction = (response) => {
  console.log('STOREID==>',response)
  return {
    type: GET_FCM_TOKEN,
    data: response
  }
}

export const forgotpasswordaction = (response) => {
  return {
    type: FORGOT_PASSWORD_ACTION,
    data: response
  }
}

export const changepasswordaction = (response) => {
  return {
    type: CHANGE_PASSWORD_ACTION,
    data: response
  }
}

export const signupaction = (response) => {
  return {
    type: SIGNUP_ACTION,
    data: response
  }
}
export const emailverificationaction = () => {
  return {
    type: EMAILVERIFICATION_ACTION,
  }
}

export const userupdateaction = (response) => {
  return {
    type: USER_UPDATE,
    data: response
  }
}

export const logout = () => {
  return {
    type: LOGOUT,
  }
}




export const loginApiAction = (request) => async dispatch => {
  return await axios.post(BASE_URL + 'user/login', {
    email: request.email,
    password: request.password,
    fcm_id:request.fcm_id
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        if (response.data.data[0].email_verification_status === 1) {
          dispatch(loginaction(response.data.data[0]))
          return 'Done';
        } else if (response.data.data[0].email_verification_status === 0) {
          console.log('Not Verified')
          dispatch(loginaction(response.data.data[0]))
          return 'not verified';
        } else {
          return response.data.msg
        }

      } else {
        console.log('ERROR => ', response)
        return response.data.msg;
      }
    }).catch((error) => {
      console.log(error.message)
   })
};

export const forgotPasswordApiAction = (request) => async dispatch => {
  await axios.post(BASE_URL + 'user/forgot_password', {
    email: request.email,
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        return Promise.resolve();

      } else {
        console.log('ERROR => ', response)
        return Promise.reject(response.data.msg);
      }
    })
};

export const changePasswordApiAction = (request) => async dispatch => {
  console.log('kinjal => ', request)

  await axios.post(BASE_URL + 'user/change-password', {
    _id: request._id,
    old_password: request.old_password,
    new_password: request.new_password,

  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        dispatch(changepasswordaction(response.data.data))
        return Promise.resolve();

      } else {
        console.log('ERROR => ', response)
        return Promise.reject(response.data.msg);
      }
    })
};

// export const signupApiAction = (request) => dispatch => {
//   return axios.post(BASE_URL + 'user/signup', {
//     fname: request.fname,
//     lname: request.lname,
//     email: request.email,
//     password: request.password,
//     phone: request.phone,
//     address: request.address,
//     city: request.city,
//     state: request.state,
//     zipcode: request.zipcode,
//   })
// };

export const signupApiAction = (request) => async dispatch => {
  await axios.post(BASE_URL + 'user/signup', {
    fname: request.fname,
    lname: request.lname,
    email: request.email,
    password: request.password,
    phone: request.phone,
    address: request.address,
    city: request.city,
    state: request.state,
    zipcode: request.zipcode,
    farm: request.farm,
    fcm_id:request.fcm_id
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        dispatch(signupaction(response.data.data))
        return Promise.resolve();

      } else {
        console.log('ERROR => ', response)
        return Promise.reject(response.data.msg);
      }
    })
};

export const verifyCodeApiAction = (reqPara) => async dispatch => {
  return await axios.post(BASE_URL + 'user/verified-email', reqPara)
};

export const emailVerificationApiAction = (request) => async dispatch => {
  await axios.post(BASE_URL + 'user/email-verification', {
    _id: request._id,
    email_verification_code: request.email_verification_code,
  })
    .then(function (response) {
      console.log(response);
      if (response.data.status === 1) {
        console.log('Hii')
        dispatch(emailverificationaction())
        return Promise.resolve();
      }
      else {
        console.log("VAALIDATEJISUFI MSSSSSGGG", response.data.msg);
        return Promise.reject(response.data.msg);
      }
    })
};

export const userUpdateApiAction = (request) => async dispatch => {
  console.log('userUpdateApiAction request => ', request)

  await axios.post(BASE_URL + 'user/update-user', request)
    .then(function (response) {
      console.log(response);
      if (response.data.status === 1) {
        console.log("response => ", response.data.msg);
        dispatch(userupdateaction(response.data.data))
        return Promise.resolve();
      }
      else {
        console.log("updateUser", response.data);
        return Promise.reject(response.data.msg);
      }
    })
};




export const getFeedbackApi = (request) => async dispatch => {
  await axios.post(BASE_URL + 'user/add-user-feedback', request)
    .then(function (response) {
      console.log(response);
      if (response.data.status === 1) {
        return Promise.resolve();
      }
      else {
        console.log("VAALIDATEJISUFI MSSSSSGGG", response.data.msg);
        return Promise.reject(response.data.msg);
      }
    })
};


export const addaddress = (data) => async dispatch => {
  await axios.post(BASE_URL + 'user/add-address', {
    user_id: data.user_id,
    address: data.address,
    city: data.city,
    state: data.state,
    zipcode: data.zipcode
  })
    .then(function (response) {
      console.log("Statua => ",response.data.status,response);
      if (response.data.status === 1) {
        dispatch(userupdateaction(response.data.data))
        return Promise.resolve();
      }else{
        console.log("updateUser", response.data.msg);
        return Promise.reject(response.data.msg);
      }
    })
};