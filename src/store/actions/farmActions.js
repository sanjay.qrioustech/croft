import axios from 'axios'
import { GET_GARDENINFO_DATA, GET_FARM_LIST, GET_FARM_FILTERED_LIST, GET_FARM_DETAILS, EMPTY_FARM_DETAILS, STORE_CART_LIST } from './types';
import { BASE_URL } from 'appconstant'
import { Platform } from 'react-native';
import moment from 'moment'
export const getGardenInfo = (response) => {
  return {
    type: GET_GARDENINFO_DATA,
    data: response
  }
}

export const getFarmInfo = (response) => {
  return {
    type: GET_FARM_LIST,
    data: response
  }
}

export const getFarmFilteredInfo = (response) => {
  return {
    type: GET_FARM_FILTERED_LIST,
    data: response
  }
}

export const getFarmDetails = (response) => {
  return {
    type: GET_FARM_DETAILS,
    data: response
  }
}

export const emptyFarmDetails = () => {
  return {
    type: EMPTY_FARM_DETAILS,
  }
}

export const getCartList = (response) => {
  return {
    type: STORE_CART_LIST,
    data: response
  }
}


export const getGardenInfoApiAction = () => async dispatch => {

  await axios.get(BASE_URL + 'getgardeninfo')
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      let gardenInfo = response.data.data.map((data, index) => {
        if (index === 0) {
          let item = {
            ...data,
            expanded: true
          }
          return item
        } else {
          let item = {
            ...data,
            expanded: false
          }
          return item
        }
      })
      dispatch(getGardenInfo(gardenInfo))
    })
};

export const imageUploadApiAction = (image, callback) => dispatch => {
  var data = new FormData();
  data.append('image',
    {
      uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
      type: image.type,
      name: `${image.fileName + moment().unix()}`
    });

    console.log('image req', data)
  return axios.post(BASE_URL + 'upload', data, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data;'
    },
    onUploadProgress: callback,
  })

};

export const imageDeleteApiAction = (image, callback) => dispatch => {
    console.log('image req', image)
  return axios.post(BASE_URL + 'remove', image)

};

export const addProductApiAction = (productInfo) => dispatch => {

  return axios.post(BASE_URL + 'product/add', productInfo)

};

export const getFarmInfoApiAction = (request) => async dispatch => {
  await axios.post(BASE_URL + 'user/farm-list', {
    latitude: request.latitude,
    longitude: request.longitude
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("FARM_LIST => ", farmInfo)
        dispatch(getFarmInfo(farmInfo))
      }
    })
};

export const getFarmInfoFilterApiAction = (request) => async dispatch => {
  await axios.post(BASE_URL + 'user/farm-list-filter', request)
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("FARM_LIST => ", farmInfo)
        dispatch(getFarmFilteredInfo(farmInfo))
      }
    })
};

export const getFarmDetailsApiAction = (id) => async dispatch => {
  await axios.post(BASE_URL + 'user/farm-detail', {
    user_id: id
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("FARM_DETAILS => ", farmInfo)
        dispatch(getFarmDetails(farmInfo))
      }
    })
};

export const getProductListApiAction = (id) => async dispatch => {
  return await axios.post(BASE_URL + 'product/filterlist', {
    user_id: id
  })
  // .then(function (response) {
  //   console.log(response);
  //   console.log('STATUS => ', response.data.status);
  //   if (response.data.status === 1) {
  //     let farmInfo = response.data.data
  //     console.log("FARM_PRODUCTS => ", farmInfo)
  //   }
  // })
};

export const getCartListApiAction = (id) => async dispatch => {
  await axios.post(BASE_URL + 'cart/cartlist', {
    user_id: id,
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("CART_LIST => ", farmInfo)
        dispatch(getCartList(farmInfo))
      }
    })
};


export const addToCartApiAction = (productInfo) => async dispatch => {
  await axios.post(BASE_URL + 'cart/addtocart', productInfo)
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("AddToCart => ", farmInfo)
        dispatch(getCartListApiAction(productInfo.user_id))
      }
    })
};

export const removeFromCartApiAction = (productInfo) => async dispatch => {
  await axios.post(BASE_URL + 'cart/deletecartItem', productInfo)
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("RemoveFromCart => ", farmInfo)
        dispatch(getCartListApiAction(productInfo.user_id))
      }
    })
};

export const removeAllFromCartApiAction = (user_id, cart_id) => async dispatch => {
  await axios.post(BASE_URL + 'cart/deletecart', {
    user_id: user_id,
    cart_id: cart_id
  })
    .then(function (response) {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        let farmInfo = response.data.data
        console.log("RemoveFromCart => ", farmInfo)
        dispatch(getCartList(null))
      }
    })
};

export const placeorderAction = (data) => async dispatch => {
  await axios.post(BASE_URL + 'order/placeorder', data)
    .then((response) => {
      console.log(response);
      console.log('STATUS => ', response.data.status);
      if (response.data.status === 1) {
        // let farmInfo = response.data.data
        // console.log("AddToCart => ", farmInfo)
        // dispatch(getCartListApiAction(productInfo.user_id))
        Promise.resolve();
      } else {
        Promise.reject(response.data.msg)
      }
    })
};

export const getReviewsListApiAction = (id) => async dispatch => {
  return await axios.post(BASE_URL + 'user/get-review', {
    user_id: id
  })
};

export const addReviewsApiAction = (reviewData) => async dispatch => {
  return await axios.post(BASE_URL + 'user/add-review', reviewData)
};

export const updateProductDataApiAction = (productData) => async dispatch => {
  return await axios.post(BASE_URL + 'product/update', productData)
};

export const getBoughtListApiAction = (id) => async dispatch => {
  return await axios.post(BASE_URL + 'order/boughtorder', {
    user_id: id
  })
};

export const getSoldListApiAction = (id) => async dispatch => {
  return await axios.post(BASE_URL + 'order/sellorder', {
    user_id: id
  })
};

export const acceptOrderApiAction = (id) => async dispatch => {
  return await axios.post(BASE_URL + 'order/acceptorder', {
    _id: id
  })
};

export const rejectOrderApiAction = (id, message) => async dispatch => {
  return await axios.post(BASE_URL + 'order/rejectorder', {
    _id: id,
    reason: message
  })
};
export const completeOrderApiAction = (reqPara) => async dispatch => {
  return await axios.post(BASE_URL + 'order/confirmorder', reqPara)
};