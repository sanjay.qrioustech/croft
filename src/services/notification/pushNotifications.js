import PushNotification from 'react-native-push-notification';
import { PushNotificationIOS } from 'react-native';

const configure = () => {
    
    PushNotification.configure({
        
        onRegister: function (token) {
            //process token
            console.log('TOKEN',JSON.stringify(token))
        },

        onNotification: function (notification) {
            // process the notification
            // required on iOS only
            notification.finish(PushNotificationIOS.FetchResult.NoData);
        },
        senderId: '123456789',

        permissions: {
            alert: true,
            badge: true,
            sound: true
        },

        popInitialNotification: true,
        requestPermissions: true,

    });
};

const localNotification = (message) => {
    PushNotification.localNotification({
        autoCancel: true,
        largeIcon: "ic_launcher",
        smallIcon: "ic_notification",
        // bigText: "My big text that will be shown when notification is expanded",
        // subText: "This is a subText",
        color: "green",
        vibrate: true,
        vibration: 300,
        title: "Order No : XXXXXXXXX",
        message: message,
        playSound: true,
        soundName: 'default',
         actions: '["DLCA", "DLCF"]',
    });
};

export {
    configure,
    localNotification,
};