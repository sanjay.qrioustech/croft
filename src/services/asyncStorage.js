import { AsyncStorage } from 'react-native';
import { Alert } from 'react-native';

export const saveStorage = async (key, item) => {
    try {
        await AsyncStorage.setItem(key, JSON.stringify(item));
    } catch (error) {
        // Error retrieving data
        Alert.alert("Error",error);
    }
};

export const getStorage = async key => {
    try {
        const retrievedItem = await AsyncStorage.getItem(key);
        const item = JSON.parse(retrievedItem);
        return item;
    } catch (error) {
        // Error retrieving data
        Alert.alert("Error",error);
    }
};

export const removeItemStorage = async key => {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    } catch (error) {
        Alert.alert("Error",error);
    }
};