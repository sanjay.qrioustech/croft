import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from './SplashScreen'
import LoginNavigator from '../Screens/LoginNavigation/LoginNavigator'
import HomeNavigator from '../Screens/Main/HomeNavigation'
import ReviewOrderScreen from '../Screens/Main/ReviewOrderScreen/ReviewOrderScreen'
import EditCartScreen from '../Screens/Main/EditCartScreen/EditCartScreen'

const AppNavigator = createSwitchNavigator({
  Splash: {
    screen: SplashScreen,
    navigationOptions: {
      header: null,
    }
  },
  Auth: {
    screen: LoginNavigator,
    navigationOptions: {
      header: null,
    }
  },
  Home: {
    screen: HomeNavigator,
    navigationOptions: {
      header: null,
    }
  }
},
  {
    initialRouteName: 'Splash',
    headerLayoutPreset: 'center',
    // headerMode: 'none'
  });

export default createAppContainer(AppNavigator);