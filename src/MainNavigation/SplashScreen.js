import React, { Component } from 'react'
import { SafeAreaView, View, Image, Dimensions, Platform } from 'react-native'
import assets from 'res/Assets'
import { StackActions, NavigationActions } from 'react-navigation';
import { demoactions } from '../store/actions';
import { connect } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
// import {firebase} from 'react-native-firebase/iid'
import { firebase } from '@react-native-firebase/iid';
import RNFirebase from '@react-native-firebase/app';
import '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import { PushNotificationIOS } from 'react-native';
import { configure } from '../services/notification/pushNotifications'
import * as action from '../store/actions';

async function getInstanceId() {
    const id = await firebase.iid().getToken();
    console.log('Fcm => ', id)
    return id;
}

const checkPermission = async () => {
    const enabled = await RNFirebase.messaging()
        .isRegisteredForRemoteNotifications;
    if (enabled) {
        await firebase.messaging().registerForRemoteNotifications();
        getFcmToken();
    } else {
        requestPermission();
    }
};

const getFcmToken = async () => {
    const fcmToken = await firebase.iid().getToken();
    if (fcmToken) {
        console.log("TOKEN=====>", fcmToken)
    } else {
    }
};

const requestPermission = async () => {
    try {
        await RNFirebase.messaging().requestPermission();
        // User has authorised
    } catch (error) {
        // User has rejected permissions
    }
};

let messageListener = async () => {
    // let notificationListener = RNFirebase.messaging().onMessage(notification => {
    // const {title, body} = notification;
    // showAlert(title, body);
    // });

    // let notificationOpenedListener = RNFirebase.notifications().onNotificationOpened(
    // notificationOpen => {
    // const {title, body} = notificationOpen.notification;
    // showAlert(title, body);
    // },
    // );

    // const notificationOpen = await RNFirebase.notifications().getInitialNotification();
    // if (notificationOpen) {
    // const {title, body} = notificationOpen.notification;
    // showAlert(title, body);
    // }

    messageListener = RNFirebase.messaging().onMessage(message => { });
};
class SplashScreen extends Component {

    componentDidMount() {
        let _this = this;
        PushNotification.configure({
        
            onRegister: function (tok) {
                //process token
                console.log('TOKEN',JSON.stringify(tok))
                _this.props.dispatch(action.fcmtokenaction(tok.token))
            },
        
            onNotification: function (notification) {
                // process the notification
                // required on iOS only
                notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
            senderId: '123456789',
        
            permissions: {
                alert: true,
                badge: true,
                sound: true
            },
        
            popInitialNotification: true,
            requestPermissions: true,
        
        });
        //configure();
        console.log("Data CUrrent", this.props.Auth)
        this.props.dispatch(demoactions("Hello Redux"))
        if(Platform.OS === 'ios'){
            getInstanceId().then(id => {
                console.log('ID => ', id);
                this.props.dispatch(action.fcmtokenaction(id))
            });
            
        }else{
            getInstanceId().then(id => {
                console.log('ID => ', id);
                this.props.dispatch(action.fcmtokenaction(id))
            });
        }
        checkPermission();
        messageListener();
    }


    isUserLoggedInCheck = async () => {
        //isUserLoggedInCheck().then(res => (console.log("2nd func",res))) 
        var status = ""
        await new Promise(r => setTimeout(r, 2000));
        // const resetAction = StackActions.reset({
        //     index: 0,
        //     actions: [NavigationActions.navigate({ routeName: isEmpty(this.props.Auth) ? 'Auth' : 'Home'})],
        //   });
        //   this.props.navigation.dispatch(resetAction);
        isEmpty(this.props.Auth) ? 'Auth' : 'Home'
        this.props.navigation.navigate(isEmpty(this.props.Auth) || this.props.Auth.email_verification_status === 0 ? 'Auth' : 'Home')
    }

    render() {
        this.isUserLoggedInCheck()
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                    <Image source={assets.logo_transperentsize} style={{ height: Dimensions.get('screen').width / 1.5, width: Dimensions.get('screen').width / 1.5, marginBottom: 100 }}></Image>
                </View>
            </SafeAreaView>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        Auth: state.authreducers.auth
    }
}

export default connect(mapStateToProps, null)(SplashScreen)