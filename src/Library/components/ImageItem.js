import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'

class ImageItem extends Component {
    render() {
        return (
            <View style={[{ marginVertical: 1 }, this.props.style]}>
                <View style={{ flex: 1, margin: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image resizeMode='cover' source={this.props.image} style={{ borderRadius: 5, height: Dimensions.get('screen').width/3, width: Dimensions.get('screen').width/3 }} />
                       
                    </View>
                </View>
            </View>
        )
    }
}

export default ImageItem