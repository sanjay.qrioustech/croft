import React, { Component } from 'react';
import { CheckBox } from 'react-native-elements'
import assets from 'res/Assets'
import colors from 'res/Colors'
import { Alert, LayoutAnimation, StyleSheet, View, Text, ScrollView, UIManager, TouchableOpacity, Platform, Image } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import isEmpty from 'lodash/isEmpty'
class Expandable_ListView extends Component {

    constructor() {

        super();

        this.state = {
            layout_Height: 0,
        }
    }

    componentDidMount(){
        if (this.props.item.expanded) {
            this.setState(() => {
                return {
                    layout_Height: null
                }
            });
        }
        else {
            this.setState(() => {
                return {
                    layout_Height: 0
                }
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.item.expanded) {
            this.setState(() => {
                return {
                    layout_Height: null
                }
            });
        }
        else {
            this.setState(() => {
                return {
                    layout_Height: 0
                }
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layout_Height !== nextState.layout_Height || this.props.selectedItems !== undefined) {
            return true;
        }
        return false;
        //return true;
    }

    show_Selected_Category = (item) => {

        // Write your code here which you want to execute on sub category selection.
        Alert.alert(item);

    }

    render() {
        return (
            <View style={styles.MainContainer}>

                <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction} style={styles.category_View}>

                    <Text style={styles.category_Text}>{this.props.item.category_Name} </Text>

                    <Image
                        source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
                        style={styles.iconStyle} />

                </TouchableOpacity>

                <View style={{ height: this.state.layout_Height, overflow: 'hidden', backgroundColor: '#fcfcfc', paddingTop: 5 }}>

                    {
                        this.props.item.sub_Category.map((item, key) => (

                            <TouchableOpacity key={key} style={styles.sub_Category_Text} >
                                <CheckBox
                                    key={key}
                                    title={item.name}
                                    // checkedIcon='dot-circle-o'
                                    // uncheckedIcon='circle-o'
                                    checkedColor={colors.theme}
                                    uncheckedColor='gray'
                                    checkedIcon={<AntDesign name='checkcircle' size={18} color={colors.theme} />}
                                    uncheckedIcon={<AntDesign name='checkcircleo' size={18} color='gray' />}
                                    checked={this.props.selectedItems.includes(item.name)}
                                    containerStyle={{ marginVertical: -20, backgroundColor: 'transparent', borderColor: 'transparent', marginBottom: -5 }}
                                    onPress={()=>this.props.onItemSelect(this.props.item.category_Name, item.name, this.props.selectedItems)}
                                ></CheckBox>
                                {/* <Text> {item.name} </Text>

                                <View style={{ width: '100%', height: 1, backgroundColor: '#000' }} /> */}

                            </TouchableOpacity>

                        ))
                    }

                </View>

            </View>

        );
    }
}
const styles = StyleSheet.create({

    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
        marginTop:2
    },

    iconStyle: {
        width: 20,
        height: 20,
        marginRight: 5,
        justifyContent: 'flex-end',
        alignItems: 'center',
        tintColor: Colors.text,
    },

    sub_Category_Text: {
        color: Colors.subText,
        padding: 10
    },

    category_Text: {
        textAlign: 'left',
        color: Colors.text,
        padding: 10, 
        fontSize:16
    },

    category_View: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white'
    },

    Btn: {
        padding: 10,
        backgroundColor: '#FF6F00'
    }

});

export default Expandable_ListView;