import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleText from 'library/components/SimpleText'
import { Pages } from 'react-native-pages';
// import { TouchableOpacity } from 'react-native-gesture-handler'

class HomePageFarmItem extends Component {
    render() {
        let imageWidth = Dimensions.get('screen').width - 20
        let imageHeight = this.props.imageHeight !== undefined ? this.props.imageHeight : 200
        return (
            <View style={[{ backgroundColor: 'white', marginVertical: 1, opacity: this.props.isOpen ? 1 : 0.6 }, this.props.style]}>
                <View style={{ flex: 1, margin: 10 }}>
                    <View style={{ width: '100%', height: imageHeight, borderRadius: 5, overflow: 'hidden' }}>

                        <Pages indicatorColor={Colors.theme}>
                            {
                                this.props.image.map((item) => (
                                    <View style={{ backgroundColor: 'transparent' }}>
                                        <Image resizeMode='cover' source={{ uri: 'https://croft-products.s3-us-west-1.amazonaws.com/' + item }} style={{ width: imageWidth, height: imageHeight, marginVertical: 5, borderRadius: 5, overflow: 'hidden' }} />
                                    </View>
                                ))
                            }
                        </Pages>
                    </View>
                    <TouchableOpacity onPress={this.props.action}>
                        <View>
                            <SimpleText style={{ marginVertical: 5, fontSize: 10, fontWeight: 'bold', color: Colors.theme }} text={this.props.isOpen ? 'OPEN NOW' : 'CLOSED NOW'} />
                            <SimpleText style={{ fontWeight: 'bold', fontSize: 15 }} text={this.props.farmName} />
                            <View style={{ marginVertical: 5, flexDirection: 'row' }}>
                                <SimpleText style={{ width: '50%', color: Colors.subtextGray, fontSize: 13 }} text={this.props.farmProducts} />

                                <SimpleText style={{ width: '50%', color: Colors.subtextGray, fontSize: 13, textAlign: 'right' }} text={this.props.distance} />
                            </View>
                            <View style={{ marginVertical: 0, flexDirection: 'row' }}>
                                <SimpleText style={{ width: '50%', color: Colors.subtextGray, fontSize: 13 }} text='organic' />

                                <SimpleText style={{ width: '50%', color: Colors.subtextGray, fontSize: 13, textAlign: 'right' }} text={this.props.orderType} />
                            </View>
                            <View style={{ marginVertical: 5, flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', width: '50%' }}>
                                    <Ionicons size={15} name='md-star' color={Colors.theme} />
                                    <SimpleText style={{ color: Colors.theme, fontSize: 13, marginLeft: 5 }} text='4' />
                                </View>
                                <SimpleText style={{ width: '50%', color: Colors.subtextGray, fontSize: 13, textAlign: 'right' }} text={this.props.isOpen ? 'Closes at '+this.props.closeTime : 'Opens at '+this.props.openTime} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default HomePageFarmItem