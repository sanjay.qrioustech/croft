import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'

class ReviewsListItem extends Component {
    render() {
        return (
            <View style={[{ backgroundColor: 'white', marginVertical: 1 }, this.props.style]}>
                <View style={{ flex: 1, margin: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image resizeMode='cover' source={this.props.image} style={{ borderRadius: 20, height: 40, width: 40 }} />
                        <View>
                            <SimpleText style={{ marginHorizontal: 5, fontSize: 12, fontWeight: 'bold' }} text={this.props.name} />
                            <View style={{ marginHorizontal: 5, marginTop: 3, flexDirection: 'row', alignItems:'center' }}>
                                <Ionicons size={15} name='md-star' color={Colors.theme}  />
                                <SimpleText style={{ color: Colors.theme, fontSize: 13, marginLeft: 5 }} text={this.props.stars} />
                            </View>
                        </View>
                    </View>
                    <SimpleText style={{ color: Colors.subtextGray, fontSize: 13, marginTop: 5 }} text={this.props.review} />
                </View>
            </View>
        )
    }
}

export default ReviewsListItem