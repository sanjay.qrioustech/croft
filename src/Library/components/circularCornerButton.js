import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native'
import Colors from 'res/Colors'

class CircularCornerButton extends Component {

    componentDidMount(){
    }

    render() {
        console.log("Disabled" ,this.props.disabled)

        return (
            this.props.disabled ?
                <TouchableOpacity disabled={this.props.disabled} style={[{ alignItems: 'center', justifyContent: "center", backgroundColor: Colors.disabledtheme, borderRadius: 20, height: 40}, this.props.style]}>
                    <Text style={{ color: 'white', fontWeight: 'bold' }}> {this.props.title} </Text>
                </TouchableOpacity> 
                
                :

                <TouchableOpacity onPress={this.props.action} style={[{ alignItems: 'center', justifyContent: "center", backgroundColor: Colors.theme, borderRadius: 20, height: 40 }, this.props.style]}>
                    <Text style={{ color: 'white', fontWeight: 'bold' }}> {this.props.title} </Text>
                </TouchableOpacity>
        )
    }
}
export default CircularCornerButton