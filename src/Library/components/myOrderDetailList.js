import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'
import moment from 'moment'
import Divider from 'library/components/divider'

class MyOrderDetailList extends Component {
    render() {
        return (
            <View style={[{ marginVertical: 5}, this.props.style]}>
                <View style={{ flex: 1,}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <View style={{flex:1, alignSelf: 'flex-start', justifyContent: 'flex-start'}}>
                            <SimpleText style={{ marginHorizontal: 10, fontSize: 16,color:Colors.black}} text={this.props.name} />
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>

                            <View style={{flexDirection:'row'}}>
                            <SimpleText style={{marginLeft:22,color: Colors.subtextGray, fontSize: 16, marginTop: 5 }} text={this.props.quantity + ' x'} />
                            <SimpleText style={{ marginLeft:4,color: Colors.subtextGray, fontSize:16, marginTop: 5 }} text={'$ '+this.props.price} />
                            </View>

                            <View>
                            <SimpleText style={{ marginHorizontal: 10, color: Colors.subtextGray, fontSize:16, marginTop: 5 }} text={'$ '+this.props.total} />
                            </View>
                            </View>
                        </View>
                    </View>
                    <Divider/>
                </View>
            </View>
        )
    }
}

export default MyOrderDetailList