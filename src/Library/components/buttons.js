import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native'
import Colors from 'res/Colors'

class Button extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.action} style={[{ flex: 1, alignItems: 'center', justifyContent: 'center', borderRadius: 0, height: 25, backgroundColor: Colors.theme }, this.props.style]}>
                <Text style={{fontWeight:'bold', fontSize: 11, color: 'white' }}> {this.props.title} </Text>
            </TouchableOpacity>
        )
    }
}
export default Button