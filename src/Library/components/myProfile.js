import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, TouchableOpacity, StyleSheet, Modal, Alert, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleText from 'library/components/SimpleText'
import { Pages } from 'react-native-pages';
import CardView from 'react-native-cardview';
import Assets from 'res/Assets'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { CheckBox } from 'react-native-elements'
import { KeyboardAvoidingView } from 'react-native';
import colors from 'res/Colors'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
export default class MyProfile extends React.Component{
render(){
    return(
        <View style={{}}>
        <TouchableOpacity onPress={this.props.action}>
        <View style={[Styles.containerSingle, {flexDirection:'row',justifyContent:'space-between',alignItems:'center', paddingTop: 10, }]}>
            <SimpleText style={Styles.titleSingle} text={this.props.text}></SimpleText>
            <MaterialIcons name='keyboard-arrow-right' size={20} style={{marginRight:10,color:'#b6b6b6'}}></MaterialIcons>

        </View>
        </TouchableOpacity>
        </View>
        );
    }
}
const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row', marginLeft: 10, marginTop: 10
    },
    containerSingle: {
        flexDirection: 'row', marginTop: 5, marginBottom: 5
    },
    title: {
        fontWeight: 'bold', color: colors.theme, fontSize: 14, marginRight: 10
    },
    titleSingle: {
        fontWeight: 'bold',paddingHorizontal: 10, color: colors.black, alignSelf: 'center', fontSize: 14
    },
    subContainer:
    {
        flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 5, paddingLeft: 10
    },
    subTitle: {
        marginTop: 2, fontWeight: 'bold', paddingLeft: 15, color: colors.text, fontSize: 13
    },
    text: {
        marginTop: 2, paddingLeft: 15, color: colors.text, fontSize: 13,
    },
    view: {
        padding: 6,
        marginTop: 6
    }

});

