import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text } from 'react-native'
import { Input, Icon } from 'react-native-elements'
import Colors from 'res/Colors'
import Ionicon from 'react-native-vector-icons/Ionicons'

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } } };
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } } };


class GooglePlacesAutoComplete extends React.Component {

    state = {
        text: ''
    }
    nameTextChange(text) {
        this.setState({
            text: text
        })
    }

    render() {
        return (
            <View style={{ marginVertical: 0.5, height: 50, flex: 1, width: "100%", }}>
                <View >
                    {this.props.isFullInput == undefined ?
                        <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white', paddingLeft: 10 }}>
                            {/* <View style={{ flexDirection: "row", height: 50, flex: 1, width: "100%", alignItems: 'center', justifyContent: 'flex-end', backgroundColor: 'white' }}> */}
                            <Text style={{ width: '35%', paddingRight: 10, color: Colors.black }}>{this.props.name}</Text>
                            <GooglePlacesAutocomplete
                                placeholder='Required'
                                minLength={2} // minimum length of text to search
                                autoFocus={false}
                                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
                                listViewDisplayed='auto'    // true/false/undefined
                                fetchDetails={true}
                                renderDescription={row => row.description} // custom description render
                                onPress={(data, details = "ab") => { // 'details' is provided when fetchDetails = true
                                    console.log(data, details);
                                }}

                                getDefaultValue={() => ''}

                                query={{
                                    // available options: https://developers.google.com/places/web-service/autocomplete
                                    key: 'AIzaSyAQ4zdRDkuoPojGvGTnKYFFWdtjbJDPl7k',                               
                                    language: 'en', // language of the results
                                    types: '(cities)' // default: 'geocode'
                                }}

                                styles={{
                                    textInputContainer: {
                                      backgroundColor: 'rgba(0,0,0,0)',
                                      borderTopWidth: 0,
                                      borderBottomWidth:0
                                    },
                                    textInput: {
                                      marginLeft: 0,
                                      marginRight: 0,
                                      height: 38,
                                      color: '#5d5d5d',
                                      fontSize: 16
                                    },
                                    predefinedPlacesDescription: {
                                      color: 'black'
                                    },
                                  }}

                                currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                                currentLocationLabel="Current location"
                                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                GoogleReverseGeocodingQuery={{
                                    // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                }}
                                GooglePlacesSearchQuery={{
                                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                    rankby: 'distance',
                                    type: 'cafe'
                                }}

                                GooglePlacesDetailsQuery={{
                                    // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                                    fields: 'formatted_address',
                                }}

                                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                                predefinedPlaces={[homePlace, workPlace]}

                                debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                //  renderLeftButton={() => <Ionicon name='ios-arrow-down' size={15} style={{ marginLeft: 5, marginTop: 3 }} color='black' />}
                                //  renderRightButton={() => <Text>Custom</Text>}
                            />
                        </View>
                        :

                        <Input
                            inputStyle={{ fontSize: 15, color: Colors.subtextGray }}
                            style={{ fontSize: 12 }}
                            secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                            value={this.state.text}
                            onChangeText={(text) => this.nameTextChange(text)}
                            placeholder={this.props.placeholder}
                            containerStyle={[this.props.style]}
                        />
                    }

                    {/* <Input
                    secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                    value={this.state.text}
                    onChangeText={(text) => this.nameTextChange(text)}
                    label={this.state.text === "" ? "" : this.props.placeholder}
                    labelStyle={styles.placeholderLabel}
                    placeholder={this.props.placeholder}
                    containerStyle={[{ padding: 20 }, this.props.style]} /> */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    placeholderLabel: {
        color: Colors.theme
    },
    headerView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: "#cf0336",
        alignItems: 'center'
    },
    backImage: {
        height: 25,
        width: 25,
        margin: 10
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'left'
    },
    exitButton: {
        color: 'white',
        fontSize: 15,
        alignSelf: 'center',
        marginRight: 10,
        textAlign: 'center'

    },
    subHeaderText: {
        color: 'black',
        fontSize: 16,
        alignSelf: 'flex-start',
        textAlign: 'left',
        paddingTop: 20,
        paddingLeft: 10,
        fontWeight: '400'
    },
    categoryView: {
        backgroundColor: 'black',
        borderRadius: 15,
        height: 30,
        width: 200,
        marginLeft: 20,
        marginTop: 5,
        justifyContent: 'center'
    },
    featureView: {
        marginLeft: 20,
        marginTop: 15,
        justifyContent: 'center'
    }
})

export default GooglePlacesAutoComplete