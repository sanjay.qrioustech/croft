import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'
import moment from 'moment'
class ProductListItemhorizontal extends Component {
    render() {
        return (
            <View style={[{ marginVertical: 1 }, this.props.style]}>
                <View style={{ flex: 1, marginLeft: 10 }}>
                    <View style={{ flexDirection: 'column', justifyContent: 'flex-start', }}>
                        <Image resizeMode='cover' source={{uri: 'https://croft-products.s3-us-west-1.amazonaws.com/'+this.props.image[0]}} style={{ borderRadius: 5, height: Dimensions.get('screen').width / 3, width: Dimensions.get('screen').width / 3 }} />
                        <View style={{ flex: 1,}}>
                            <View style={{ flexDirection: 'row', marginTop: 5, justifyContent: "space-between", alignItems: 'center' }}>
                                <SimpleText style={{ fontSize: 15, width : "50%" }} numberOfLines={1} text={this.props.name} />
                                <SimpleText style={{ color: Colors.subtextGray, fontSize: 10 }} text={this.props.price} />
                            </View>
                            <SimpleText style={{ marginTop: 5, color: Colors.subtextGray, fontSize: 12, }} text={moment(this.props.date).format('DD/MM/YYYY')} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default ProductListItemhorizontal