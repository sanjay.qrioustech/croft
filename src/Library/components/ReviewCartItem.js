import React, { Component } from 'react'
import { SafeAreaView, View, Image, TouchableOpacity, Text, TextInput, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'
import { Icon } from 'react-native-elements';
import DividerSmall from 'library/components/divider_small_margin'

class ReviewCartItem extends Component {


    state = {
        Quantity: '5'
    }

    onTextChange = (value) => {
        this.setState({
            Quantity: value
        })
    }


    render() {


        return (
            <View style={{ flex: 1, height: 100, margin: 5 }}>
                <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: 'white', padding: 10, borderRadius: 9, backgroundColor: 'white' }}>
                    <Image resizeMode='cover' source={{uri:this.props.productImage}} style={{ flex: 1, height: 100, width: 100, alignSelf: 'center',borderRadius: 5, overflow: 'hidden' }} />
                    <View style={{ flex: 3, marginLeft: 10, }}>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start' }}>
                            <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                                <SimpleText text={this.props.productName} style={{ fontWeight: 'bold' }} />
                                <Icon
                                    color={Colors.theme}
                                    size={20}
                                    style={{alignSelf: 'flex-end'}}
                                    type='material-community'
                                    name='lead-pencil'
                                    onPress={this.props.EditAction}
                                />
                            </View>
                            <SimpleText text={this.props.farmName} style={{ color: Colors.subtextGray }} />
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                            <SimpleText text='Quantity :' />
                            <SimpleText text={this.props.productQuantity} style={{ marginLeft: 10 }} />
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', }}>
                            <SimpleText text={"$" + this.props.productPrice} />
                        </View>
                    </View>                    
                </View>
            </View>
        )
    }
}

export default ReviewCartItem