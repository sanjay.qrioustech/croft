import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text } from 'react-native'
import { Input, Icon } from 'react-native-elements'
import Colors from 'res/Colors'
class CommonInputTextMultilineNoTitle extends React.Component {

    state = {
        text: 'Delivery was very fast, i needed the products in one hour they delivered in time. All products are very fresh. Very nice farm. All products are very fresh and organic.'
    }
    nameTextChange(text) {
        this.setState({
            text: text
        })
    }

    render() {
        return (
            <View style={[{ marginTop:5, flex: 1, width: "100%" }]}>
                <View >
                    {this.props.isFullInput == undefined ?
                        <View  style={{ width: '100%', height:100, justifyContent: 'flex-start', alignSelf:'center'}}>
                            <TextInput
                                style={[{ height: 100, marginVertical:5,alignSelf:'flex-start'}, this.props.style]}
                                secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                                value={this.props.value}
                                onChangeText={(text) => this.props.onChangeText(text)}
                                placeholder={this.props.placeholder}
                                placeholderTextColor='#454545'
                                multiline
                                numberOfLines={4}
                            />
                         </View>
                        :

                        <Input
                            inputStyle={{fontSize: 15}}
                            style={{fontSize: 12}}
                            secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                            value={this.state.text}
                            onChangeText={(text) => this.nameTextChange(text)}
                            placeholder={this.props.placeholder}
                            containerStyle={[ this.props.style]} 
                            />
                    }

                    {/* <Input
                    secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                    value={this.state.text}
                    onChangeText={(text) => this.nameTextChange(text)}
                    label={this.state.text === "" ? "" : this.props.placeholder}
                    labelStyle={styles.placeholderLabel}
                    placeholder={this.props.placeholder}
                    containerStyle={[{ padding: 20 }, this.props.style]} /> */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    placeholderLabel: {
        color: Colors.theme
    },
    headerView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: "#cf0336",
        alignItems: 'center'
    },
    backImage: {
        height: 25,
        width: 25,
        margin: 10
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'left'
    },
    exitButton: {
        color: 'white',
        fontSize: 15,
        alignSelf: 'center',
        marginRight: 10,
        textAlign: 'center'

    },
    subHeaderText: {
        color: 'black',
        fontSize: 16,
        alignSelf: 'flex-start',
        textAlign: 'left',
        paddingTop: 20,
        paddingLeft: 10,
        fontWeight: '400'
    },
    categoryView: {
        backgroundColor: 'black',
        borderRadius: 15,
        height: 30,
        width: 200,
        marginLeft: 20,
        marginTop: 5,
        justifyContent: 'center'
    },
    featureView: {
        marginLeft: 20,
        marginTop: 15,
        justifyContent: 'center'
    }
})

export default CommonInputTextMultilineNoTitle