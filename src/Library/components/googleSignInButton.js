import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
import Colors from 'res/Colors'
import Assets from 'res/Assets'

class GoogleSignInButton extends Component
{
    render(){
        return(
            <TouchableOpacity style={[{ flex: 1, backgroundColor: Colors.googleBlue, borderRadius: 5 }, this.props.style]}>
                <View style={[{ width:'100%', flexDirection: 'row', alignItems: 'center' }, this.props.style]}>
                    <Image resizeMode="contain" source={ Assets.googleIcon } style={{ marginHorizontal: 5,height: 30, width: 30, overflow: 'hidden', borderRadius:15, borderWidth:3, borderColor: Colors.googleBlue}} />
                    <Text adjustsFontSizeToFit={true} style={{ width: '70%' ,color: 'white', fontWeight: 'bold', textAlign: 'center'}}>Continue with Google</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export default GoogleSignInButton