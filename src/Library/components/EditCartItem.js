import React, { Component } from 'react'
import { SafeAreaView, View, Image, TouchableOpacity, Text, TextInput, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'
import ModalDropdown from 'react-native-modal-dropdown';
import { IMAGE_BASE_URL } from 'appconstant'
class EditCartItem extends Component {


    state = {
        Quantity: '5'
    }

    onTextChange = (value) => {
        this.setState({
            Quantity: value
        })
    }
    render() {


        return (
            <View style={{ height: 170, margin: 5 }}>
                <View style={{ flex: 1, flexDirection: 'row', borderWidth: 1, borderColor: 'white', padding: 10, borderRadius: 9, backgroundColor: 'white' }}>
                    <Image resizeMode='cover' source={{ uri: IMAGE_BASE_URL + this.props.productImage }} style={{ height: 100, width: 100, alignSelf: 'flex-start', borderRadius: 5, overflow: 'hidden' }} />
                    <View style={{ flex: 3, marginLeft: 10, justifyContent: 'space-between' }}>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start' }}>
                            <SimpleText text={this.props.productName} style={{ fontWeight: 'bold' }} />
                            <SimpleText text={this.props.farmName} style={{ marginTop: 5, fontSize: 12, color: Colors.subtextGray }} />
                            <SimpleText text={'Available Stock: ' + this.props.availableStock} style={{ marginTop: 5, fontSize: 12, color: Colors.subtextGray }} />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 20 }}>
                            <SimpleText text='Quantity :' />
                            <View style={{ width: 35, marginLeft: 10, borderWidth: 0.5, height: 35, alignItems: 'center', flexDirection: 'row', borderRadius: 5 }}>
                                <TextInput value={this.props.quantity} style={{ flex: 1, fontSize: 14, alignSelf: "center", textAlign: 'center' }} onChangeText={(text) => this.props.onTextChange(text)} keyboardType='number-pad'>
                                </TextInput>
                            </View>
                            <TouchableOpacity disabled={true} style={{ height: 25, alignItems: 'center', flexDirection: 'row', borderRadius: 5 }}>
                                <ModalDropdown
                                    disabled={true}
                                    defaultValue={this.props.unit}
                                    defaultIndex={0}
                                    style={{ marginHorizontal: 10 }}
                                    textStyle={{ fontSize: 12 }}
                                    dropdownTextStyle={{ fontSize: 15 }}
                                    options={['Pounds', 'Dozens']}
                                />
                                {/* <SimpleText style={{ marginHorizontal: 10, fontSize: 15, fontWeight: '400' }} text="Pounds" /> */}
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 3 }}>
                            <SimpleText text={'$' + this.props.productPrice + '/' + this.props.unit} style={{ flex: 1 }} />
                            <TouchableOpacity disabled={this.props.updateDisable} onPress={this.props.onUpdate}>
                                <SimpleText text=' Update ' style={{ flex: 0.5, marginLeft: 18, borderColor: this.props.updateDisable ? 'gray' : Colors.green, borderWidth: .5, fontSize: 12, padding: 5, color: this.props.updateDisable ? 'gray' : Colors.green, borderRadius: 5 }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.props.onRemove}>
                                <SimpleText text=' Remove ' style={{ flex: 0.5, marginLeft: 18, borderColor: Colors.red, borderWidth: .5, fontSize: 12, padding: 5, color: Colors.red, borderRadius: 5 }} />
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </View>

        )
    }
}

export default EditCartItem