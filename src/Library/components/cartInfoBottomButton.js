import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import Assets from 'res/Assets'
import Colors from 'res/Colors'
import SimpleText from 'library/components/SimpleText'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux';

class CartInfoBottomButton extends React.Component {
    componentDidMount() {
        console.log('farm_name',this.props.cartList)
        
    }
    render() {
        console.log('farm_name',this.props.cartList)
        return ( this.props.cartList !== null ?
            <View>
                <View style={{
                    width: '50%',
                    //height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    position: 'absolute', //Here is the trick
                    bottom: Platform.OS === 'ios' ? 10 : 10,
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ReviewOrder')} style={[{ alignItems: 'center', justifyContent: "center", backgroundColor: '#de2618', borderRadius: 20 }]}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Ionicons name={'md-cart'} size={22} color='white' style={{ marginLeft: 15 }} />
                            <View style={{ flex: 3, flexDirection: 'column', paddingLeft: 10, justifyContent: 'space-between', alignItems: 'center', paddingVertical: 10 }}>
                                <SimpleText style={{ fontWeight: 'bold', color: 'white', fontSize: 8 }} text='VIEW CART'></SimpleText>
                                <SimpleText style={{ fontWeight: 'bold', fontSize: 12, color: 'white', paddingBottom: 1 }} text={this.props.cartList.farm_name}></SimpleText>
                            </View>
                            <SimpleText style={{ paddingRight: 10, color: 'white', fontSize: 10 }} text={ this.props.cartList.products.length+' items'}></SimpleText>
                        </View>
                    </TouchableOpacity>
                </View>
            </View> : null
        );
    }
}
const mapStatetoProps = (state) => {
    return {
        auth: state.authreducers.auth,
        cartList: state.farmReducers.cartData
    }
}

export default connect(mapStatetoProps, null)(CartInfoBottomButton)