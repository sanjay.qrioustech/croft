import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
import Colors from 'res/Colors'
import Assets from 'res/Assets'

class FacebookLogInButton extends Component
{
    render(){
        return(
            <TouchableOpacity style={[{ flex: 1, backgroundColor: Colors.facebookBlue, borderRadius: 5 }, this.props.style]}>
                <View style={[{ width:'100%', flexDirection: 'row', alignItems: 'center' }, this.props.style]}>
                    <Image resizeMode="contain" source={ Assets.facebookLogo } style={{ marginHorizontal: 10,height: 25, width: 25}} />
                    <Text adjustsFontSizeToFit={true} style={{ width: '70%' ,color: 'white', fontWeight: 'bold', textAlign: 'center'}}>Continue with Facebook</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export default FacebookLogInButton