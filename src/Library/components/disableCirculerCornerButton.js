import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native'
import Colors from 'res/Colors'

class DisableCirculerCornerButton extends Component
{
    render(){
        return(
            <TouchableOpacity onPress={this.props.action} style={[{flex: 1, alignItems: 'center', justifyContent:"center",backgroundColor: Colors.backgroundGray, borderRadius: 20, height: 40},this.props.style]}>
                <Text style={{color: 'white', fontWeight: 'bold'}}> {this.props.title} </Text>
            </TouchableOpacity>
        )
    }
}
export default DisableCirculerCornerButton