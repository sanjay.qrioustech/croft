import React, { Component } from 'react'
import { Text, Platform } from 'react-native'

export default class SimpleText extends Component
{
    render(){
    return(<Text numberOfLines={this.props.numberOfLines !== undefined ? 1 : undefined} style={[{fontFamily: Platform.OS == 'ios' ? 'Verdana' : 'catamaran'}, this.props.style]}>{this.props.text}</Text>)
    }
}

