import React, { Component } from 'react'
import { View } from 'react-native'

export default class Divider extends Component {
    render() {
        return (<View style={{ marginHorizontal: 5, height: 0.5, backgroundColor: '#b6b6b6', margin: 5 }}></View>
        )
    }
}