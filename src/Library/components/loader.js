import React, {Component} from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

export default class Loader extends Component {
    render(){
        return (
            <View
                style={loader.view}>
                <ActivityIndicator size="large" color="#000000" style={loader.centerView}/>
            </View>
        )
    }
}

const loader = StyleSheet.create({
    view : {
        height : '100%',
        width : '100%',
        backgroundColor : 'rgba(0,0,0,0.3)',
        position : 'absolute'
    },
    centerView : {
        width : '100%',
        height : '100%',
        flex : 1,
        justifyContent : 'center'
    }
})