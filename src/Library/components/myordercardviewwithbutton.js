import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, TouchableOpacity, StyleSheet, Modal, Alert, TextInput } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleText from 'library/components/SimpleText'
import { Pages } from 'react-native-pages';
import CardView from 'react-native-cardview';
import Assets from 'res/Assets'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { CheckBox } from 'react-native-elements'
import { KeyboardAvoidingView } from 'react-native';

// import { TouchableOpacity } from 'react-native-gesture-handler'

class MyOrderCardViewWithButton extends Component {

    state = {
        modalVisible: false,
        message: ''
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    render() {
        let curremtView = View
        switch (this.props.orderStatus) {
            case '1':
                curremtView = <View style={{flexDirection:'row'}}>
                    <TouchableOpacity onPress={this.props.onAccept}>
                        <SimpleText text="Accept" style={{ color: Colors.green, fontSize: 12, borderWidth: .5, borderRadius: 5, paddingHorizontal: 6, paddingVertical: 5, borderColor: Colors.green }}></SimpleText>
                    </TouchableOpacity >
                    <TouchableOpacity onPress={() => { this.setModalVisible(true) }}>
                        <SimpleText text="Reject" style={{ color: Colors.red, fontSize: 12, borderWidth: .5, borderRadius: 5, paddingHorizontal: 6, paddingVertical: 5, marginLeft: 10, borderColor: Colors.red }} ></SimpleText>
                    </TouchableOpacity>
                </View>
                break;
            case '2':
                curremtView = <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={this.props.onComplete}>
                    <SimpleText text="Complete" style={{ color: Colors.green, fontSize: 12, borderWidth: .5, borderRadius: 5, paddingHorizontal: 6, paddingVertical: 5, borderColor: Colors.green }}></SimpleText>
                </TouchableOpacity >
                <TouchableOpacity onPress={() => { this.setModalVisible(true) }}>
                    <SimpleText text="Cancel" style={{ color: Colors.red, fontSize: 12, borderWidth: .5, borderRadius: 5, paddingHorizontal: 6, paddingVertical: 5, marginLeft: 10, borderColor: Colors.red }} ></SimpleText>
                </TouchableOpacity>
            </View>
                break;
            case '3':
                curremtView = <SimpleText text="Rejected" style={{ color: Colors.red, fontSize: 12 }}></SimpleText>
                break;
            case '4':
                curremtView = <SimpleText text="Completed" style={{ color: Colors.green, fontSize: 12 }}></SimpleText>
                break;
            default:
                curremtView = View
        }
        return (
            <CardView
                style={Styles.card}
                cardElevation={1}
                cardMaxElevation={2}
                cornerRadius={3}>
                <View style={Styles.farmContainer}>
                    <Image source={{uri: this.props.farmImage}} style={Styles.image} />
                    <View style={{ paddingLeft: 10, flex: 2 }}>
                        <SimpleText text={this.props.farmName} style={{ fontWeight: 'bold', fonSize: 18 }}></SimpleText>
                        <SimpleText text={this.props.farmAddress} style={{ color: Colors.subtextGray, fontSize: 12 }}></SimpleText>
                    </View>
                    {curremtView}
                </View>
                <View style={{ paddingLeft: 5, margin: 3 }}>
                    <SimpleText style={{ marginTop: 8, paddingLeft: 10, color: Colors.text, fontSize: 12 }} text='PRODUCT'></SimpleText>
                    <SimpleText style={{ marginTop: 1, paddingLeft: 10, color: Colors.subtextGray, fontSize: 12 }} text={this.props.productName}></SimpleText>
                </View>
                <View style={{ paddingLeft: 5, margin: 3 }}>
                    <SimpleText style={{ marginTop: 8, paddingLeft: 10, color: Colors.text, fontSize: 12 }} text='ORDERED DATE'></SimpleText>
                    <SimpleText style={{ marginTop: 1, paddingLeft: 10, color: Colors.subtextGray, fontSize: 12 }} text={this.props.orderDate}></SimpleText>
                </View>
                <View style={{ paddingLeft: 5, margin: 3 }}>
                    <SimpleText style={{ marginTop: 8, paddingLeft: 10, color: Colors.text, fontSize: 12 }} text='TOTAL AMOUNT'></SimpleText>
                    <SimpleText style={{ marginTop: 1, paddingLeft: 10, color: Colors.subtextGray, fontSize: 12 }} text={'$'+this.props.total}></SimpleText>
                </View>

                <View style={{ marginTop: 22 }}>
                    <Modal
                        animationType='slide'
                        transparent={true}
                        // transparent={Colors.disabledtheme}
                        // backgroundColor={Colors.disabledtheme}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }}>

                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',
                            backgroundColor: 'rgba(211,211,211,0.4)',
                            alignItems: 'center'
                        }}>
                            <View style={{
                                 width: '80%', backgroundColor: Colors.white, padding: 20, borderTopEndRadius: 10, borderTopStartRadius: 10,
                                borderWidth: 1,
                                borderColor: '#fff',
                            }}>
                                {/* <View style={{ flexDirection: 'row-reverse', width: '100%' }}>
                                    <AntDesign name='close' size={25} style={{ padding: 5 }} color={Colors.theme} onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }}
                                    ></AntDesign>
                                </View> */}
                                <View style={{flexDirection: "row", justifyContent: 'space-between', alignItems: 'center'}} >
                                <SimpleText style={{ color: Colors.theme, fontSize: 16, }} text='Why do you want to Reject?' />
                                <AntDesign name='close' color={Colors.theme} size={25} onPress={()=>{
                                    this.setModalVisible(!this.state.modalVisible);
                                }} />
                                </View>
                                
                                {/* <TextInput placeholder='select location'
                                    onChangeText={this.updateSearch}
                                    // style={{marginTop:200}}
                                    value={search}></TextInput> */}
                                {/* <CheckBox
                                    title='Delivery distance too far?'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    // style={{marginTop:40,backgroundColor:'transparent', borderColor: 'transparent' }}
                                    checked={this.state.selectedIndex === 1}
                                    onPress={() => this.setState({ selectedIndex: 1, message: 'Delivery distance too far' })}
                                    containerStyle={{ marginTop: 5, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox> */}

                                <CheckBox
                                    title='Delivery time not available'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={this.state.selectedIndex === 2}
                                    onPress={() => this.setState({ selectedIndex: 2, message: 'Delivery time not available' })}
                                    containerStyle={{ marginTop: 5, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                                <CheckBox
                                    title='Product sold out'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={this.state.selectedIndex === 3}
                                    onPress={() => this.setState({ selectedIndex: 3, message: 'Product sold out' })}
                                    containerStyle={{ marginTop: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                                <CheckBox
                                    title='items have gone bad'
                                    checkedColor={Colors.theme}
                                    uncheckedColor={Colors.primary}
                                    checked={this.state.selectedIndex === 4}
                                    onPress={() => this.setState({ selectedIndex: 4, message: 'items have gone bad' })}
                                    containerStyle={{ marginTop: -10, backgroundColor: 'transparent', borderColor: 'transparent' }}
                                ></CheckBox>
                                <SimpleText style={{ color: Colors.theme, fontSize: 14, marginTop: 10 }} text='other:' />

                                <TextInput
                                    style={{ marginLeft: 10, borderBottomColor: Colors.theme, borderBottomWidth: 1, marginVertical: 10 }}
                                    placeholder='Write Here' onChangeText={(text)=>{
                                        this.setState({ selectedIndex: 0, message: text})
                                    }}></TextInput>

<View style={{ height: '10%', width: '100%', marginTop: 20, padding: -20}}>
                                <TouchableOpacity
                                disabled={this.state.message === ''}
                                onPress={()=>{
                                    this.setModalVisible(!this.state.modalVisible);
                                    this.props.onReject(this.state.message)
                                    }} style={{ width: '100%', height: 40, backgroundColor: this.state.message === '' ? Colors.disabledtheme : Colors.theme, alignSelf: 'flex-end', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 16 }}>Submit</Text>
                                </TouchableOpacity>
                            </View>
                            </View>
                           
                        </View>
                    </Modal>
                </View>

            </CardView>
        )
    }
}

const Styles = StyleSheet.create({
    image: {
        height: 40,
        width: 40,
    },
    card: {
        marginHorizontal: 30,
        marginVertical: 10,
        backgroundColor: Colors.white,
        padding: 5,


    },
    farmContainer: {
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: Colors.linegray,
        borderBottomWidth: 0.5,
        justifyContent: 'space-around',
        alignItems: 'center'

    }

})

export default MyOrderCardViewWithButton