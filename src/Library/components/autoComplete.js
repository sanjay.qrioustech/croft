import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Text } from 'react-native'
import { Input, Icon } from 'react-native-elements'
import Colors from 'res/Colors'
import  Ionicons  from "react-native-vector-icons/Ionicons";
//import shortid from "shortid";
import { Autocomplete, withKeyboardAwareScrollView } from "react-native-dropdown-autocomplete";

class AutoComplete extends React.Component {

    state = {
        text: ''
    }
    nameTextChange(text) {
        this.setState({
            text: text
        })
    }

    handleSelectItem(item, index) {
        const { onDropdownClose } = this.props;
        onDropdownClose();
        console.log(item);
    }

    render() {
        const autocompletes = [...Array(1).keys()];

        const apiUrl = "https://5b927fd14c818e001456e967.mockapi.io/branches";

        const { scrollToInput, onDropdownClose, onDropdownShow } = this.props;
        return (
            <View style={{ marginVertical: 0.5, height: 50, flex: 1, width: "100%", }}>
                <View >
                    {this.props.isFullInput == undefined ?
                        <View style={{ flexDirection: "row", height: 50, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: 'white',paddingLeft:10 }}>
                            {/* <View style={{ flexDirection: "row", height: 50, flex: 1, width: "100%", alignItems: 'center', justifyContent: 'flex-end', backgroundColor: 'white' }}> */}
                            <Text style={{ width: '35%',paddingRight: 10 ,color:Colors.black}}>{this.props.name}</Text>
                            {autocompletes.map(() => (
                                <Autocomplete
                                    //key={shortid.generate()}
                                    //style={styles.input}
                                    style={styles.input}
                                    //scrollToInput={ev => scrollToInput(ev)}
                                    handleSelectItem={(item, id) => this.handleSelectItem(item, id)}
                                   // onDropdownClose={() => onDropdownClose()}
                                    //onDropdownShow={() => onDropdownShow()}
                                    // renderIcon={() => (
                                    //     <Ionicons name="ios-add-circle-outline" size={20} color="#c7c6c1"  />
                                    // )}
                                    placeholder = 'Requied'
                                    fetchDataUrl={apiUrl}
                                    minimumCharactersCount={2}
                                    //highlightText
                                    valueExtractor={item => item.name}
                                    rightContent
                                    rightTextExtractor={item => item.properties}
                                />
                            ))}
                        </View>
                        :

                        <Input
                            inputStyle={{ fontSize: 15,color:Colors.subtextGray }}
                            style={{ fontSize: 12 }}
                            secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                            value={this.state.text}
                            onChangeText={(text) => this.nameTextChange(text)}
                            placeholder={this.props.placeholder}
                            containerStyle={[this.props.style]}
                        />
                    }

                    {/* <Input
                    secureTextEntry={this.props.isSecure != undefined ? this.props.isSecure : false}
                    value={this.state.text}
                    onChangeText={(text) => this.nameTextChange(text)}
                    label={this.state.text === "" ? "" : this.props.placeholder}
                    labelStyle={styles.placeholderLabel}
                    placeholder={this.props.placeholder}
                    containerStyle={[{ padding: 20 }, this.props.style]} /> */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    placeholderLabel: {
        color: Colors.theme
    },
    headerView: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: "#cf0336",
        alignItems: 'center'
    },
    backImage: {
        height: 25,
        width: 25,
        margin: 10
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'left'
    },
    exitButton: {
        color: 'white',
        fontSize: 15,
        alignSelf: 'center',
        marginRight: 10,
        textAlign: 'center'

    },
    subHeaderText: {
        color: 'black',
        fontSize: 16,
        alignSelf: 'flex-start',
        textAlign: 'left',
        paddingTop: 20,
        paddingLeft: 10,
        fontWeight: '400'
    },
    categoryView: {
        backgroundColor: 'black',
        borderRadius: 15,
        height: 30,
        width: 200,
        marginLeft: 20,
        marginTop: 5,
        justifyContent: 'center'
    },
    featureView: {
        marginLeft: 20,
        marginTop: 15,
        justifyContent: 'center'
    },
    input: {
        borderColor: 'white',
        borderWidth: 0,
    }
})

//export default AutoComplete
export default withKeyboardAwareScrollView(AutoComplete);
