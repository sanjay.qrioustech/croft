import React, { Component } from 'react'
import { SafeAreaView, View, Image, Text, Dimensions } from 'react-native'
import Colors from 'res/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from 'react-native-star-rating';
import SimpleText from 'library/components/SimpleText'
import moment from 'moment'
class ProductListItem extends Component {
    render() {
        return (
            <View style={[{ marginVertical: 1 }, this.props.style]}>
                <View style={{ flex: 1, margin: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image resizeMode='cover' source={{uri: 'https://croft-products.s3-us-west-1.amazonaws.com/'+this.props.image[0]}} style={{ borderRadius: 5, height: Dimensions.get('screen').width/4.5, width: Dimensions.get('screen').width/4.5 }} />
                        <View style={{flex:1, alignSelf: 'flex-start', justifyContent: 'flex-start'}}>
                            <SimpleText style={{ marginHorizontal: 10, fontSize: 15,}} text={this.props.name} />
                            <SimpleText style={{ marginHorizontal: 10, color: Colors.subtextGray, fontSize: 12, marginTop: 5 }} text={"Price per unit: "+this.props.price} />
                            <SimpleText style={{ marginHorizontal: 10, color: Colors.subtextGray, fontSize:12, marginTop: 5 }} text={"Harvesting Date: "+moment(this.props.date).format('DD/MM/YYYY')} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default ProductListItem