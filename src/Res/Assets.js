import googleSignIn from './assets/googleSignIn.png'
import googleIcon from './assets/googleIcon.png'
import facebookLogo from './assets/facebookLogo.png'
import homeTabIcon from './assets/homeTabIcon.png'
import postTabIcon from './assets/postTabIcon.png'
import profileTabIcon from './assets/profileTabIcon.png'
import myOrdersTabIcon from './assets/myOrdersTabIcon.png'
import searchTabIcon from './assets/searchTabIcon.png'
import testImageMango from './assets/testImageMango.jpg'
import logo_transperentsize from './assets/logo_transperentsize.jpg'
import transperentlogo from './assets/transperentlogo.png'
import noFarm from './assets/noFarm.png'
import testImageApple from './assets/testImageApple.jpg'
import testImageLichi from './assets/testImageLichi.jpg'
import home from './assets/home.png'

const assets = {
    googleSignIn,
    googleIcon,
    facebookLogo,
    homeTabIcon,
    postTabIcon,
    profileTabIcon,
    myOrdersTabIcon,
    searchTabIcon,
    testImageMango,
    logo_transperentsize,
    transperentlogo,
    testImageLichi,
    testImageApple,
    noFarm,
    home
  }
  
  export default assets