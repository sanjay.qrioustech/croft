const colors = {
    theme: '#00aea7',
    disabledtheme : 'rgba(0, 174, 167,0.2)',
    googleBlue: '#4c8bf5',
    facebookBlue: '#3b5998',
    inactiveTab: '#454545',
    backgroundGray: '#f3f3f3',
    subtextGray: '#777777',
    title: '#00B75D',
    text: '#0C222B',
    button: '#036675',
    white: '#FFFFFF',
    black: '#000000',
    linegray: '#DCDCDC',
    red:'#e04040',
    green:'#38b33d',
    yellow:'#ddcd37'

  }
  
  export default colors